<?php
/**
 * The template for displaying BuddyPress content.
 *
 * @package WordPress
 * @subpackage BuddyBoss
 * @since BuddyBoss 3.0
 */

get_header(); ?>

	<!-- if widgets are loaded for any BuddyPress component, display the BuddyPress sidebar -->
	<?php if (
		( is_active_sidebar('members') && bp_is_current_component( 'members' ) && !bp_is_user() ) ||
		( is_active_sidebar('profile') && bp_is_user() ) ||
		( is_active_sidebar('groups') && bp_is_current_component( 'groups' ) && !bp_is_group() && !bp_is_user() ) ||
		( is_active_sidebar('group') && bp_is_group() ) ||
		( is_active_sidebar('activity') && bp_is_current_component( 'activity' ) && !bp_is_user() ) ||
		( is_active_sidebar('blogs') && is_multisite() && bp_is_current_component( 'blogs' ) && !bp_is_user() ) ||
		( is_active_sidebar('forums') && bp_is_current_component( 'forums' ) && !bp_is_user() )
	): ?>
		<!-- <div class="page-right-sidebar"> -->

	<!-- if not, hide the sidebar -->
	<?php else: ?>
		<!-- <div class="page-full-width"> -->
	<?php endif; ?>
	<div class="ui grid">
		<div class="four wide column mobile">
			<?php get_sidebar('buddypress'); ?>
		</div>
		<div class="twelve wide column mobile">
			<!-- BuddyPress template content -->

			<div id="content" role="main">

				<article>
				<?php while ( have_posts() ): the_post();
					if ( ( STYLESHEETPATH != TEMPLATEPATH && file_exists( STYLESHEETPATH . '/content-buddypress.php' ) ) || ( STYLESHEETPATH == TEMPLATEPATH && file_exists( TEMPLATEPATH . '/content-buddypress.php' ) ) ) {
						get_template_part( 'content', 'buddypress' );
					} else {
						include plugin_dir_path( dirname( __FILE__ ) ) . 'templates/content-buddypress.php';
					} ?>
					<?php // get_template_part( 'content', 'buddypress' ); ?>
					<?php comments_template( '', true ); ?>
				<?php endwhile; // end of the loop. ?>
				</article>

			</div><!-- #content -->
		</div>



		</div><!-- closing div -->

<?php get_footer(); ?>