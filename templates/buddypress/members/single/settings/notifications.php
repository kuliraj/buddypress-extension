<?php
/**
 * BuddyPress - Members Settings Notifications
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/settings/profile.php */
do_action( 'bp_before_member_settings_template' ); ?>

<?php if ( isset( $_GET['unsubscribe'] ) && true == $_GET['unsubscribe'] ) : ?>
	<?php $user_id = get_current_user_id(); ?>
	<?php if ( $user_id ): ?>
		<?php update_user_meta( $user_id, '_group_email_subscription', 'unsubscribed') ?>
		<div class="ui success message unsubscribed">
			<i class="close icon"></i>
			<div class="header">
				Your have been unsubscribed successfully.
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>
<form action="<?php echo bp_displayed_user_domain() . bp_get_settings_slug() . '/notifications'; ?>" method="post" class="standard-form" id="settings-form">

	<p><?php _e( 'Send an email notice when:', 'buddypress' ); ?></p>
		
	<?php

	/**
	 * Fires at the top of the member template notification settings form.
	 *
	 * @since 1.0.0
	 */
	do_action( 'bp_notification_settings' ); ?>

	<?php

	/**
	 * Fires before the display of the submit button for user notification saving.
	 *
	 * @since 1.5.0
	 */
	do_action( 'bp_members_notification_settings_before_submit' ); ?>

	<div class="submit">
		<input type="submit" name="submit" value="<?php esc_attr_e( 'Save Changes', 'buddypress' ); ?>" id="submit" class="auto" />
	</div>

	<?php

	/**
	 * Fires after the display of the submit button for user notification saving.
	 *
	 * @since 1.5.0
	 */
	do_action( 'bp_members_notification_settings_after_submit' ); ?>

	<?php wp_nonce_field('bp_settings_notifications' ); ?>

</form>

<?php

/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/settings/profile.php */
do_action( 'bp_after_member_settings_template' ); ?>
