<?php
/**
 * BuddyPress - Groups Calendar
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */
?>
<div id="calendar-body" class="<?php echo is_super_admin() || bp_group_is_admin() ? '' : 'players'; ?>">
	<?php if ( is_super_admin() || bp_group_is_admin() ): ?>
	<div class="row">
		<button class="ui basic button btn-new-event">
			<i class="fa fa-calendar-plus-o"></i>
			Create Event
		</button>
	</div>
		
	<div id="event_react_container" class="create-event-container"></div>
	<?php endif; ?>
		
	<div id="event_react_calendar" ></div>

	<div id="event_react_block"></div>

</div><!-- #calendar-body -->
