<?php
/**
 * BuddyPress - Groups Home
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */
?>
<div id="buddypress" class="bp-carpathiafc">

	<?php if ( bp_has_groups() ) : while ( bp_groups() ) : bp_the_group(); ?>

	<?php

	/**
	 * Fires before the display of the group home content.
	 *
	 * @since 1.2.0
	 */
	do_action( 'bp_before_group_home_content' ); ?>

	<div id="item-header" role="complementary">

		<?php
		/**
		 * If the cover image feature is enabled, use a specific header
		 */
		if ( bp_group_use_cover_image_header() ) :
			bp_get_template_part( 'groups/single/cover-image-header' );
		else :
			bp_get_template_part( 'groups/single/group-header' );
		endif;
		?>

	</div><!-- #item-header -->
	<div id="item-nav" class="group-nav">
		<div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
			<ul>

				<?php bp_get_options_nav(); ?>

				<?php

				/**
				 * Fires after the display of group options navigation.
				 *
				 * @since 1.2.0
				 */
				do_action( 'bp_group_options_nav' ); ?>

			</ul>
		</div>
	</div><!-- #item-nav -->


 	<?php /* <div id="item-nav">
	<div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
		<div class="ui compact labeled icon menu">
			<a href="<?php echo bp_group_permalink(); ?>" class="item"><i class="home icon"></i>Home</a>
			<a href="<?php echo bp_group_permalink(); ?>calendar" class="item"><i class="calendar icon"></i>Calendar</a>
			<a href="<?php echo bp_group_permalink(); ?>members" class="item"><i class="user icon"></i>Members</a>
			<?php if ( is_admin() || bp_group_is_admin() ): ?>
				<a href="<?php echo bp_group_permalink(); ?>invite" class="item"><i class="announcement icon"></i>Invite</a>
				<a href="<?php echo bp_group_permalink(); ?>admin" class="item"><i class="setting icon"></i>Manage</a>
			<?php endif; ?>
		</div>
	</div>
</div>
	*/ ?>

	<?php do_action( 'bp_after_groups_item_nav' ); ?>

	<div id="content-body">

		<?php

		/**
		 * Fires before the display of the group home body.
		 *
		 * @since 1.2.0
		 */
		do_action( 'bp_before_group_body' );

		/**
		 * Does this next bit look familiar? If not, go check out WordPress's
		 * /wp-includes/template-loader.php file.
		 *
		 * @todo A real template hierarchy? Gasp!
		 */

			// Looking at home location
			if ( bp_is_group_home() ) :

				if ( bp_group_is_visible() ) {

					// Load appropriate front template
					bp_groups_front_template_part();

				} else {

					/**
					 * Fires before the display of the group status message.
					 *
					 * @since 1.1.0
					 */
					do_action( 'bp_before_group_status_message' ); ?>

					<div id="message" class="info">
						<p><?php bp_group_status_message(); ?></p>
					</div>

					<?php

					/**
					 * Fires after the display of the group status message.
					 *
					 * @since 1.1.0
					 */
					do_action( 'bp_after_group_status_message' );

				}

			// Not looking at home
			else :

				// Group Admin
				if     ( bp_is_group_admin_page() ) : bp_get_template_part( 'groups/single/admin'        );

				// Group Activity
				elseif ( bp_is_group_activity()   ) : bp_get_template_part( 'groups/single/activity'     );

				// Group Mass Message
				elseif ( lckrm_bp_is_group_mass_message() ) : bp_get_template_part( 'groups/single/mass-message'     );

				// Group Calendar
				elseif ( lckrm_bp_is_group_calendar()   ) : bp_get_template_part( 'groups/single/events-calendar'     );

				// Group Invite
				elseif ( lckrm_bp_is_group_invite()   ) : bp_get_template_part( 'groups/single/groups-invite'     );

				// Group Members
				elseif ( bp_is_group_members()    ) : bp_get_template_part( 'groups/single/members' );

				// Group Invitations
				elseif ( bp_is_group_invites()    ) : bp_get_template_part( 'groups/single/send-invites' );

				// Single Event
				elseif ( lckrm_bp_is_group_single_event()    ) : bp_get_template_part( 'groups/single/single-event' );

				// Old group forums
				elseif ( bp_is_group_forum()      ) : bp_get_template_part( 'groups/single/forum'        );

				// Membership request
				elseif ( bp_is_group_membership_request() ) : bp_get_template_part( 'groups/single/request-membership' );

				// Anything else (plugins mostly)
				else                                : bp_get_template_part( 'groups/single/plugins'      );

				endif;

			endif;

		/**
		 * Fires after the display of the group home body.
		 *
		 * @since 1.2.0
		 */
		do_action( 'bp_after_group_body' ); ?>

	</div><!-- #item-body -->

	<?php

	/**
	 * Fires after the display of the group home content.
	 *
	 * @since 1.2.0
	 */
	do_action( 'bp_after_group_home_content' ); ?>

	<?php endwhile; endif; ?>

</div><!-- #buddypress -->