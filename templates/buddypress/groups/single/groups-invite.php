<?php
/**
 * BuddyPress - Groups Invite
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>
<?php if ( is_super_admin() || bp_group_is_admin() ): ?>
<div class="row">
	<button class="ui basic button btn-new-invite">
		<i class="plus icon"></i>
		Add Member
	</button>
</div>

<div id="react_invite_members"></div>
<?php endif; ?>
