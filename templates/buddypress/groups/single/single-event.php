<?php
/**
 * BuddyPress - Single Event
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

if ( ! isset( $_GET['event_id'] ) || empty( $_GET['event_id'] ) ) {
	return;
} 
$event_id = $_GET['event_id']; ?>

<div id="single-event-body">
	<div id="event_single_block"></div>
	
	<?php
	$results = sp_get_main_results( $event_id );
	
	$teams = get_post_meta( $event_id, 'sp_team' );
	$teams = array_filter( $teams, 'sp_filter_positive' );
	if ( $teams ):
		$team_logos = array();
		$i = 0;
		foreach ( $teams as $team ):
			if ( ! has_post_thumbnail( $team ) ) {
				$logo = '';
			} else {
				$logo = get_the_post_thumbnail( $team, 'sportspress-fit-icon' );
			}

			$alt = sizeof( $teams ) == 2 && $i % 2;

			// Add team name
		
			if ( $alt ) {
				$logo .= ' <strong class="sp-team-name">' . get_the_title( $team ) . '</strong>';
			} else {
				$logo = '<strong class="sp-team-name">' . get_the_title( $team ) . '</strong> ' . $logo;
			}
		

			// Add link
			if ( get_option( 'sportspress_link_teams', 'no' ) == 'yes' ) $logo = '<a href="' . get_post_permalink( $team ) . '">' . $logo . '</a>';

			// Add result
			$team_result = array_shift( $results );
			$team_result = apply_filters( 'sportspress_event_logos_team_result', $team_result, $id, $team );
			if ( $alt ) {
				$logo = '<strong class="sp-team-result">' . $team_result . '</strong> ' . $logo;
			} else {
				$logo .= ' <strong class="sp-team-result">' . $team_result . '</strong>';
			}
		

			$team_logos[] = '<span class="sp-team-logo">' . $logo . '</span>';
			$i++;
		endforeach;

		$team_logos = array_filter( $team_logos );
		if ( ! empty( $team_logos ) ):
			echo '<div class="sp-template sp-template-event-logos"><div class="sp-event-logos sp-event-logos-' . sizeof( $teams ) . '">';
			$delimiter = get_option( 'sportspress_event_teams_delimiter', 'vs' );
			echo implode( ' ' . $delimiter . ' ', $team_logos );
			echo '</div></div>';
		endif;
	endif;
	
	$venues = get_the_terms( $event_id, 'sp_venue' );
	if ( $venues ) {
		foreach( $venues as $venue ):
			$t_id = $venue->term_id;
			$meta = get_option( "taxonomy_$t_id" );

			$name = $venue->name;
			$name = '<a href="' . get_term_link( $t_id, 'sp_venue' ) . '">' . $name . '</a>';

			$address = sp_array_value( $meta, 'sp_address', '' );
			$latitude = sp_array_value( $meta, 'sp_latitude', 0 );
			$longitude = sp_array_value( $meta, 'sp_longitude', 0 );
			$map_directions = 'https://maps.google.com?saddr=Current+Location&daddr=' . $latitude . ',' . $longitude . ''; ?>
			<div class="sp-template sp-template-event-venue">
				<table class="sp-data-table sp-event-venue">
					<thead>
						<tr>
							<th><?php echo $name; ?></th>
						</tr>
					</thead>
					<?php if ( $latitude != null && $longitude != null ): ?>
						<tbody>
							<tr class="sp-event-venue-map-row">
								<td>
									<div class="map-wrapper">
										<a href="<?php echo esc_url( $map_directions ); ?>" class="btn-map-directions" target="_blank" >Get Directions</a>
										<div class="sp-google-map <?php echo is_tax( 'sp_venue' ) ? 'sp-venue-map' : ''; ?>" data-address="<?php echo $address; ?>" data-latitude="<?php echo $latitude; ?>" data-longitude="<?php echo $longitude; ?>"></div>
									</div>
								</td>
							</tr>
							<?php if ( $address != null ) { ?>
								<tr class="sp-event-venue-address-row">
									<td><?php echo $address; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					<?php endif; ?>
				</table>
			</div>
			<?php
		endforeach;
	}

	echo do_shortcode( '[event_performance id="' . $event_id . '"]' )
	?>
</div><!-- #calendar-body -->
