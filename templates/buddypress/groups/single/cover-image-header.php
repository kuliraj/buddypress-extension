<?php
/**
 * BuddyPress - Groups Cover Image Header.
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

/**
 * Fires before the display of a group's header.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_group_header' ); ?>
<?php

$team_id = groups_get_groupmeta( bp_get_group_id(), 'team_bp_group_id', true );
if ( $team_id ) {
	$team_leagues = get_the_terms( $team_id, 'sp_league' );
	$team_seasons = get_the_terms( $team_id, 'sp_season' );
	$team_venue = get_the_terms( $team_id, 'sp_venue' );
}
$custom_avatar = wp_get_attachment_image_src( get_post_thumbnail_id( $team_id ), 'medium' );
?>
<div id="cover-image-container">
	<a id="header-cover-image" href="<?php bp_group_permalink(); ?>"></a>

	<div id="item-header-cover-image">
		<?php if ( ! bp_disable_group_avatar_uploads() ) : ?>
			<div id="item-header-avatar" class="image-wrap">
				<a href="<?php bp_group_permalink(); ?>" title="<?php bp_group_name(); ?>" style="background-image: url(<?php echo $custom_avatar[0]; ?>);"></a>
			</div><!-- #item-header-avatar -->
		<?php endif; ?>

		<div id="item-header-content">

			<div id="item-buttons"><?php

				/**
				 * Fires in the group header actions section.
				 *
				 * @since 1.2.6
				 */
				do_action( 'bp_group_header_actions' ); ?></div><!-- #item-buttons -->

			<?php

			/**
			 * Fires before the display of the group's header meta.
			 *
			 * @since 1.2.0
			 */
			do_action( 'bp_before_group_header_meta' ); ?>

			<div id="item-meta">

				<?php

				/**
				 * Fires after the group header actions section.
				 *
				 * @since 1.2.0
				 */
				do_action( 'bp_group_header_meta' ); ?>

				<div class="team-events-info">
						<div class="seasons">
						<?php if ( $team_seasons ): ?>
							<?php foreach ( $team_seasons as $_season ): ?>
								<span>Season <?php echo $_season->name; ?></span>
							<?php endforeach; ?>
						<?php endif; ?>
						</div>
						<div class="competitions">
						<?php if ( $team_leagues ) { ?>
							<?php foreach ( $team_leagues as $_league ) { ?>
									<span><?php echo $_league->name; ?></span>
							<?php } ?>
						<?php } ?>
						</div>
						<div class="venue">
						<?php if ( $team_venue ) { ?>
							<?php foreach ( $team_venue as $venue ) { ?>
								<a href="<?php echo esc_url( get_term_link( $venue->term_id, 'sp_venue' ) ); ?>"><i class="fa fa-map-marker"></i><?php echo $venue->name; ?></a>
							<?php } ?>
						<?php } ?>
						</div>
					</div>
				</div>
		</div><!-- #item-header-content -->

		<!--<div id="item-actions">

			<?php if ( bp_group_is_visible() ) : ?>

				<h3><?php _e( 'Group Admins', 'buddypress' ); ?></h3>

				<?php bp_group_list_admins();

				/**
				 * Fires after the display of the group's administrators.
				 *
				 * @since 1.1.0
				 */
				do_action( 'bp_after_group_menu_admins' );

				if ( bp_group_has_moderators() ) :

					/**
					 * Fires before the display of the group's moderators, if there are any.
					 *
					 * @since 1.1.0
					 */
					do_action( 'bp_before_group_menu_mods' ); ?>

					<h3><?php _e( 'Group Mods' , 'buddypress' ); ?></h3>

					<?php bp_group_list_mods();

					/**
					 * Fires after the display of the group's moderators, if there are any.
					 *
					 * @since 1.1.0
					 */
					do_action( 'bp_after_group_menu_mods' );

				endif;

			endif; ?>

		</div> #item-actions -->

	</div><!-- #item-header-cover-image -->
</div><!-- #cover-image-container -->

<?php

/**
 * Fires after the display of a group's header.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_group_header' );

/** This action is documented in bp-templates/bp-legacy/buddypress/activity/index.php */
do_action( 'template_notices' ); ?>

