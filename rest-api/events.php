<?php


class LCKRM_REST_Events_v1 extends WP_REST_Posts_Controller {
	/**
	 * Check if we can read a post.
	 *
	 * Correctly handles posts with the inherit status.
	 *
	 * @param object $post Post object.
	 * @return bool Can we read it?
	 */
	public function check_read_permission( $post ) {
		$group_id = get_post_meta( $post->ID, 'lckrm_event_created_group', true );
		$is_group_member = groups_is_user_member( get_current_user_id(), $group_id );

		if ( $group_id && $is_group_member ) {
			return true;
		}

		return parent::check_read_permission( $post );
	}


	/**
	 * Check if we can edit a post.
	 *
	 * @param object $post Post object.
	 * @return bool Can we edit it?
	 */
	protected function check_update_permission( $post ) {
		if ( false ) {
			return true;
		}

		return parent::check_update_permission( $post );
	}
}

$LCKRM_REST_Events_v1 = new LCKRM_REST_Events_v1( 'sp_event' );
$LCKRM_REST_Events_v1->register_routes();
