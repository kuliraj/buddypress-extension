import React, { PropTypes } from 'react';
import ReactDOM from 'react-dom';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import Toolbar from 'react-big-calendar/lib'
require( 'moment-timezone' );

BigCalendar.setLocalizer(
	BigCalendar.momentLocalizer(moment)
);

var EventForm = React.createClass({
	getInitialState: function() {
		if ( undefined !== this.props.event ) {
			var event = this.props.event.attributes;
			return {
				is_new: false,
				event_type: event.format ? event.format : '',
				date: event.date ? moment(event.date.localEquivalent()).format('D MMMM, YYYY') : '',
				time_hours: event.date ? moment(event.date.localEquivalent()).format('h') : '',
				time_mins: event.date ? moment(event.date.localEquivalent()).format('mm') : '',
				time_post: event.date ? moment(event.date.localEquivalent()).format('A') : '',
				event_single_recurring: event.is_recurring ? event.is_recurring : '',
				event_notification: event.event_notifications ? event.event_notifications : '', 
				home_team: event.teams[0] ? event.teams[0] : '',
				away_team: event.teams[1] ? event.teams[1] : '',
				event_venue: event.venue.term_id ? event.venue.term_id : ''
			};
		} else {
			return {
				is_new: true,
				event_type: '',
				date: '',
				time_hours: '',
				time_mins: '',
				time_post: 'PM',
				event_single_recurring: 'single_type',
				event_notification: '', 
				home_team: '',
				away_team: '',
				event_venue: ''
			};
		}
	},
	onTypeChange: function(e){
		this.setState({
			'event_type': e.target.value
		});

		if ( 'league' == e.target.value || 'friendly' == e.target.value ) {
			$(ReactDOM.findDOMNode(this.refs.selectTypeSingle)).attr('disabled', 'disabled');
			$(ReactDOM.findDOMNode(this.refs.selectTypeSingle)).prop( "checked", true );
			$(ReactDOM.findDOMNode(this.refs.selectTypeRecurring)).attr('disabled', 'disabled');
			$(ReactDOM.findDOMNode(this.refs.selectTypeRecurring)).removeAttr('selected');
			this.setState({
				'event_single_recurring': 'single_type'
			});

			$(ReactDOM.findDOMNode(this.refs.selectTeam1)).removeAttr('disabled');
			$(ReactDOM.findDOMNode(this.refs.selectTeam2)).removeAttr('disabled');

		} else {
			$(ReactDOM.findDOMNode(this.refs.selectTypeSingle)).removeAttr('disabled');
			$(ReactDOM.findDOMNode(this.refs.selectTypeRecurring)).removeAttr('disabled');
			
			$(ReactDOM.findDOMNode(this.refs.selectTeam1)).attr('disabled', 'disabled');
			$(ReactDOM.findDOMNode(this.refs.selectTeam2)).attr('disabled', 'disabled');

			$(ReactDOM.findDOMNode(this.refs.selectTeam1)).val(lckrm_main.group_team).find("option[value=" + lckrm_main.group_team +"]").attr('selected', true);

			this.setState({
				'home_team': lckrm_main.group_team,
				'away_team': ''
			});
		}
	},
	onNotifyOnChange: function(e){
		if ( this.state.event_notification === 'yes' ) {
			this.setState({
				'event_notification': ''
			});
		} else {
			this.setState({
				'event_notification': e.target.value
			});
		}
	},
	onEventTypeChange: function(e){
		this.setState({
			'event_single_recurring': e.target.value
		});

	},
	componentDidMount:function() {
    	$(ReactDOM.findDOMNode(this.refs.datePicker)).pickadate();
    	if ( this.refs.event_delete ) {
	    	$(ReactDOM.findDOMNode(this.refs.event_delete)).popup({ popup : $(ReactDOM.findDOMNode(this.refs.cancel_deletion)), on: 'click' });
    	}
	},

	onCalendarButtonClick: function(e){
		e.preventDefault();

		var datepicker = $(ReactDOM.findDOMNode(this.refs.datePicker)).pickadate('picker');

		if ( datepicker ) {
			if (datepicker.get('open')) { 
				datepicker.close();
			} else {
				datepicker.open();
			}
		}
	},
	onHomeTeamChange: function(e){
		this.setState({
			'home_team': e.target.value
		});
	},
	onAwayTeamChange: function(e){
		this.setState({
			'away_team': e.target.value
		});
	},
	onVenueChange: function(e){
		this.setState({
			'event_venue': e.target.value
		});
	},
	onTimeHoursChange: function(e) {
		this.setState({
			'time_hours': e.target.value
		});
	},
	onTimeMinsChange: function(e) {
		this.setState({
			'time_mins': e.target.value
		});
	},
	onTimePostChange: function(e){
		this.setState({
			'time_post': e.target.value
		});
	},
	onDeleteEvent: function(e) {
		e.preventDefault();
	},
	cancelDeletion: function(e) {
		e.preventDefault();
		var popup = $(ReactDOM.findDOMNode(this.refs.cancel_deletion));
		if ( popup.hasClass('visible') ) {
			popup.removeClass('visible');
			popup.addClass('hidden');
		}
	},
	proceedDeletion: function(e) {
		e.preventDefault();
		var delete_btn = $(ReactDOM.findDOMNode(this.refs.deleting_event));
		var event = new wp.api.models.Sp_event({id: lckrm_main.single_event_id});
		
		delete_btn.attr('disabled','disabled');
		
		event.fetch().success(function(){
			event.destroy(); // Remove this post from sp_events

			/*  send notifications for cancelled event */
			$.post(lckrm_main.ajax, {action: 'notify_memebers_on_event', group_id: lckrm_main.group_id, event_id: lckrm_main.single_event_id, event_action: 'cancelled'}, function(r){
				window.location = lckrm_main.group_permalink + 'calendar';
			});
		});
	},
	onSubmit:function(e){
		e.preventDefault();
		
		var submit_btn = $(ReactDOM.findDOMNode(this.refs.submitBtn));
		
		if ( '' == this.state.event_venue || '' == this.state.home_team || '' == this.state.event_type || '' == this.state.time_hours || '' == this.state.time_mins || ( '' == this.state.away_team && ( 'league' == this.state.event_type  || 'friendly' == this.state.event_type ) ) ) {

			ReactDOM.render(<ErrorMessage form_state={this.state} />, document.getElementById('on_error_event'));

		} else {
		
			var selectTeam1Index = ReactDOM.findDOMNode(this.refs.selectTeam1).selectedIndex;
			var team1 = ReactDOM.findDOMNode(this.refs.selectTeam1)[selectTeam1Index].text;

			var selectTeam2Index = ReactDOM.findDOMNode(this.refs.selectTeam2).selectedIndex;
			var team2 = ReactDOM.findDOMNode(this.refs.selectTeam2)[selectTeam2Index].text;
			
			var date = $(ReactDOM.findDOMNode(this.refs.datePicker)).pickadate('picker').get('select').obj;
			var time = this.state.time_hours + ':' + this.state.time_mins + ' ' + this.state.time_post;
			var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if (AMPM == "PM" && hours < 12) hours = hours + 12;
			if (AMPM == "AM" && hours == 12) hours = hours - 12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if (hours < 10) sHours = "0" + sHours;
			if (minutes < 10) sMinutes = "0" + sMinutes;
			date.setHours(sHours,sMinutes);
			
			var $event_title = '';
			if ( 'league' == this.state.event_type || 'friendly' == this.state.event_type ) {
				$event_title = team1 + ' vs ' + team2;
			} else {
				switch(this.state.event_type) {
					case 'practice':
						$event_title = 'Practice';
						break;
					case 'meeting':
						$event_title = 'Meeting'
						break;
					default:
						$event_title = 'Other'
				}

				$event_title
			}
			
			submit_btn.attr('disabled','disabled');

			if ( false != this.state.is_new ) {
				var event = new wp.api.models.Sp_event();
			} else {
				var event = new wp.api.models.Sp_event({id: lckrm_main.single_event_id});
			}

			event.set({
				title: $event_title,
				date: date,
				content: '',
				teams: [this.state.home_team, this.state.away_team],
				format: this.state.event_type,
				venue: this.state.event_venue,
				is_recurring: this.state.event_single_recurring,
				event_notifications: 'yes' == this.state.event_notification ? 'yes' : 'no',
				status: 'publish',
				group_id_on_event: lckrm_main.group_id
			});

			var send_notification = 'yes' == this.state.event_notification;

			if ( false != this.state.is_new ) {
				/* Create post in  sp_events and add it to collection */
				event.sync('create', event).success(function(data){
					event.set( event.parse( data ) );
					event.start = moment(event.get('date').localEquivalent()).toDate();
					event.end = moment(event.get('date').localEquivalent()).toDate();
					event.end.setTime( event.end.getTime() + (2*60*60*1000) );
					this.props.events.add( event );
					ReactDOM.render(<EventCreated post={event} />, document.getElementById('event_react_container'));

					if ( send_notification ) {
						/*  send notifications for created event */
						$.post(lckrm_main.ajax, {action: 'notify_memebers_on_event', group_id: lckrm_main.group_id, event_id: event.id, event_action: 'created'}, function(r){

						});
					}

				}.bind(this));
			} else {
				/* Update the post from sp_events */
				event.sync('update', event).success(function(data){
					event.set( event.parse( data ) );
					ReactDOM.unmountComponentAtNode(document.getElementById('edit_single_event_form'));
					ReactDOM.render(<EventCreated post={event} single={true}/>, document.getElementById('edit_single_event_form'));
					this.props.updateEvent(event);
					/*  send notifications for changed event */
					if ( send_notification ) {
						$.post(lckrm_main.ajax, {action: 'notify_memebers_on_event', group_id: lckrm_main.group_id, event_id: lckrm_main.single_event_id, event_action: 'changed'}, function(r){

						});
					}
				}.bind(this));
			}
		}
	},
	render: function() {
		var delete_event = null;
		var submit_event = null;
	
		if ( false == this.state.is_new ) {
			delete_event = (
				<div className="delete-single-event">
					<a href="#" ref='event_delete' onClick={this.onDeleteEvent}><i className="fa fa-calendar-times-o"></i> Delete event</a>
					<div className="ui flowing popup top left transition hidden" ref='cancel_deletion'>
						<h4>Are you sure?</h4>
						<div className="ui buttons">
							<button className="ui button" onClick={this.cancelDeletion}>Cancel</button>
							<div className="or"></div>
							<button className="ui negative button" ref="deleting_event" onClick={this.proceedDeletion}>Delete</button>
						</div>
					</div>
				</div>
			);

			submit_event = (
				<button className="ui basic button submit-event" type="submit" ref="submitBtn" onClick={this.onSubmit}>
					<i className="fa fa-floppy-o"></i> 
					Save Event
				</button>
			);
		} else {
			submit_event = (
				<button className="ui basic button submit-event" type="submit" ref="submitBtn" onClick={this.onSubmit}>
					<i className="fa fa-calendar-check-o"></i> 
					Create Event
				</button>
			);
		}

		return (
			<div className="ui row">
				<div id="on_error_event"></div>
				<form className="ui form event-form">
					<div className="inline fields desktop">
						<div className="field">
							<div className="ui radio checkbox">
								<input type="radio" className="hidden" name="event-type" defaultChecked={this.state.event_type === 'league'} value="league" id="event_type_league" onChange={this.onTypeChange} />
								<label htmlFor="event_type_league">League Game</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
								<input type="radio" className="hidden" name="event-type" defaultChecked={this.state.event_type === 'friendly'} value="friendly" id="event_type_friendly" onChange={this.onTypeChange} />
								<label htmlFor="event_type_friendly">Friendly Game</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
								<input type="radio" className="hidden" name="event-type" defaultChecked={this.state.event_type === 'practice'} value="practice" id="event_type_practice" onChange={this.onTypeChange} />
								<label htmlFor="event_type_practice">Practice</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
								<input type="radio" className="hidden" name="event-type" defaultChecked={this.state.event_type === 'meeting'} value="meeting" id="event_type_meeting" onChange={this.onTypeChange} />
								<label htmlFor="event_type_meeting">Meeting</label>
							</div>
						</div>
						<div className="field">
							<div className="ui radio checkbox">
								<input type="radio" className="hidden" name="event-type" defaultChecked={this.state.event_type === 'other'} value="other" id="event_type_other" onChange={this.onTypeChange} />
								<label htmlFor="event_type_other">Other</label>
							</div>
						</div>
					</div>
					<div className="ui stackable column grid mobile">
						<div className="row">
							<div className="column">
						 	<select className="ui fluid dropdown" defaultValue={this.state.event_type} onChange={this.onTypeChange}>
						 		<option value="" disabled hidden>Please Select Format</option>
					 			<option value="league">League Game</option>
					 			<option value="friendly">Friendly Game</option>
					 			<option value="practice">Practice</option>
					 			<option value="meeting">Meeting</option>
					 			<option value="other">Other</option>
						 	</select>
						</div>
						</div>
					</div>

					<div className="ui stackable three column grid">
						<div className="row">
							<div className="column">
								<div className="date-label">Date</div>
								<div className="ui input">
								<input type="text" ref='datePicker' defaultValue={this.state.date} placeholder={this.props.placeholder} onClick={this.onDateFieldClick}/>
			  					<div onClick={this.onCalendarButtonClick} className="date-picker"><i className="fa fa-calendar"></i></div>
			  					<div id="date-picker"> </div>
								</div>
							</div>
							<div className="column">
									<div className="time-label">Time</div>
								<div className="ui three column grid">
								<div className="column">
										<select className="ui fluid dropdown" defaultValue={this.state.time_hours} onChange={this.onTimeHoursChange}>
								 			<option value="" disabled hidden></option>
							 				<option value="1">1</option>
							 				<option value="2">2</option>
							 				<option value="3">3</option>
							 				<option value="4">4</option>
							 				<option value="5">5</option>
							 				<option value="6">6</option>
							 				<option value="7">7</option>
							 				<option value="8">8</option>
							 				<option value="9">9</option>
							 				<option value="10">10</option>
							 				<option value="11">11</option>
							 				<option value="12">12</option>
							 			</select>
									{/*<div className="ui input">
										<input type="number" min="1" max="12" step="1" defaultValue={this.state.time_hours} placeholder={this.props.placeholder} onChange={this.onTimeHoursChange}/>
									</div>*/}
								</div>
								<div className="column">
									<select className="ui fluid dropdown" defaultValue={this.state.time_mins} onChange={this.onTimeMinsChange}>
							 			<option value="" disabled hidden></option>
						 				<option value="0">00</option>
						 				<option value="05">05</option>
						 				<option value="10">10</option>
						 				<option value="15">15</option>
						 				<option value="20">20</option>
						 				<option value="25">25</option>
						 				<option value="30">30</option>
						 				<option value="35">35</option>
						 				<option value="40">40</option>
						 				<option value="45">45</option>
						 				<option value="50">50</option>
						 				<option value="55">55</option>
						 			</select>

										{/*<div className="ui input">
										  <input type="number" min="00" max="55" step="5" defaultValue={this.state.time_mins} placeholder={this.props.placeholder} onChange={this.onTimeMinsChange}/>
										</div>*/}
								</div>
								<div className="column">
										<div className="field">
										 	<select className="ui fluid dropdown" defaultValue={this.state.time_post} onChange={this.onTimePostChange}>
									 			<option value="PM">PM</option>
									 			<option value="AM">AM</option>
										 	</select>
										</div>
								</div>
								</div>
							</div>
							<div className="column">
								<div className="venue-label">Venue</div>
								<div className="field">
								 	<select className="ui fluid dropdown" defaultValue={this.state.event_venue} onChange={this.onVenueChange} value={this.state.event_venue.term_id}>
								 		<option value="" disabled hidden>Select Venue</option>
							 			{Object.keys(lckrm_main.venues).map(function(key, i, arr) {
											return <option value={key} key={i}>{lckrm_main.venues[key]}</option>;
										})}
								 	</select>
								</div>
							</div>
						</div>
					</div>
					<div className="ui stackable three column grid">
						<div className="row">
							<div className="column">
								<div className="home-team-label">Home Team</div>
								<div className="field">
								 	<select className="ui fluid dropdown" ref='selectTeam1' defaultValue={this.state.home_team} onChange={this.onHomeTeamChange}>
								 		<option value="" disabled hidden>Select Home Team</option>
									 	{Object.keys(lckrm_main.teams).map(function(key, i, arr) {
											return <option value={key} key={i}>{lckrm_main.teams[key]}</option>;
										})}
								 	</select>
								</div>
							</div>
							<div className="column">
								<div className="away-team-label">Away Team</div>
								<div className="field">
								 	<select className="ui fluid dropdown" ref='selectTeam2' defaultValue={this.state.away_team} value={this.state.away_team} onChange={this.onAwayTeamChange} >
								 		<option value="" disabled hidden>Select Away Team</option>
							 			{Object.keys(lckrm_main.teams).map(function(key, i, arr) {
											return <option value={key} key={i}>{lckrm_main.teams[key]}</option>;
										})}
								 	</select>
								</div>
							</div>
						</div>
					</div>
					<div className="ui stackable three column grid">
						<div className="row">
							<div className="column">
							{delete_event}
							</div>
							<div className="column">
								<div className="field notification-field">
									 <div className="ui toggle checkbox">
										<input type="checkbox" className="hidden" name="event-notification" defaultChecked={this.state.event_notification === 'yes'} defaultValue="yes" id="event_notify" onChange={this.onNotifyOnChange} />
										<label htmlFor="event_notify">Send Notification</label>
									</div>
								</div>
							</div>
							<div className="column">
								{submit_event}
							</div>
						</div>
					</div>
				</form>			
			</div>
		);
	}
});

var ErrorMessage = React.createClass({
	render: function() {
		var form_state = this.props.form_state;
		var on_away = null;

		return (
			<div className="ui error message">
				<div className="header">Empty Request</div>
				{Object.keys(form_state).map(function(key, i, arr) {
					var mar = '';
					if ( 'event_notification' != key && 'away_team' != key  && 'date' != key && 'is_new' != key && 'event_single_recurring' != key && '' == form_state[key] ) {
						switch(key) {
							case 'home_team':
								mar = 'Home Team'
								break;
							case 'event_type':
								mar = 'Event Type'
								break;
							case 'event_venue':
								mar = 'Event Venue'
								break;
							case 'time_hours':
								mar = 'Hours'
								break;
							case 'time_mins':
								mar = 'Minutes'
								break;
							default:
								'field'
						}

						return <p key={i}>Please set {mar}!</p>;
					} else {
						if ( '' == form_state['away_team'] && ( 'league' == form_state['event_type']  || 'friendly' == form_state['event_type'] ) ) {
						var away = 'Away Team';
							on_away = (
								<p>Please set {away}!</p>
							);
						}
					}
						
				})}
				{on_away}
			</div>
		);
	}
});

var EventCreated = React.createClass({
	onCloseClick: function() {
		$(ReactDOM.findDOMNode(this.refs.theMessage)).fadeOut('slow');
	},
	render: function(){

		var post = true == this.props.single ? this.props.post : this.props.post;
		var venue = this.props.post.get('venue');
		return (
			<div className="event-created" ref='theMessage'>
				<div className="ui grid success message">
					<i className="close icon" onClick={this.onCloseClick}></i>
					<div className="two wide column">
						<a className="centered-success"><i className="fa fa-check-circle"></i></a>
					</div>
					<div className="twelve wide column">
						<p>Success! Your event is scheduled.</p>
						<h3>{post.get('title').rendered}</h3>
						<p>{moment(post.get('date').localEquivalent()).format('dddd.MMMM Do, YYYY @ h:mm A')}</p>
						<a href={post.get('venue').term_link} ><i className="fa fa-map-marker"></i>{post.get('venue').name}</a>
					</div>
			</div>
			</div>
		);
	}
});

var EventCalendar = React.createClass({

	getInitialState: function() {
		var d = new Date();
		return {
			'events': this.props.events,
			'filters': _.pluck( this.props.types, 'id' ),
			'month_numeric': d.getMonth() + 1,
			'year': d.getFullYear(),
			'currentDay': new Date(),
			'selectedDate': false
		};
	},
	

	onPreviousClick: function(e){
		e.preventDefault();

		if ( 1 < this.state.month_numeric ) {
			this.setState({
				'month_numeric': this.state.month_numeric -1,
				'selectedDate': false
			});
		} else if( 1 == this.state.month_numeric ) {
			this.setState({
				'month_numeric': 12,
				'year': this.state.year -1,
				'selectedDate': false
			});
		}
		
	},

	componentDidMount: function(){
		var events = this.state.events;
		this._boundForceUpdate = _.throttle(function(){
			this.setState({
				events: this.props.events,
			});
		}.bind(this, null), 300, {leading: false});
		events.on('all', this._boundForceUpdate, this);
		$(ReactDOM.findDOMNode(this.refs.export)).dropdown({on:'click'});
	},

	onNextClick: function(e){
		e.preventDefault();

		if ( 12 > this.state.month_numeric ) {
			this.setState({
				'month_numeric': this.state.month_numeric +1,
				'selectedDate': false
			});
		} else if( 12 == this.state.month_numeric ) {
			this.setState({
				'month_numeric': 1,
				'year': this.state.year + 1,
				'selectedDate': false
			});
		}
	},

	FilterClick: function(e){
		var filters = this.state.filters;

		e.preventDefault();

		if ( '1' == e.target.dataset.selected ) {
			filters = _.without( filters, e.target.dataset.type );
		} else if ( -1 === filters.indexOf( e.target.dataset.type ) ) {
			filters.push( e.target.dataset.type );
		}

		e.target.dataset.selected = '1' == e.target.dataset.selected ? '0' : '1';

		this.setState({
			'filters': filters
		});
	},
	updateBlock: function(){
		var events = new wp.api.collections.Sp_event();

		events.fetch({
			'data': {
				'context': 'edit',
				'filter': {
					'post_status': [ 'publish', 'future' ],
					'orderby': 'date',
					'order': 'ASC',
					'nopaging': true,
					'meta_key': 'lckrm_event_created_group',
					'meta_value': lckrm_main.group_id					
				}
			}
		}).success(function(){
			events.map(function(event){
				event.start = moment(event.get('date').localEquivalent()).toDate();
				event.end = moment(event.get('date').localEquivalent()).toDate();
				event.end.setTime( event.end.getTime() + (2*60*60*1000) );

				return event;
			});
			
			this.setState({
				events: events,
			});
		}.bind(this));
	},
	renderEvent: function(event) {
		var event_type = {};
		var event_title = 'X'		

		_.find(this.props.types, function(type){
			if ( -1 !== type.format.indexOf( event.event.get('format') ) ) {
				event_type = type;
			}
		});

		if ( event_type ) {
			switch(event_type.letter) {
				case 'G':
					event_title = 'Game'
					break;
				case 'P':
					event_title = 'Practice'
					break;
				case 'M':
					event_title = 'Meeting'
					break;
				default:
					event_title = 'Other'
			}
		}

		return (
			<a href={lckrm_main.group_permalink + 'event/?event_id=' + event.event.get('id')} className={'ui large label ' + ( event_type ? event_type.color : '')} title={event.event.get('title').rendered}>{moment(event.event.get('date').localEquivalent()).format('h:mmA')}<span className="detachable-event-title">-{event_title}</span></a>
		);
	},
	onDayClick: function(date){
		this.setState({
			'currentDay': date,
			'selectedDate': true,
			'month_numeric': date.getMonth() + 1,
			'year': date.getFullYear()
		});
	},
	render: function() {
		var _export = null;
		var formats = _.uniq( _.flatten( _.pluck( _.filter( this.props.types, function(type){
			return -1 !== this.state.filters.indexOf( type.id );
		}.bind(this) ), 'format' ) ) ),
			currentDate = new Date(),
			events = this.state.events.filter(function(event){
				var filter;

				if ( -1 == formats.indexOf( event.get('format') ) ) {
					filter = false;
				} else {
					filter = true;
				}


				return filter;
			}.bind(this));

		var listEvents = events.filter(function(event){
			if ( this.state.selectedDate ) {
				return moment(event.get('date').localEquivalent()).toDate().toDateString() === this.state.currentDay.toDateString();
			} else {
				var event_date = event.get('date').moment;
				var event_month = event_date.format('M');
				var event_year = event_date.format('Y');
				// Event is in a different month
				if ( event_month != this.state.month_numeric || event_year != this.state.year ) {
					return false;
				// Event is within current month and the it has already passed
				} else if ( ( currentDate.getMonth() + 1 ) == this.state.month_numeric && currentDate.getDate() > event_date.format('D') ) {
					return false;
				} else {
					return true;
				}
			}
		}.bind(this));

		if ( '' != lckrm_main.calendar_export ) {
			_export = (
				<div className="row export-calendar">
					<div className="ui compact menu" >
						<div className="ui simple dropdown item" ref="export">
							<i className="calendar icon"></i>
							Export to Calendar
							<div className="menu">
								{Object.keys(lckrm_main.calendar_export).map(function(key, i, arr) {
									return <a key={'calendar_' + i } href={lckrm_main.calendar_export[key]} target="_blank" className="item">{key}</a>
								})}
							</div>
						</div>
					</div>
				</div>
			);
		}

		return (
			<div className="event_react_calendar">
				<div className="ui three column grid the-calendar">
					<div className="column event-type">
						{(this.props.types.map(function(type){
							return (
								<a key={'filter_type' + type.id} onClick={this.FilterClick} data-type={type.id} data-selected={-1 != this.state.filters.indexOf( type.id ) ? '1' : '0'} className={'ui large circular label ' + ( -1 != this.state.filters.indexOf( type.id ) ? type.color : 'grey' )}>{type.letter}</a>
							);
						}.bind(this)))}
					</div>
					<div className="column event-month">
						<h1>{this.props.months[this.state.month_numeric]} {this.state.year}</h1>
					</div>
					<div className="column event-date-pagi">
						<a href="#" className="previous-date desktop" onClick={this.onPreviousClick} ><i className="caret left icon"></i>Previous</a>
						<a href="#" className="previous-date mobile" onClick={this.onPreviousClick} ><i className="arrow left icon"></i></a>
						
						<a href="#" className="next-date desktop" onClick={this.onNextClick} >Next<i className="caret right icon"></i></a>
						<a href="#" className="next-date mobile" onClick={this.onNextClick} ><i className="arrow right icon"></i></a>
					</div>
				</div>
				<div className="react-big-calenadr">
					<BigCalendar
						date={new Date(this.state.year, this.state.month_numeric - 1, this.state.currentDay.getDate())}
						events={events}
						defaultDate={new Date()}
						onNavigate={this.onDayClick}
						views={['month']}
						toolbar={false}
						components={{
							event: this.renderEvent
						}}
					/>
				</div>
				{_export}
				<div className="events-list">
					{listEvents.map(function(event){
						var single = false;
						return (
							<EventMatchBlock single={single} types={this.props.types} key={'event_' + event.get('id')} event={event} updateBlock={this.updateBlock}/>
						);
					}.bind(this))}
				</div>
			</div>
		);
	}
});

var EventMatchBlock = React.createClass({
	getInitialState: function() {
		return {};
	},
	playersShow: function(e) {
		e.preventDefault();
		$(ReactDOM.findDOMNode(this.refs.attending)).popup({ on: 'click' });
	},
	onClick: function (e) {
		e.preventDefault();

		var event = this.props.event;
		
		var member_choice = '';
		var event_id = event.attributes.id;
		var target_e = $(ReactDOM.findDOMNode(e.target));

		if ( target_e.hasClass('attend') || target_e.parent().hasClass('attend') ) {
			member_choice = 'attend';
			$(ReactDOM.findDOMNode(this.refs.event_attend)).addClass('active');
			$(ReactDOM.findDOMNode(this.refs.event_deny)).removeClass('active');
		} else if ( target_e.hasClass('deny') || target_e.parent().hasClass('deny') ) {
			member_choice = 'deny';
			$(ReactDOM.findDOMNode(this.refs.event_deny)).addClass('active');
			$(ReactDOM.findDOMNode(this.refs.event_attend)).removeClass('active');
		}

		$.ajax({
			type : 'post',
			url: lckrm_main.ajax,
			data: {
				action: 'maybe_attend_event',
				event_id: event_id,
				member_choice: member_choice,
				group_id: lckrm_main.group_id
			},
			success: function( response ){
				if ( true == this.props.single ) {
					var event = new wp.api.models.Sp_event({id: lckrm_main.single_event_id});
					event.fetch().success(function(){
						this.props.updateEvent(event);
					}.bind(this));
				} else {
					this.props.updateBlock();
				}
			}.bind(this)
		});

	},
	render: function(){

		var event = this.props.event;
		var attendings = 0;
		var attending_players = false;
		var will_attend = '';
		var will_deny = '';

		var event_type = {};

		_.find(this.props.types, function(type){
			if ( -1 !== type.format.indexOf( event.get('format') ) ) {
				event_type = type;
			}
		});
		
		Object.keys(event.get('signed_players')).map(function(key, i, arr) {
			if ( 'attend' == event.get('signed_players')[key].member_choice ) {
				return attending_players = true;
			}
		})

		if ( true == attending_players && false == this.props.single ) {
			var attending_popup = <div className="ui flowing popup top left transition hidden">{Object.keys(event.get('signed_players')).map(function(key, i, arr) {
				if ( 'attend' == event.get('signed_players')[key].member_choice ) {
					return  <a key={i} className="ui image label signed-member">
							<img src={event.get('signed_players')[key].avatar}/>
							{event.get('signed_players')[key].name}
							</a>;
				}
			})}</div>
		}

		{Object.keys(event.get('signed_players')).map(function(key, i, arr) {
			if ( 'attend' == event.get('signed_players')[key].member_choice ) {
				return attendings++;
			}
		})}
		
		{Object.keys(event.get('signed_players')).map(function(key, i, arr) {
			if ( lckrm_main.member_id == key ) {
				if ( 'attend' == event.get('signed_players')[key].member_choice ) {
					return will_attend = 'active';
				} else {
					return will_deny = 'active';
				}
			}
		})}

		return (
			<div className="event-react-block">
				<div className="ui grid mobile">
					<div className="four wide column event-type">
						<a href={lckrm_main.group_permalink + 'event/?event_id=' + event.get('id')} className={'ui large circular label ' + ( event_type ? event_type.color : '')}>{event_type ? event_type.letter : 'X'}</a>
					</div>
					<div className="four wide column event-info">
						<h3><a href={lckrm_main.group_permalink + 'event/?event_id=' + event.get('id')}>{event.get('title').rendered}</a></h3>
						<p className="week-day">{moment(event.get('date').localEquivalent()).format('dddd')}</p>
						<p>{moment(event.get('date').localEquivalent()).format('MMMM Do, YYYY @ h:mm A')}</p>
						<a href={event.get('venue').term_link}><i className="fa fa-map-marker"></i>{event.get('venue').name}</a>
					</div>
					<div className="four wide column event-attending" ref="attending" onMouseEnter={this.playersShow} onMouseLeave={this.playersHide}>
						<div className="ui statistic" >
							<div className="value">
							{attendings}
							</div>
							<div className="label">
								Attending
							</div>
						</div>
					</div>
					{attending_popup}
					<div className="four wide column event-rsvp">
						<div className="ui buttons">
							<button className={"ui icon button attend " + will_attend} ref="event_attend" onClick={this.onClick}>
								<i className="checkmark icon"></i>
							</button>
							<button className={"ui icon button deny " + will_deny} ref="event_deny" onClick={this.onClick}>
								<i className="remove icon"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

var ErrorInvite = React.createClass({
	render: function(){
		return (
			<div className="ui error message">
				<div className="header">Empty Request</div>
				<p>Please fill out the form.</p>
			</div>
		);
	}
});

var GroupInvite = React.createClass({
	getInitialState: function(){
		return {
			'on_error': 'no',
			'index': 0,
			'new_members': [{
				'name': '',
				'email': '',
				'phone': '',
				'index': 0
			}]
		};
	},
	AdditionalRow: function(e) {
		e.preventDefault();

		var new_members = this.state.new_members,
			new_index = this.state.index + 1;

		new_members.push({
			'name': '',
			'email': '',
			'phone': '',
			'index': new_index
		});

		this.setState( {
			'index': new_index,
			'new_members': new_members
		} );
	},
	onChange: function(index, values) {
		var new_members = this.state.new_members;

		if ( undefined != new_members[ index ] ) {
			new_members[ index ] = values;

			this.setState( {
				'new_members': new_members
			} );
		}
	},
	RemoveRow: function(index) {
		if ( 1 < this.state.new_members.length ) {
			var new_members = this.state.new_members;

			new_members = new_members.filter( function( el, i ){
				return i !== index;
			} );

			this.setState( {
				'new_members': new_members
			} );
		}
	},
	onFormSubmit: function(e) {
		e.preventDefault();

		var has_errors = false;

		var invite_btn = $(ReactDOM.findDOMNode(this.refs.invite_btn));		

		for (var key in this.state.new_members) {
			if (this.state.new_members.hasOwnProperty(key)) {
				var obj = this.state.new_members[key];
				for (var prop in obj) {
					if ( '' == this.state.new_members[key].name || '' == this.state.new_members[key].email ) {
						has_errors = true;
					}
				}
			}
		}

		if ( has_errors ) {
			ReactDOM.render(<ErrorInvite />, document.getElementById('on_error_invite'));
		} else {
			ReactDOM.unmountComponentAtNode(document.getElementById('on_error_invite'));
			invite_btn.attr('disabled','disabled');
			$.ajax({
				type : 'post',
				url: lckrm_main.ajax,
				data: {
					action: 'get_new_members',
					new_members: this.state.new_members,
					team_id : lckrm_main.group_team,
					group_id : lckrm_main.group_id,
					message: this.refs.invitation_message.value
				},
				success: function( response ){
					if ( true != response.success ) {	
						console.log( response.data.response );
					} else {
						ReactDOM.unmountComponentAtNode(document.getElementById('react_invite_members'));
						ReactDOM.render(<SuccessOnInvite />, document.getElementById('react_invite_members'));
					}
				}.bind(this)
			});
		}
	},
	render: function(){
		var message = 'Hey {member_First_Name}, you\'ve been invited to join {Group_Name}. Please click on the link below to register.\n\n{Short_URL}';
		return (
			<div className="invite-group-members" ref="invite_members">
				<div id="on_error_invite"></div>
				<form className="ui form">
					<div className="ui four column grid">
						{(this.state.new_members.map(function(new_member, index){
							return (
								<NewMemberRow key={'row_' + new_member.index} data={new_member} showAddButton={( index + 1 ) == this.state.new_members.length} showRemoveButton={true} index={index} onAdd={this.AdditionalRow} RemoveRow={this.RemoveRow} onChange={this.onChange} />
							);
						}.bind(this)))}
					</div>
					<div className="grid field member-message">
						<textarea ref="invitation_message" rows="4" defaultValue={message} />
					</div>
					<div className="column">
						<button className="ui basic button right floated submit-invite" ref="invite_btn" type="submit" onClick={this.onFormSubmit}>
							<i className="announcement icon"></i>
							Send Invites
						</button>
					</div>
				</form>
			</div>
		);
	}
});

var NewMemberRow = React.createClass({
	onChange: function(){
		this.props.onChange(this.props.index, {
			'name': this.refs.name.value,
			'email': this.refs.email.value,
			'phone': this.refs.phone.value,
			'index': this.props.data.index
		});
	},
	onRemove: function(e){
		e.preventDefault();
		this.props.RemoveRow( this.props.index );
	},
	render: function(){
		var button = null,
			delete_button = null;

		if ( this.props.showAddButton ) {
			button = (
				<button className="ui circular blue icon button" ref="add_row" onClick={this.props.onAdd}>
					<i className="plus icon"></i>
				</button>
			);
		}

		if ( this.props.showRemoveButton ) {
			delete_button = (
				<button className="ui circular red icon button" ref="add_row" data-index={this.props.index} onClick={this.onRemove}>
					<i className="remove icon"></i>
				</button>
			);
		}

		return (
			<div className="row" ref="invite_row">
				<div className="column">
					<div className="ui icon input">
						<input type="text" ref="name" placeholder="Name" onChange={this.onChange} defaultValue={this.props.data.name} />
						<i className="add user icon"></i>
					</div>
				</div>
				<div className="column">
					<div className="ui icon input">
						<input type="text" ref="email" placeholder="Email" onChange={this.onChange} defaultValue={this.props.data.email} />
						<i className="mail outline icon"></i>
					</div>
				</div>
				<div className="column">
					<div className="ui right icon cheat left icon input">
						<i className="plus icon"></i>
						<input type="text" ref="phone" placeholder="Cell #" onChange={this.onChange} defaultValue="1" />
						<i className="phone icon"></i>
					</div>
				</div>
				<div className="column">
					{delete_button}
					{button}
				</div>
			</div>
		);
	}
});

var SingleEventParent = React.createClass({
	getInitialState: function() {
		return {
			event: this.props.event
		}
	},
	handleEventUpdate: function(event) {
        this.setState({
            event: event
        });
    },
	onEditClick: function(e) {
		e.preventDefault();
		var edit_button = $(ReactDOM.findDOMNode(this.refs.edit_btn));
		if ( edit_button.hasClass('visible') ) {
			edit_button.removeClass('visible');
			edit_button.children().last().removeClass('fa-caret-up');
			edit_button.children().last().addClass('fa-caret-down');
			ReactDOM.unmountComponentAtNode(document.getElementById('edit_single_event_form'));
		} else {
			ReactDOM.render(<EventForm event={this.state.event} types={this.props.types} updateEvent={this.handleEventUpdate} />, document.getElementById('edit_single_event_form'));

			edit_button.addClass('visible');
			edit_button.children().last().removeClass('fa-caret-down');
			edit_button.children().last().addClass('fa-caret-up');
		}
	},
	onListsClick: function(e) {
		e.preventDefault();
		var lists_btn = $(ReactDOM.findDOMNode(this.refs.rsvpBtn));
		if ( lists_btn.hasClass('visible') ) {
			lists_btn.removeClass('visible');
			lists_btn.children().last().removeClass('fa-caret-up');
			lists_btn.children().last().addClass('fa-caret-down');
			ReactDOM.unmountComponentAtNode(document.getElementById('event_rsvp_lists'));
		} else {
			var rsvpd_players = [];
			var event = this.state.event;
			{Object.keys(event.get('signed_players')).map(function(key, i, arr) {
				return rsvpd_players.push(parseInt( key, 10 ));
			})}

			var users = new wp.api.collections.Users();
			users.fetch({
				'data': {
					'lckrm_users_for_group': lckrm_main.group_id,
					'per_page': 100,
					'filter': {
						'include': _.difference(lckrm_main.group_members, rsvpd_players)
					}
				}
			}).success(function(){
				ReactDOM.render(<EventRsvpLists event={event} not_rsvpd={users} />, document.getElementById('event_rsvp_lists'));

			});

			lists_btn.addClass('visible');
			lists_btn.children().last().removeClass('fa-caret-down');
			lists_btn.children().last().addClass('fa-caret-up');
		}
	},
	render: function() {
		var event = this.state.event;
		var edit_event = null;
		var rsvpd_players = [];
		
		if ( 'yes' == lckrm_main.group_moderator ) {
			edit_event = (
				<a href="#" className="edit-single-event" ref="edit_btn" onClick={this.onEditClick} ><i className="write icon"></i>Edit <i className="fa fa-caret-down"></i></a>
			);
		}

		{Object.keys(event.get('signed_players')).map(function(key, i, arr) {
			return rsvpd_players.push(parseInt( key, 10 ));
		})}

		var users = new wp.api.collections.Users();
		users.fetch({
			'data': {
				'lckrm_users_for_group': lckrm_main.group_id,
				'per_page': 100,
				'filter': {
					'include': _.difference(lckrm_main.group_members, rsvpd_players)
				}
			}
		}).success(function(){
			ReactDOM.render(<EventRsvpLists event={event} not_rsvpd={users} />, document.getElementById('event_rsvp_lists'));

		});

		return(
			<div>
				<EventMatchBlock single={this.props.single} event={this.state.event} types={this.props.types} updateEvent={this.handleEventUpdate}/>
				{edit_event}
				<a href="#" className="rsvp-lists visible" ref="rsvpBtn" onClick={this.onListsClick} ><i className="users icon"></i>RSVP Info <i className="fa fa-caret-up"></i>
				</a>
				<div id="edit_single_event_form" className="single-event-form"></div>
				<div id="event_rsvp_lists" className="event-rsvps-lists" ></div>

			</div>
		);
	}
});


var SuccessOnInvite = React.createClass({
	onCloseClick: function() {
		$(ReactDOM.findDOMNode(this.refs.SuccessMsg)).fadeOut('slow');
	},
	render: function() {
		return(
			<div className="invite-created" ref='SuccessMsg'>
				<div className="ui success message transition" >
					<i className="close icon" onClick={this.onCloseClick}></i>
					<div className="header">
						Invitations were sent successfully.
					</div>
				</div>
			</div>
		);
	}
}); 

var Member = React.createClass({
	getInitialState: function() {
		return {
			user: this.props.user,
			text: false,
			mail: false
		}
	},
	initFormText: function(e) {
		e.preventDefault();

		var form = $(ReactDOM.findDOMNode(this.refs.textForm));
		var target = $(ReactDOM.findDOMNode(this.refs.textBtn));
		var mail_btn = $(ReactDOM.findDOMNode(this.refs.mailBtn));

		if ( ! target.hasClass('button') ) {
			target = target.parent();
		}


		if ( target.hasClass('active') ) {
			target.removeClass('active');
			if ( ! mail_btn.hasClass('active') ) {
				form.slideUp( "fast", function() {}).css('display', 'none').removeClass('visible');
			}
			this.setState({
				'text': false
			});
		} else {
			target.addClass('active');
			if ( ! mail_btn.hasClass('active') ) {
				form.slideDown( "fast", function() {}).css('display', 'block').addClass('visible');
			}
			this.setState({
				'text': true
		});
		}
	},
	initFormMail: function(e) {
		e.preventDefault();

		var form = $(ReactDOM.findDOMNode(this.refs.textForm));
		var target = $(ReactDOM.findDOMNode(this.refs.mailBtn));
		var text_btn = $(ReactDOM.findDOMNode(this.refs.textBtn));

		if ( ! target.hasClass('button') ) {
			target = target.parent();
		}


		if ( target.hasClass('active') ) {
			target.removeClass('active');
			if ( ! text_btn.hasClass('active') ) {
				form.slideUp( "fast", function() {}).css('display', 'none').removeClass('visible');
			}
			this.setState({
				'mail': false
			});
		} else {
			target.addClass('active');
			if ( ! text_btn.hasClass('active') ) {
				form.slideDown( "fast", function() {}).css('display', 'block').addClass('visible');
			}
			this.setState({
				'mail': true
		});
		}
	},
	submitMessages: function(e) {
		e.preventDefault();
		var submit_btn = $(ReactDOM.findDOMNode(this.refs.submitMsg));
		var textarea = this.refs.member_message.value;
		var form = $(ReactDOM.findDOMNode(this.refs.textForm));
		var mail_btn = $(ReactDOM.findDOMNode(this.refs.mailBtn));
		var text_btn = $(ReactDOM.findDOMNode(this.refs.textBtn));

		if ( '' != textarea ) {
			submit_btn.attr('disabled','disabled');

			$.post(lckrm_main.ajax, {action: 'direct_memeber_messages', user_id: this.state.user.attributes.id, text: this.state.text, mail: this.state.mail, msg:textarea, group_id:lckrm_main.group_id}, function(r){
				if ( r.success ) {
					form.slideUp( "fast", function() {}).css('display', 'none').removeClass('visible');
					submit_btn.removeAttr('disabled');
					mail_btn.removeClass('active');
					text_btn.removeClass('active');
				}
			}.bind(this));
		}

	},
	render: function() {
		var user = this.state.user;
		var msg_buttons = null;
		var no_phone = user.attributes.cellphone ? false : true;

		if ( 'yes' == lckrm_main.group_moderator ) {
			msg_buttons = (
				<div className="ui two column grid">
					<div className="column">
						<button className="ui icon button send-text" ref="textBtn" onClick={this.initFormText} disabled={no_phone}>
						  <i className="phone icon"></i>
						</button>
						
					</div>
					<div className="column">
						<button className="ui icon button send-mail" ref="mailBtn" onClick={this.initFormMail}>
					 		<i className="mail outline icon"></i>
						</button>
						
					</div>
				</div>
			);
		}

		return (
			<div className="ui grid">
				<div className="thirteen wide column">
					<a href={lckrm_main.home_permalink + 'members/' + user.attributes.slug}><img className="ui middle aligned tiny image" src={user.attributes.avatar_urls[96]}/></a>
					<a href={lckrm_main.home_permalink + 'members/' + user.attributes.slug} className="user-nick">{user.attributes.name}</a>					
				</div>
				<div className="three wide column">
				{msg_buttons}
				</div>
				<div className="ui form" ref="textForm">
					<div className="grid field member-message">
						<textarea ref="member_message" rows="2"  />
					</div>
					<button className="ui basic button right floated" ref="submitMsg" onClick={this.submitMessages}>
						<i className="send outline icon"></i> 
						Send
					</button>
				</div>
			</div>
		);
	},
});

var GroupMembers = React.createClass({
	getInitialState: function() {
		return {
			users: this.props.users,
			mail: false,
			text: false,
		}
	},
	componentDidMount:function() {
    	if ( this.refs.textBtn ) {
	    	$(ReactDOM.findDOMNode(this.refs.textBtn)).popup({ popup : $(ReactDOM.findDOMNode(this.refs.textPopup)), on: 'click' });
    	}

    	if ( this.refs.mailBtn ) {
	    	$(ReactDOM.findDOMNode(this.refs.mailBtn)).popup({ popup : $(ReactDOM.findDOMNode(this.refs.mailPopup)), on: 'click' });
    	}
	},
	ShowTextPop: function(e) {
		e.preventDefault();
	},
	ShowMailPop: function(e) {
		e.preventDefault();
	},
	render: function() {
		return(
			<div className="group-members">
				{(this.state.users.map(function(user, index){
					if ( undefined != user ) {

						return (
							<Member key={'row_' + index} user={user} />
						);
					}
				}.bind(this)))}
			</div>
		);
	}
});

var EventRsvpLists = React.createClass({
	getInitialState: function() {
		return {
			all_attending: false,
			attending: '',
			all_not_attending: false,
			not_attending: '',
			all_no_response: false,
			no_response: ''

		}
	},
	componentDidMount:function() {
		var event = this.props.event;
		var not_rsvpd = this.props.not_rsvpd;
		var _attending = null;
		var _no_attending = null;
		var _no_response = null;

		{Object.keys(event.get('signed_players')).map(function(key, i, arr) {
			if ( 'attend' == event.get('signed_players')[key].member_choice ) {
				_attending = i + 1;
			}
		})}

		{Object.keys(event.get('signed_players')).map(function(key, i, arr) {
			if ( 'deny' == event.get('signed_players')[key].member_choice ) {
				_no_attending = i + 1;
			}
		})}

		{not_rsvpd.map(function(user, i, arr) {
			_no_response = i + 1;
		})}

		this.setState({
			attending: _attending,
			not_attending: _no_attending,
			no_response: _no_response
		});
	},
	onMoreNoResponse: function(e) {
		e.preventDefault();
		this.setState({
			all_no_response: true
		});

	},
	onMoreNotAttending: function(e) {
		e.preventDefault();
		this.setState({
			all_not_attending: true
		});

	},
	onMoreAttending: function(e) {
		e.preventDefault();
		this.setState({
			all_attending: true
		});

	},
	render: function() {
		var event = this.props.event;
		var not_rsvpd = this.props.not_rsvpd;
		var attend_item = null;
		var declined_item = null;
		var response_item = null;

		if ( 11 <= this.state.no_response && ! this.state.all_no_response ) {
			response_item = (
				<div className="item show_more">
					<div className="content">
						<a href="#" onClick={this.onMoreNoResponse} >Show {this.state.no_response - 10} more</a>
					</div>
				</div>
			);
		}
		if ( 11 <= this.state.not_attending && ! this.state.all_not_attending ) {
			declined_item = (
				<div className="item show_more">
					<div className="content">
						<a href="#" onClick={this.onMoreNotAttending} >Show {this.state.not_attending - 10} more</a>
					</div>
				</div>
			);
		}
		if ( 11 <= this.state.attending && ! this.state.all_attending ) {
			attend_item = (
				<div className="item show_more">
					<div className="content">
						<a href="#" onClick={this.onMoreAttending} >Show {this.state.attending - 10} more</a>
					</div>					
				</div>
			);
		}

		return (
			<div className="ui stackable three column grid">
				<div className="column">
					<div className="attending-members">
						<div className="ui small header">Attending</div>
						<div className="ui middle aligned divided list">
							{Object.keys(event.get('signed_players')).map(function(key, i, arr) {
								if ( 'attend' == event.get('signed_players')[key].member_choice ) {
									if ( i <= 9 || this.state.all_attending ) {
										return <div key={i} className="item">
													<img className="ui avatar image" src={event.get('signed_players')[key].avatar}/>
													<div className="content">
														<a href={event.get('signed_players')[key].link} className="header">{event.get('signed_players')[key].name}</a>
													</div>
												</div>
									}
								}
							}.bind(this))}
							{attend_item}
						</div>
					</div>
				</div>
				<div className="column">
					<div className="not-attending-members">
						<div className="ui small header">Declined</div>
						<div className="ui middle aligned divided list">
							{Object.keys(event.get('signed_players')).map(function(key, i, arr) {
								if ( 'deny' == event.get('signed_players')[key].member_choice ) {
									if ( i <= 9 || this.state.all_not_attending ) {
										return <div key={i} className="item">
													<img className="ui avatar image" src={event.get('signed_players')[key].avatar}/>
													<div className="content">
														<a href={event.get('signed_players')[key].link} className="header">{event.get('signed_players')[key].name}</a>
													</div>
												</div>
									}
								}
							}.bind(this))}
							{declined_item}
						</div>
					</div>
				</div>
				<div className="column">
					<div className="not-rsvpd-members">
						<div className="ui small header">No Response</div>
						<div className="ui middle aligned divided list">
							{not_rsvpd.map(function(user, i, arr) {
								if ( i <= 9 || this.state.all_no_response ) {
									return <div key={i} className="item">
												<img className="ui avatar image" src={user.attributes.avatar_urls[96]}/>
												<div className="content">
													<a href={lckrm_main.home_permalink + 'members/' + user.attributes.slug} className="header">
														{user.attributes.name}
													</a>
												</div>
											</div>
								}
							}.bind(this))}
							{response_item}
						</div>
					</div>
				</div>
			</div>
		);
	}
});

(function($){
	$(document).ready(function() {
		var months = {1:"January", 2:"February", 3:"March", 4:"April", 5:"May", 6:"June", 7:"July", 8:"August", 9:"September", 10:"October", 11:"November", 12:"December"},
			event_types = [
				{
					'id': 'game',
					'color': 'blue',
					'letter': 'G',
					'format': ['league', 'friendly']
				},
				{
					'id': 'practice',
					'color': 'green',
					'letter': 'P',
					'format': ['practice']
				},
				{
					'id': 'meeting',
					'color': 'red',
					'letter': 'M',
					'format': ['meeting']
				},
				{
					'id': 'other',
					'color': 'yellow',
					'letter': 'O',
					'format': ['other']
				}
			],
			events,
			event;

		if ( $('#event_react_calendar').length ) {
			events = new wp.api.collections.Sp_event();
			events.fetch({
				'data': {
					'context': 'view',
					'filter': {
						'post_status': [ 'publish', 'future' ],
						'orderby': 'date',
						'order': 'ASC',
						'nopaging': true,
						'meta_key': 'lckrm_event_created_group',
						'meta_value': lckrm_main.group_id
					}
				}
			}).success(function(){
				events.map(function(event){
					event.start = moment(event.get('date').localEquivalent()).toDate();
					event.end = moment(event.get('date').localEquivalent()).toDate();
					event.end.setTime( event.end.getTime() + (2*60*60*1000) );

					return event;
				});

				ReactDOM.render(<EventCalendar events={events} types={event_types} months={months}/>, document.getElementById('event_react_calendar'));
			});
		}

		$('.btn-new-event').on('click', function(event) {
			event.preventDefault();
			if ( $(this).closest('#calendar-body').find('.create-event-container').hasClass('visible') ) {
				ReactDOM.unmountComponentAtNode(document.getElementById('event_react_container'));
				$(this).closest('#calendar-body').find('.create-event-container').removeClass('visible');
			} else {
				$(this).closest('#calendar-body').find('.create-event-container').addClass('visible');
				ReactDOM.render(<EventForm types={event_types} events={events} />, document.getElementById('event_react_container'));				
			}
		});

		if ( $('#event_single_block').length ) {
			event = new wp.api.models.Sp_event({id: lckrm_main.single_event_id});
			event.fetch().success(function(){
				var single = true;
				ReactDOM.render(<SingleEventParent single={single} event={event} types={event_types}/>, document.getElementById('event_single_block'));
			});
		}

		if ( $('#react_group_members').length ) {
			var users = new wp.api.collections.Users();
			users.fetch({
				'data': {
					'lckrm_users_for_group': lckrm_main.group_id,
					'per_page': 100,
					'orderby' : 'name',
					'order'   : 'asc',
					'filter': {
						// 'include': lckrm_main.group_members
					}
				}
			}).success(function(){
				var group_members = users;

				ReactDOM.render(<GroupMembers users={group_members}/>, document.getElementById('react_group_members'));
			});
		}

		$('.btn-new-invite').on('click', function(event) {
			event.preventDefault();
			if ( $(this).closest('#content-body').find('#react_invite_members').hasClass('visible') ) {
				ReactDOM.unmountComponentAtNode(document.getElementById('react_invite_members'));
				$(this).closest('#content-body').find('#react_invite_members').removeClass('visible');
			} else {
				$(this).closest('#content-body').find('#react_invite_members').addClass('visible');
				ReactDOM.render(<GroupInvite />, document.getElementById('react_invite_members'));
			}

		});

		if ( $('#reac_invited_members').length ) {
			ReactDOM.render(<GroupInvitedMembers />, document.getElementById('reac_invited_members'));
		}

	});
})(jQuery);

var DateWithTimezone = function(moment){
	this.moment = moment;
	moment.tz(DateWithTimezone.getTimezone());
}
DateWithTimezone.timezone = 'America/Detroit';

_.extend(DateWithTimezone.prototype, {

	toISO: function(){
		return this.format();
	},

	format: function(format) {
		return this.moment.format(format);
	},

	localEquivalent: function(){
		return moment(this.format(), "YYYY-MM-DDTHH:mm:ss").toDate();
	},

	add: function(field, value) {
		var cloned = this.moment.clone();
		cloned.add(field, value);
		return new DateWithTimezone(cloned);
	},

	hours: function() {
		return this.moment.hours();
	},

	minutes: function() {
		return this.moment.minutes();
	},

	isLaterThan: function(compareTo) {
		return this.moment.valueOf() > compareTo.moment.valueOf();
	}
});

_.extend(DateWithTimezone, {
	fromISO: function(dateTimeString){
		return new DateWithTimezone(moment(dateTimeString));
	},

	fromLocalEquivalent: function(localEquivalent){
		var strWithoutTimezone = moment(localEquivalent).format("YYYY-MM-DDTHH:mm:ss");
		var timezone = moment.tz(DateWithTimezone.getTimezone()).format("Z");
		return new DateWithTimezone(moment(strWithoutTimezone + timezone));
	},

	getTimezone: function(){
		return DateWithTimezone.timezone || "Etc/UTC";
	},

	now: function(){
		return new DateWithTimezone(moment());
	}
});

// Override the default toJSON() and parse() methods as they don't work
// well when the client uses a different timezone than the server
wp.api.models.Sp_event = wp.api.models.Sp_event.extend({
	'_parseableDates': [ 'date', 'modified', 'date_gmt', 'modified_gmt' ],
	/**
	 * Serialize the entity pre-sync.
	 *
	 * @returns {*}.
	 */
	toJSON: function() {
		var attributes = _.clone( this.attributes );

		// Serialize Date objects back into 8601 strings.
		_.each( this._parseableDates, function( key ) {
			if ( key in attributes ) {

				// Don't convert null values
				if ( ! _.isNull( attributes[ key ] ) ) {
					attributes[ key ] = moment(attributes[ key ]).format("YYYY-MM-DDTHH:mm:ss");

					var timezone = moment.tz(DateWithTimezone.getTimezone()).format("Z");
					attributes[ key ] = new DateWithTimezone(moment(attributes[ key ] + timezone)).toISO();
				}
			}
		} );

		return attributes;
	},

	/**
	 * Unserialize the fetched response.
	 *
	 * @param {*} response.
	 * @returns {*}.
	 */
	parse: function( response ) {
		var timestamp;

		// Parse dates into native Date objects.
		_.each( this._parseableDates, function( key ) {
			if ( ! ( key in response ) ) {
				return;
			}

			// Don't convert null values
			if ( ! _.isNull( response[ key ] ) ) {
				// timestamp = wp.api.utils.parseISO8601( response[ key ] );
				response[ key ] = DateWithTimezone.fromISO( response[ key ] );
			}
		});

		return response;
	}
});

// Set the model used by the Sp_event collection
wp.api.collections.Sp_event = wp.api.collections.Sp_event.extend({
	'model': wp.api.models.Sp_event
});
