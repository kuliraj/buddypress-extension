'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactBigCalendar = require('react-big-calendar');

var _reactBigCalendar2 = _interopRequireDefault(_reactBigCalendar);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _lib = require('react-big-calendar/lib');

var _lib2 = _interopRequireDefault(_lib);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('moment-timezone');

_reactBigCalendar2.default.setLocalizer(_reactBigCalendar2.default.momentLocalizer(_moment2.default));

var EventForm = _react2.default.createClass({
	displayName: 'EventForm',

	getInitialState: function getInitialState() {
		if (undefined !== this.props.event) {
			var event = this.props.event.attributes;
			return {
				is_new: false,
				event_type: event.format ? event.format : '',
				date: event.date ? (0, _moment2.default)(event.date.localEquivalent()).format('D MMMM, YYYY') : '',
				time_hours: event.date ? (0, _moment2.default)(event.date.localEquivalent()).format('h') : '',
				time_mins: event.date ? (0, _moment2.default)(event.date.localEquivalent()).format('mm') : '',
				time_post: event.date ? (0, _moment2.default)(event.date.localEquivalent()).format('A') : '',
				event_single_recurring: event.is_recurring ? event.is_recurring : '',
				event_notification: event.event_notifications ? event.event_notifications : '',
				home_team: event.teams[0] ? event.teams[0] : '',
				away_team: event.teams[1] ? event.teams[1] : '',
				event_venue: event.venue.term_id ? event.venue.term_id : ''
			};
		} else {
			return {
				is_new: true,
				event_type: '',
				date: '',
				time_hours: '',
				time_mins: '',
				time_post: 'PM',
				event_single_recurring: 'single_type',
				event_notification: '',
				home_team: '',
				away_team: '',
				event_venue: ''
			};
		}
	},
	onTypeChange: function onTypeChange(e) {
		this.setState({
			'event_type': e.target.value
		});

		if ('league' == e.target.value || 'friendly' == e.target.value) {
			$(_reactDom2.default.findDOMNode(this.refs.selectTypeSingle)).attr('disabled', 'disabled');
			$(_reactDom2.default.findDOMNode(this.refs.selectTypeSingle)).prop("checked", true);
			$(_reactDom2.default.findDOMNode(this.refs.selectTypeRecurring)).attr('disabled', 'disabled');
			$(_reactDom2.default.findDOMNode(this.refs.selectTypeRecurring)).removeAttr('selected');
			this.setState({
				'event_single_recurring': 'single_type'
			});

			$(_reactDom2.default.findDOMNode(this.refs.selectTeam1)).removeAttr('disabled');
			$(_reactDom2.default.findDOMNode(this.refs.selectTeam2)).removeAttr('disabled');
		} else {
			$(_reactDom2.default.findDOMNode(this.refs.selectTypeSingle)).removeAttr('disabled');
			$(_reactDom2.default.findDOMNode(this.refs.selectTypeRecurring)).removeAttr('disabled');

			$(_reactDom2.default.findDOMNode(this.refs.selectTeam1)).attr('disabled', 'disabled');
			$(_reactDom2.default.findDOMNode(this.refs.selectTeam2)).attr('disabled', 'disabled');

			$(_reactDom2.default.findDOMNode(this.refs.selectTeam1)).val(lckrm_main.group_team).find("option[value=" + lckrm_main.group_team + "]").attr('selected', true);

			this.setState({
				'home_team': lckrm_main.group_team,
				'away_team': ''
			});
		}
	},
	onNotifyOnChange: function onNotifyOnChange(e) {
		if (this.state.event_notification === 'yes') {
			this.setState({
				'event_notification': ''
			});
		} else {
			this.setState({
				'event_notification': e.target.value
			});
		}
	},
	onEventTypeChange: function onEventTypeChange(e) {
		this.setState({
			'event_single_recurring': e.target.value
		});
	},
	componentDidMount: function componentDidMount() {
		$(_reactDom2.default.findDOMNode(this.refs.datePicker)).pickadate();
		if (this.refs.event_delete) {
			$(_reactDom2.default.findDOMNode(this.refs.event_delete)).popup({ popup: $(_reactDom2.default.findDOMNode(this.refs.cancel_deletion)), on: 'click' });
		}
	},

	onCalendarButtonClick: function onCalendarButtonClick(e) {
		e.preventDefault();

		var datepicker = $(_reactDom2.default.findDOMNode(this.refs.datePicker)).pickadate('picker');

		if (datepicker) {
			if (datepicker.get('open')) {
				datepicker.close();
			} else {
				datepicker.open();
			}
		}
	},
	onHomeTeamChange: function onHomeTeamChange(e) {
		this.setState({
			'home_team': e.target.value
		});
	},
	onAwayTeamChange: function onAwayTeamChange(e) {
		this.setState({
			'away_team': e.target.value
		});
	},
	onVenueChange: function onVenueChange(e) {
		this.setState({
			'event_venue': e.target.value
		});
	},
	onTimeHoursChange: function onTimeHoursChange(e) {
		this.setState({
			'time_hours': e.target.value
		});
	},
	onTimeMinsChange: function onTimeMinsChange(e) {
		this.setState({
			'time_mins': e.target.value
		});
	},
	onTimePostChange: function onTimePostChange(e) {
		this.setState({
			'time_post': e.target.value
		});
	},
	onDeleteEvent: function onDeleteEvent(e) {
		e.preventDefault();
	},
	cancelDeletion: function cancelDeletion(e) {
		e.preventDefault();
		var popup = $(_reactDom2.default.findDOMNode(this.refs.cancel_deletion));
		if (popup.hasClass('visible')) {
			popup.removeClass('visible');
			popup.addClass('hidden');
		}
	},
	proceedDeletion: function proceedDeletion(e) {
		e.preventDefault();
		var delete_btn = $(_reactDom2.default.findDOMNode(this.refs.deleting_event));
		var event = new wp.api.models.Sp_event({ id: lckrm_main.single_event_id });

		delete_btn.attr('disabled', 'disabled');

		event.fetch().success(function () {
			event.destroy(); // Remove this post from sp_events

			/*  send notifications for cancelled event */
			$.post(lckrm_main.ajax, { action: 'notify_memebers_on_event', group_id: lckrm_main.group_id, event_id: lckrm_main.single_event_id, event_action: 'cancelled' }, function (r) {
				window.location = lckrm_main.group_permalink + 'calendar';
			});
		});
	},
	onSubmit: function onSubmit(e) {
		e.preventDefault();

		var submit_btn = $(_reactDom2.default.findDOMNode(this.refs.submitBtn));

		if ('' == this.state.event_venue || '' == this.state.home_team || '' == this.state.event_type || '' == this.state.time_hours || '' == this.state.time_mins || '' == this.state.away_team && ('league' == this.state.event_type || 'friendly' == this.state.event_type)) {

			_reactDom2.default.render(_react2.default.createElement(ErrorMessage, { form_state: this.state }), document.getElementById('on_error_event'));
		} else {

			var selectTeam1Index = _reactDom2.default.findDOMNode(this.refs.selectTeam1).selectedIndex;
			var team1 = _reactDom2.default.findDOMNode(this.refs.selectTeam1)[selectTeam1Index].text;

			var selectTeam2Index = _reactDom2.default.findDOMNode(this.refs.selectTeam2).selectedIndex;
			var team2 = _reactDom2.default.findDOMNode(this.refs.selectTeam2)[selectTeam2Index].text;

			var date = $(_reactDom2.default.findDOMNode(this.refs.datePicker)).pickadate('picker').get('select').obj;
			var time = this.state.time_hours + ':' + this.state.time_mins + ' ' + this.state.time_post;
			var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if (AMPM == "PM" && hours < 12) hours = hours + 12;
			if (AMPM == "AM" && hours == 12) hours = hours - 12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if (hours < 10) sHours = "0" + sHours;
			if (minutes < 10) sMinutes = "0" + sMinutes;
			date.setHours(sHours, sMinutes);

			var $event_title = '';
			if ('league' == this.state.event_type || 'friendly' == this.state.event_type) {
				$event_title = team1 + ' vs ' + team2;
			} else {
				switch (this.state.event_type) {
					case 'practice':
						$event_title = 'Practice';
						break;
					case 'meeting':
						$event_title = 'Meeting';
						break;
					default:
						$event_title = 'Other';
				}

				$event_title;
			}

			submit_btn.attr('disabled', 'disabled');

			if (false != this.state.is_new) {
				var event = new wp.api.models.Sp_event();
			} else {
				var event = new wp.api.models.Sp_event({ id: lckrm_main.single_event_id });
			}

			event.set({
				title: $event_title,
				date: date,
				content: '',
				teams: [this.state.home_team, this.state.away_team],
				format: this.state.event_type,
				venue: this.state.event_venue,
				is_recurring: this.state.event_single_recurring,
				event_notifications: 'yes' == this.state.event_notification ? 'yes' : 'no',
				status: 'publish',
				group_id_on_event: lckrm_main.group_id
			});

			var send_notification = 'yes' == this.state.event_notification;

			if (false != this.state.is_new) {
				/* Create post in  sp_events and add it to collection */
				event.sync('create', event).success(function (data) {
					event.set(event.parse(data));
					event.start = (0, _moment2.default)(event.get('date').localEquivalent()).toDate();
					event.end = (0, _moment2.default)(event.get('date').localEquivalent()).toDate();
					event.end.setTime(event.end.getTime() + 2 * 60 * 60 * 1000);
					this.props.events.add(event);
					_reactDom2.default.render(_react2.default.createElement(EventCreated, { post: event }), document.getElementById('event_react_container'));

					if (send_notification) {
						/*  send notifications for created event */
						$.post(lckrm_main.ajax, { action: 'notify_memebers_on_event', group_id: lckrm_main.group_id, event_id: event.id, event_action: 'created' }, function (r) {});
					}
				}.bind(this));
			} else {
				/* Update the post from sp_events */
				event.sync('update', event).success(function (data) {
					event.set(event.parse(data));
					_reactDom2.default.unmountComponentAtNode(document.getElementById('edit_single_event_form'));
					_reactDom2.default.render(_react2.default.createElement(EventCreated, { post: event, single: true }), document.getElementById('edit_single_event_form'));
					this.props.updateEvent(event);
					/*  send notifications for changed event */
					if (send_notification) {
						$.post(lckrm_main.ajax, { action: 'notify_memebers_on_event', group_id: lckrm_main.group_id, event_id: lckrm_main.single_event_id, event_action: 'changed' }, function (r) {});
					}
				}.bind(this));
			}
		}
	},
	render: function render() {
		var delete_event = null;
		var submit_event = null;

		if (false == this.state.is_new) {
			delete_event = _react2.default.createElement(
				'div',
				{ className: 'delete-single-event' },
				_react2.default.createElement(
					'a',
					{ href: '#', ref: 'event_delete', onClick: this.onDeleteEvent },
					_react2.default.createElement('i', { className: 'fa fa-calendar-times-o' }),
					' Delete event'
				),
				_react2.default.createElement(
					'div',
					{ className: 'ui flowing popup top left transition hidden', ref: 'cancel_deletion' },
					_react2.default.createElement(
						'h4',
						null,
						'Are you sure?'
					),
					_react2.default.createElement(
						'div',
						{ className: 'ui buttons' },
						_react2.default.createElement(
							'button',
							{ className: 'ui button', onClick: this.cancelDeletion },
							'Cancel'
						),
						_react2.default.createElement('div', { className: 'or' }),
						_react2.default.createElement(
							'button',
							{ className: 'ui negative button', ref: 'deleting_event', onClick: this.proceedDeletion },
							'Delete'
						)
					)
				)
			);

			submit_event = _react2.default.createElement(
				'button',
				{ className: 'ui basic button submit-event', type: 'submit', ref: 'submitBtn', onClick: this.onSubmit },
				_react2.default.createElement('i', { className: 'fa fa-floppy-o' }),
				'Save Event'
			);
		} else {
			submit_event = _react2.default.createElement(
				'button',
				{ className: 'ui basic button submit-event', type: 'submit', ref: 'submitBtn', onClick: this.onSubmit },
				_react2.default.createElement('i', { className: 'fa fa-calendar-check-o' }),
				'Create Event'
			);
		}

		return _react2.default.createElement(
			'div',
			{ className: 'ui row' },
			_react2.default.createElement('div', { id: 'on_error_event' }),
			_react2.default.createElement(
				'form',
				{ className: 'ui form event-form' },
				_react2.default.createElement(
					'div',
					{ className: 'inline fields desktop' },
					_react2.default.createElement(
						'div',
						{ className: 'field' },
						_react2.default.createElement(
							'div',
							{ className: 'ui radio checkbox' },
							_react2.default.createElement('input', { type: 'radio', className: 'hidden', name: 'event-type', defaultChecked: this.state.event_type === 'league', value: 'league', id: 'event_type_league', onChange: this.onTypeChange }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'event_type_league' },
								'League Game'
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'field' },
						_react2.default.createElement(
							'div',
							{ className: 'ui radio checkbox' },
							_react2.default.createElement('input', { type: 'radio', className: 'hidden', name: 'event-type', defaultChecked: this.state.event_type === 'friendly', value: 'friendly', id: 'event_type_friendly', onChange: this.onTypeChange }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'event_type_friendly' },
								'Friendly Game'
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'field' },
						_react2.default.createElement(
							'div',
							{ className: 'ui radio checkbox' },
							_react2.default.createElement('input', { type: 'radio', className: 'hidden', name: 'event-type', defaultChecked: this.state.event_type === 'practice', value: 'practice', id: 'event_type_practice', onChange: this.onTypeChange }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'event_type_practice' },
								'Practice'
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'field' },
						_react2.default.createElement(
							'div',
							{ className: 'ui radio checkbox' },
							_react2.default.createElement('input', { type: 'radio', className: 'hidden', name: 'event-type', defaultChecked: this.state.event_type === 'meeting', value: 'meeting', id: 'event_type_meeting', onChange: this.onTypeChange }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'event_type_meeting' },
								'Meeting'
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'field' },
						_react2.default.createElement(
							'div',
							{ className: 'ui radio checkbox' },
							_react2.default.createElement('input', { type: 'radio', className: 'hidden', name: 'event-type', defaultChecked: this.state.event_type === 'other', value: 'other', id: 'event_type_other', onChange: this.onTypeChange }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'event_type_other' },
								'Other'
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'ui stackable column grid mobile' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'column' },
							_react2.default.createElement(
								'select',
								{ className: 'ui fluid dropdown', defaultValue: this.state.event_type, onChange: this.onTypeChange },
								_react2.default.createElement(
									'option',
									{ value: '', disabled: true, hidden: true },
									'Please Select Format'
								),
								_react2.default.createElement(
									'option',
									{ value: 'league' },
									'League Game'
								),
								_react2.default.createElement(
									'option',
									{ value: 'friendly' },
									'Friendly Game'
								),
								_react2.default.createElement(
									'option',
									{ value: 'practice' },
									'Practice'
								),
								_react2.default.createElement(
									'option',
									{ value: 'meeting' },
									'Meeting'
								),
								_react2.default.createElement(
									'option',
									{ value: 'other' },
									'Other'
								)
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'ui stackable three column grid' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'column' },
							_react2.default.createElement(
								'div',
								{ className: 'date-label' },
								'Date'
							),
							_react2.default.createElement(
								'div',
								{ className: 'ui input' },
								_react2.default.createElement('input', { type: 'text', ref: 'datePicker', defaultValue: this.state.date, placeholder: this.props.placeholder, onClick: this.onDateFieldClick }),
								_react2.default.createElement(
									'div',
									{ onClick: this.onCalendarButtonClick, className: 'date-picker' },
									_react2.default.createElement('i', { className: 'fa fa-calendar' })
								),
								_react2.default.createElement(
									'div',
									{ id: 'date-picker' },
									' '
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'column' },
							_react2.default.createElement(
								'div',
								{ className: 'time-label' },
								'Time'
							),
							_react2.default.createElement(
								'div',
								{ className: 'ui three column grid' },
								_react2.default.createElement(
									'div',
									{ className: 'column' },
									_react2.default.createElement(
										'select',
										{ className: 'ui fluid dropdown', defaultValue: this.state.time_hours, onChange: this.onTimeHoursChange },
										_react2.default.createElement('option', { value: '', disabled: true, hidden: true }),
										_react2.default.createElement(
											'option',
											{ value: '1' },
											'1'
										),
										_react2.default.createElement(
											'option',
											{ value: '2' },
											'2'
										),
										_react2.default.createElement(
											'option',
											{ value: '3' },
											'3'
										),
										_react2.default.createElement(
											'option',
											{ value: '4' },
											'4'
										),
										_react2.default.createElement(
											'option',
											{ value: '5' },
											'5'
										),
										_react2.default.createElement(
											'option',
											{ value: '6' },
											'6'
										),
										_react2.default.createElement(
											'option',
											{ value: '7' },
											'7'
										),
										_react2.default.createElement(
											'option',
											{ value: '8' },
											'8'
										),
										_react2.default.createElement(
											'option',
											{ value: '9' },
											'9'
										),
										_react2.default.createElement(
											'option',
											{ value: '10' },
											'10'
										),
										_react2.default.createElement(
											'option',
											{ value: '11' },
											'11'
										),
										_react2.default.createElement(
											'option',
											{ value: '12' },
											'12'
										)
									)
								),
								_react2.default.createElement(
									'div',
									{ className: 'column' },
									_react2.default.createElement(
										'select',
										{ className: 'ui fluid dropdown', defaultValue: this.state.time_mins, onChange: this.onTimeMinsChange },
										_react2.default.createElement('option', { value: '', disabled: true, hidden: true }),
										_react2.default.createElement(
											'option',
											{ value: '0' },
											'00'
										),
										_react2.default.createElement(
											'option',
											{ value: '05' },
											'05'
										),
										_react2.default.createElement(
											'option',
											{ value: '10' },
											'10'
										),
										_react2.default.createElement(
											'option',
											{ value: '15' },
											'15'
										),
										_react2.default.createElement(
											'option',
											{ value: '20' },
											'20'
										),
										_react2.default.createElement(
											'option',
											{ value: '25' },
											'25'
										),
										_react2.default.createElement(
											'option',
											{ value: '30' },
											'30'
										),
										_react2.default.createElement(
											'option',
											{ value: '35' },
											'35'
										),
										_react2.default.createElement(
											'option',
											{ value: '40' },
											'40'
										),
										_react2.default.createElement(
											'option',
											{ value: '45' },
											'45'
										),
										_react2.default.createElement(
											'option',
											{ value: '50' },
											'50'
										),
										_react2.default.createElement(
											'option',
											{ value: '55' },
											'55'
										)
									)
								),
								_react2.default.createElement(
									'div',
									{ className: 'column' },
									_react2.default.createElement(
										'div',
										{ className: 'field' },
										_react2.default.createElement(
											'select',
											{ className: 'ui fluid dropdown', defaultValue: this.state.time_post, onChange: this.onTimePostChange },
											_react2.default.createElement(
												'option',
												{ value: 'PM' },
												'PM'
											),
											_react2.default.createElement(
												'option',
												{ value: 'AM' },
												'AM'
											)
										)
									)
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'column' },
							_react2.default.createElement(
								'div',
								{ className: 'venue-label' },
								'Venue'
							),
							_react2.default.createElement(
								'div',
								{ className: 'field' },
								_react2.default.createElement(
									'select',
									{ className: 'ui fluid dropdown', defaultValue: this.state.event_venue, onChange: this.onVenueChange, value: this.state.event_venue.term_id },
									_react2.default.createElement(
										'option',
										{ value: '', disabled: true, hidden: true },
										'Select Venue'
									),
									Object.keys(lckrm_main.venues).map(function (key, i, arr) {
										return _react2.default.createElement(
											'option',
											{ value: key, key: i },
											lckrm_main.venues[key]
										);
									})
								)
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'ui stackable three column grid' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'column' },
							_react2.default.createElement(
								'div',
								{ className: 'home-team-label' },
								'Home Team'
							),
							_react2.default.createElement(
								'div',
								{ className: 'field' },
								_react2.default.createElement(
									'select',
									{ className: 'ui fluid dropdown', ref: 'selectTeam1', defaultValue: this.state.home_team, onChange: this.onHomeTeamChange },
									_react2.default.createElement(
										'option',
										{ value: '', disabled: true, hidden: true },
										'Select Home Team'
									),
									Object.keys(lckrm_main.teams).map(function (key, i, arr) {
										return _react2.default.createElement(
											'option',
											{ value: key, key: i },
											lckrm_main.teams[key]
										);
									})
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'column' },
							_react2.default.createElement(
								'div',
								{ className: 'away-team-label' },
								'Away Team'
							),
							_react2.default.createElement(
								'div',
								{ className: 'field' },
								_react2.default.createElement(
									'select',
									{ className: 'ui fluid dropdown', ref: 'selectTeam2', defaultValue: this.state.away_team, value: this.state.away_team, onChange: this.onAwayTeamChange },
									_react2.default.createElement(
										'option',
										{ value: '', disabled: true, hidden: true },
										'Select Away Team'
									),
									Object.keys(lckrm_main.teams).map(function (key, i, arr) {
										return _react2.default.createElement(
											'option',
											{ value: key, key: i },
											lckrm_main.teams[key]
										);
									})
								)
							)
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'ui stackable three column grid' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'column' },
							delete_event
						),
						_react2.default.createElement(
							'div',
							{ className: 'column' },
							_react2.default.createElement(
								'div',
								{ className: 'field notification-field' },
								_react2.default.createElement(
									'div',
									{ className: 'ui toggle checkbox' },
									_react2.default.createElement('input', { type: 'checkbox', className: 'hidden', name: 'event-notification', defaultChecked: this.state.event_notification === 'yes', defaultValue: 'yes', id: 'event_notify', onChange: this.onNotifyOnChange }),
									_react2.default.createElement(
										'label',
										{ htmlFor: 'event_notify' },
										'Send Notification'
									)
								)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'column' },
							submit_event
						)
					)
				)
			)
		);
	}
});

var ErrorMessage = _react2.default.createClass({
	displayName: 'ErrorMessage',

	render: function render() {
		var form_state = this.props.form_state;
		var on_away = null;

		return _react2.default.createElement(
			'div',
			{ className: 'ui error message' },
			_react2.default.createElement(
				'div',
				{ className: 'header' },
				'Empty Request'
			),
			Object.keys(form_state).map(function (key, i, arr) {
				var mar = '';
				if ('event_notification' != key && 'away_team' != key && 'date' != key && 'is_new' != key && 'event_single_recurring' != key && '' == form_state[key]) {
					switch (key) {
						case 'home_team':
							mar = 'Home Team';
							break;
						case 'event_type':
							mar = 'Event Type';
							break;
						case 'event_venue':
							mar = 'Event Venue';
							break;
						case 'time_hours':
							mar = 'Hours';
							break;
						case 'time_mins':
							mar = 'Minutes';
							break;
						default:
							'field';
					}

					return _react2.default.createElement(
						'p',
						{ key: i },
						'Please set ',
						mar,
						'!'
					);
				} else {
					if ('' == form_state['away_team'] && ('league' == form_state['event_type'] || 'friendly' == form_state['event_type'])) {
						var away = 'Away Team';
						on_away = _react2.default.createElement(
							'p',
							null,
							'Please set ',
							away,
							'!'
						);
					}
				}
			}),
			on_away
		);
	}
});

var EventCreated = _react2.default.createClass({
	displayName: 'EventCreated',

	onCloseClick: function onCloseClick() {
		$(_reactDom2.default.findDOMNode(this.refs.theMessage)).fadeOut('slow');
	},
	render: function render() {

		var post = true == this.props.single ? this.props.post : this.props.post;
		var venue = this.props.post.get('venue');
		return _react2.default.createElement(
			'div',
			{ className: 'event-created', ref: 'theMessage' },
			_react2.default.createElement(
				'div',
				{ className: 'ui grid success message' },
				_react2.default.createElement('i', { className: 'close icon', onClick: this.onCloseClick }),
				_react2.default.createElement(
					'div',
					{ className: 'two wide column' },
					_react2.default.createElement(
						'a',
						{ className: 'centered-success' },
						_react2.default.createElement('i', { className: 'fa fa-check-circle' })
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'twelve wide column' },
					_react2.default.createElement(
						'p',
						null,
						'Success! Your event is scheduled.'
					),
					_react2.default.createElement(
						'h3',
						null,
						post.get('title').rendered
					),
					_react2.default.createElement(
						'p',
						null,
						(0, _moment2.default)(post.get('date').localEquivalent()).format('dddd.MMMM Do, YYYY @ h:mm A')
					),
					_react2.default.createElement(
						'a',
						{ href: post.get('venue').term_link },
						_react2.default.createElement('i', { className: 'fa fa-map-marker' }),
						post.get('venue').name
					)
				)
			)
		);
	}
});

var EventCalendar = _react2.default.createClass({
	displayName: 'EventCalendar',


	getInitialState: function getInitialState() {
		var d = new Date();
		return {
			'events': this.props.events,
			'filters': _.pluck(this.props.types, 'id'),
			'month_numeric': d.getMonth() + 1,
			'year': d.getFullYear(),
			'currentDay': new Date(),
			'selectedDate': false
		};
	},

	onPreviousClick: function onPreviousClick(e) {
		e.preventDefault();

		if (1 < this.state.month_numeric) {
			this.setState({
				'month_numeric': this.state.month_numeric - 1,
				'selectedDate': false
			});
		} else if (1 == this.state.month_numeric) {
			this.setState({
				'month_numeric': 12,
				'year': this.state.year - 1,
				'selectedDate': false
			});
		}
	},

	componentDidMount: function componentDidMount() {
		var events = this.state.events;
		this._boundForceUpdate = _.throttle(function () {
			this.setState({
				events: this.props.events
			});
		}.bind(this, null), 300, { leading: false });
		events.on('all', this._boundForceUpdate, this);
		$(_reactDom2.default.findDOMNode(this.refs.export)).dropdown({ on: 'click' });
	},

	onNextClick: function onNextClick(e) {
		e.preventDefault();

		if (12 > this.state.month_numeric) {
			this.setState({
				'month_numeric': this.state.month_numeric + 1,
				'selectedDate': false
			});
		} else if (12 == this.state.month_numeric) {
			this.setState({
				'month_numeric': 1,
				'year': this.state.year + 1,
				'selectedDate': false
			});
		}
	},

	FilterClick: function FilterClick(e) {
		var filters = this.state.filters;

		e.preventDefault();

		if ('1' == e.target.dataset.selected) {
			filters = _.without(filters, e.target.dataset.type);
		} else if (-1 === filters.indexOf(e.target.dataset.type)) {
			filters.push(e.target.dataset.type);
		}

		e.target.dataset.selected = '1' == e.target.dataset.selected ? '0' : '1';

		this.setState({
			'filters': filters
		});
	},
	updateBlock: function updateBlock() {
		var events = new wp.api.collections.Sp_event();

		events.fetch({
			'data': {
				'context': 'edit',
				'filter': {
					'post_status': ['publish', 'future'],
					'orderby': 'date',
					'order': 'ASC',
					'nopaging': true,
					'meta_key': 'lckrm_event_created_group',
					'meta_value': lckrm_main.group_id
				}
			}
		}).success(function () {
			events.map(function (event) {
				event.start = (0, _moment2.default)(event.get('date').localEquivalent()).toDate();
				event.end = (0, _moment2.default)(event.get('date').localEquivalent()).toDate();
				event.end.setTime(event.end.getTime() + 2 * 60 * 60 * 1000);

				return event;
			});

			this.setState({
				events: events
			});
		}.bind(this));
	},
	renderEvent: function renderEvent(event) {
		var event_type = {};
		var event_title = 'X';

		_.find(this.props.types, function (type) {
			if (-1 !== type.format.indexOf(event.event.get('format'))) {
				event_type = type;
			}
		});

		if (event_type) {
			switch (event_type.letter) {
				case 'G':
					event_title = 'Game';
					break;
				case 'P':
					event_title = 'Practice';
					break;
				case 'M':
					event_title = 'Meeting';
					break;
				default:
					event_title = 'Other';
			}
		}

		return _react2.default.createElement(
			'a',
			{ href: lckrm_main.group_permalink + 'event/?event_id=' + event.event.get('id'), className: 'ui large label ' + (event_type ? event_type.color : ''), title: event.event.get('title').rendered },
			(0, _moment2.default)(event.event.get('date').localEquivalent()).format('h:mmA'),
			_react2.default.createElement(
				'span',
				{ className: 'detachable-event-title' },
				'-',
				event_title
			)
		);
	},
	onDayClick: function onDayClick(date) {
		this.setState({
			'currentDay': date,
			'selectedDate': true,
			'month_numeric': date.getMonth() + 1,
			'year': date.getFullYear()
		});
	},
	render: function render() {
		var _export = null;
		var formats = _.uniq(_.flatten(_.pluck(_.filter(this.props.types, function (type) {
			return -1 !== this.state.filters.indexOf(type.id);
		}.bind(this)), 'format'))),
		    currentDate = new Date(),
		    events = this.state.events.filter(function (event) {
			var filter;

			if (-1 == formats.indexOf(event.get('format'))) {
				filter = false;
			} else {
				filter = true;
			}

			return filter;
		}.bind(this));

		var listEvents = events.filter(function (event) {
			if (this.state.selectedDate) {
				return (0, _moment2.default)(event.get('date').localEquivalent()).toDate().toDateString() === this.state.currentDay.toDateString();
			} else {
				var event_date = event.get('date').moment;
				var event_month = event_date.format('M');
				var event_year = event_date.format('Y');
				// Event is in a different month
				if (event_month != this.state.month_numeric || event_year != this.state.year) {
					return false;
					// Event is within current month and the it has already passed
				} else if (currentDate.getMonth() + 1 == this.state.month_numeric && currentDate.getDate() > event_date.format('D')) {
						return false;
					} else {
						return true;
					}
			}
		}.bind(this));

		if ('' != lckrm_main.calendar_export) {
			_export = _react2.default.createElement(
				'div',
				{ className: 'row export-calendar' },
				_react2.default.createElement(
					'div',
					{ className: 'ui compact menu' },
					_react2.default.createElement(
						'div',
						{ className: 'ui simple dropdown item', ref: 'export' },
						_react2.default.createElement('i', { className: 'calendar icon' }),
						'Export to Calendar',
						_react2.default.createElement(
							'div',
							{ className: 'menu' },
							Object.keys(lckrm_main.calendar_export).map(function (key, i, arr) {
								return _react2.default.createElement(
									'a',
									{ key: 'calendar_' + i, href: lckrm_main.calendar_export[key], target: '_blank', className: 'item' },
									key
								);
							})
						)
					)
				)
			);
		}

		return _react2.default.createElement(
			'div',
			{ className: 'event_react_calendar' },
			_react2.default.createElement(
				'div',
				{ className: 'ui three column grid the-calendar' },
				_react2.default.createElement(
					'div',
					{ className: 'column event-type' },
					this.props.types.map(function (type) {
						return _react2.default.createElement(
							'a',
							{ key: 'filter_type' + type.id, onClick: this.FilterClick, 'data-type': type.id, 'data-selected': -1 != this.state.filters.indexOf(type.id) ? '1' : '0', className: 'ui large circular label ' + (-1 != this.state.filters.indexOf(type.id) ? type.color : 'grey') },
							type.letter
						);
					}.bind(this))
				),
				_react2.default.createElement(
					'div',
					{ className: 'column event-month' },
					_react2.default.createElement(
						'h1',
						null,
						this.props.months[this.state.month_numeric],
						' ',
						this.state.year
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'column event-date-pagi' },
					_react2.default.createElement(
						'a',
						{ href: '#', className: 'previous-date desktop', onClick: this.onPreviousClick },
						_react2.default.createElement('i', { className: 'caret left icon' }),
						'Previous'
					),
					_react2.default.createElement(
						'a',
						{ href: '#', className: 'previous-date mobile', onClick: this.onPreviousClick },
						_react2.default.createElement('i', { className: 'arrow left icon' })
					),
					_react2.default.createElement(
						'a',
						{ href: '#', className: 'next-date desktop', onClick: this.onNextClick },
						'Next',
						_react2.default.createElement('i', { className: 'caret right icon' })
					),
					_react2.default.createElement(
						'a',
						{ href: '#', className: 'next-date mobile', onClick: this.onNextClick },
						_react2.default.createElement('i', { className: 'arrow right icon' })
					)
				)
			),
			_react2.default.createElement(
				'div',
				{ className: 'react-big-calenadr' },
				_react2.default.createElement(_reactBigCalendar2.default, {
					date: new Date(this.state.year, this.state.month_numeric - 1, this.state.currentDay.getDate()),
					events: events,
					defaultDate: new Date(),
					onNavigate: this.onDayClick,
					views: ['month'],
					toolbar: false,
					components: {
						event: this.renderEvent
					}
				})
			),
			_export,
			_react2.default.createElement(
				'div',
				{ className: 'events-list' },
				listEvents.map(function (event) {
					var single = false;
					return _react2.default.createElement(EventMatchBlock, { single: single, types: this.props.types, key: 'event_' + event.get('id'), event: event, updateBlock: this.updateBlock });
				}.bind(this))
			)
		);
	}
});

var EventMatchBlock = _react2.default.createClass({
	displayName: 'EventMatchBlock',

	getInitialState: function getInitialState() {
		return {};
	},
	playersShow: function playersShow(e) {
		e.preventDefault();
		$(_reactDom2.default.findDOMNode(this.refs.attending)).popup({ on: 'click' });
	},
	onClick: function onClick(e) {
		e.preventDefault();

		var event = this.props.event;

		var member_choice = '';
		var event_id = event.attributes.id;
		var target_e = $(_reactDom2.default.findDOMNode(e.target));

		if (target_e.hasClass('attend') || target_e.parent().hasClass('attend')) {
			member_choice = 'attend';
			$(_reactDom2.default.findDOMNode(this.refs.event_attend)).addClass('active');
			$(_reactDom2.default.findDOMNode(this.refs.event_deny)).removeClass('active');
		} else if (target_e.hasClass('deny') || target_e.parent().hasClass('deny')) {
			member_choice = 'deny';
			$(_reactDom2.default.findDOMNode(this.refs.event_deny)).addClass('active');
			$(_reactDom2.default.findDOMNode(this.refs.event_attend)).removeClass('active');
		}

		$.ajax({
			type: 'post',
			url: lckrm_main.ajax,
			data: {
				action: 'maybe_attend_event',
				event_id: event_id,
				member_choice: member_choice,
				group_id: lckrm_main.group_id
			},
			success: function (response) {
				if (true == this.props.single) {
					var event = new wp.api.models.Sp_event({ id: lckrm_main.single_event_id });
					event.fetch().success(function () {
						this.props.updateEvent(event);
					}.bind(this));
				} else {
					this.props.updateBlock();
				}
			}.bind(this)
		});
	},
	render: function render() {

		var event = this.props.event;
		var attendings = 0;
		var attending_players = false;
		var will_attend = '';
		var will_deny = '';

		var event_type = {};

		_.find(this.props.types, function (type) {
			if (-1 !== type.format.indexOf(event.get('format'))) {
				event_type = type;
			}
		});

		Object.keys(event.get('signed_players')).map(function (key, i, arr) {
			if ('attend' == event.get('signed_players')[key].member_choice) {
				return attending_players = true;
			}
		});

		if (true == attending_players && false == this.props.single) {
			var attending_popup = _react2.default.createElement(
				'div',
				{ className: 'ui flowing popup top left transition hidden' },
				Object.keys(event.get('signed_players')).map(function (key, i, arr) {
					if ('attend' == event.get('signed_players')[key].member_choice) {
						return _react2.default.createElement(
							'a',
							{ key: i, className: 'ui image label signed-member' },
							_react2.default.createElement('img', { src: event.get('signed_players')[key].avatar }),
							event.get('signed_players')[key].name
						);
					}
				})
			);
		}

		{
			Object.keys(event.get('signed_players')).map(function (key, i, arr) {
				if ('attend' == event.get('signed_players')[key].member_choice) {
					return attendings++;
				}
			});
		}

		{
			Object.keys(event.get('signed_players')).map(function (key, i, arr) {
				if (lckrm_main.member_id == key) {
					if ('attend' == event.get('signed_players')[key].member_choice) {
						return will_attend = 'active';
					} else {
						return will_deny = 'active';
					}
				}
			});
		}

		return _react2.default.createElement(
			'div',
			{ className: 'event-react-block' },
			_react2.default.createElement(
				'div',
				{ className: 'ui grid mobile' },
				_react2.default.createElement(
					'div',
					{ className: 'four wide column event-type' },
					_react2.default.createElement(
						'a',
						{ href: lckrm_main.group_permalink + 'event/?event_id=' + event.get('id'), className: 'ui large circular label ' + (event_type ? event_type.color : '') },
						event_type ? event_type.letter : 'X'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'four wide column event-info' },
					_react2.default.createElement(
						'h3',
						null,
						_react2.default.createElement(
							'a',
							{ href: lckrm_main.group_permalink + 'event/?event_id=' + event.get('id') },
							event.get('title').rendered
						)
					),
					_react2.default.createElement(
						'p',
						{ className: 'week-day' },
						(0, _moment2.default)(event.get('date').localEquivalent()).format('dddd')
					),
					_react2.default.createElement(
						'p',
						null,
						(0, _moment2.default)(event.get('date').localEquivalent()).format('MMMM Do, YYYY @ h:mm A')
					),
					_react2.default.createElement(
						'a',
						{ href: event.get('venue').term_link },
						_react2.default.createElement('i', { className: 'fa fa-map-marker' }),
						event.get('venue').name
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'four wide column event-attending', ref: 'attending', onMouseEnter: this.playersShow, onMouseLeave: this.playersHide },
					_react2.default.createElement(
						'div',
						{ className: 'ui statistic' },
						_react2.default.createElement(
							'div',
							{ className: 'value' },
							attendings
						),
						_react2.default.createElement(
							'div',
							{ className: 'label' },
							'Attending'
						)
					)
				),
				attending_popup,
				_react2.default.createElement(
					'div',
					{ className: 'four wide column event-rsvp' },
					_react2.default.createElement(
						'div',
						{ className: 'ui buttons' },
						_react2.default.createElement(
							'button',
							{ className: "ui icon button attend " + will_attend, ref: 'event_attend', onClick: this.onClick },
							_react2.default.createElement('i', { className: 'checkmark icon' })
						),
						_react2.default.createElement(
							'button',
							{ className: "ui icon button deny " + will_deny, ref: 'event_deny', onClick: this.onClick },
							_react2.default.createElement('i', { className: 'remove icon' })
						)
					)
				)
			)
		);
	}
});

var ErrorInvite = _react2.default.createClass({
	displayName: 'ErrorInvite',

	render: function render() {
		return _react2.default.createElement(
			'div',
			{ className: 'ui error message' },
			_react2.default.createElement(
				'div',
				{ className: 'header' },
				'Empty Request'
			),
			_react2.default.createElement(
				'p',
				null,
				'Please fill out the form.'
			)
		);
	}
});

var GroupInvite = _react2.default.createClass({
	displayName: 'GroupInvite',

	getInitialState: function getInitialState() {
		return {
			'on_error': 'no',
			'index': 0,
			'new_members': [{
				'name': '',
				'email': '',
				'phone': '',
				'index': 0
			}]
		};
	},
	AdditionalRow: function AdditionalRow(e) {
		e.preventDefault();

		var new_members = this.state.new_members,
		    new_index = this.state.index + 1;

		new_members.push({
			'name': '',
			'email': '',
			'phone': '',
			'index': new_index
		});

		this.setState({
			'index': new_index,
			'new_members': new_members
		});
	},
	onChange: function onChange(index, values) {
		var new_members = this.state.new_members;

		if (undefined != new_members[index]) {
			new_members[index] = values;

			this.setState({
				'new_members': new_members
			});
		}
	},
	RemoveRow: function RemoveRow(index) {
		if (1 < this.state.new_members.length) {
			var new_members = this.state.new_members;

			new_members = new_members.filter(function (el, i) {
				return i !== index;
			});

			this.setState({
				'new_members': new_members
			});
		}
	},
	onFormSubmit: function onFormSubmit(e) {
		e.preventDefault();

		var has_errors = false;

		var invite_btn = $(_reactDom2.default.findDOMNode(this.refs.invite_btn));

		for (var key in this.state.new_members) {
			if (this.state.new_members.hasOwnProperty(key)) {
				var obj = this.state.new_members[key];
				for (var prop in obj) {
					if ('' == this.state.new_members[key].name || '' == this.state.new_members[key].email) {
						has_errors = true;
					}
				}
			}
		}

		if (has_errors) {
			_reactDom2.default.render(_react2.default.createElement(ErrorInvite, null), document.getElementById('on_error_invite'));
		} else {
			_reactDom2.default.unmountComponentAtNode(document.getElementById('on_error_invite'));
			invite_btn.attr('disabled', 'disabled');
			$.ajax({
				type: 'post',
				url: lckrm_main.ajax,
				data: {
					action: 'get_new_members',
					new_members: this.state.new_members,
					team_id: lckrm_main.group_team,
					group_id: lckrm_main.group_id,
					message: this.refs.invitation_message.value
				},
				success: function (response) {
					if (true != response.success) {
						console.log(response.data.response);
					} else {
						_reactDom2.default.unmountComponentAtNode(document.getElementById('react_invite_members'));
						_reactDom2.default.render(_react2.default.createElement(SuccessOnInvite, null), document.getElementById('react_invite_members'));
					}
				}.bind(this)
			});
		}
	},
	render: function render() {
		var message = 'Hey {member_First_Name}, you\'ve been invited to join {Group_Name}. Please click on the link below to register.\n\n{Short_URL}';
		return _react2.default.createElement(
			'div',
			{ className: 'invite-group-members', ref: 'invite_members' },
			_react2.default.createElement('div', { id: 'on_error_invite' }),
			_react2.default.createElement(
				'form',
				{ className: 'ui form' },
				_react2.default.createElement(
					'div',
					{ className: 'ui four column grid' },
					this.state.new_members.map(function (new_member, index) {
						return _react2.default.createElement(NewMemberRow, { key: 'row_' + new_member.index, data: new_member, showAddButton: index + 1 == this.state.new_members.length, showRemoveButton: true, index: index, onAdd: this.AdditionalRow, RemoveRow: this.RemoveRow, onChange: this.onChange });
					}.bind(this))
				),
				_react2.default.createElement(
					'div',
					{ className: 'grid field member-message' },
					_react2.default.createElement('textarea', { ref: 'invitation_message', rows: '4', defaultValue: message })
				),
				_react2.default.createElement(
					'div',
					{ className: 'column' },
					_react2.default.createElement(
						'button',
						{ className: 'ui basic button right floated submit-invite', ref: 'invite_btn', type: 'submit', onClick: this.onFormSubmit },
						_react2.default.createElement('i', { className: 'announcement icon' }),
						'Send Invites'
					)
				)
			)
		);
	}
});

var NewMemberRow = _react2.default.createClass({
	displayName: 'NewMemberRow',

	onChange: function onChange() {
		this.props.onChange(this.props.index, {
			'name': this.refs.name.value,
			'email': this.refs.email.value,
			'phone': this.refs.phone.value,
			'index': this.props.data.index
		});
	},
	onRemove: function onRemove(e) {
		e.preventDefault();
		this.props.RemoveRow(this.props.index);
	},
	render: function render() {
		var button = null,
		    delete_button = null;

		if (this.props.showAddButton) {
			button = _react2.default.createElement(
				'button',
				{ className: 'ui circular blue icon button', ref: 'add_row', onClick: this.props.onAdd },
				_react2.default.createElement('i', { className: 'plus icon' })
			);
		}

		if (this.props.showRemoveButton) {
			delete_button = _react2.default.createElement(
				'button',
				{ className: 'ui circular red icon button', ref: 'add_row', 'data-index': this.props.index, onClick: this.onRemove },
				_react2.default.createElement('i', { className: 'remove icon' })
			);
		}

		return _react2.default.createElement(
			'div',
			{ className: 'row', ref: 'invite_row' },
			_react2.default.createElement(
				'div',
				{ className: 'column' },
				_react2.default.createElement(
					'div',
					{ className: 'ui icon input' },
					_react2.default.createElement('input', { type: 'text', ref: 'name', placeholder: 'Name', onChange: this.onChange, defaultValue: this.props.data.name }),
					_react2.default.createElement('i', { className: 'add user icon' })
				)
			),
			_react2.default.createElement(
				'div',
				{ className: 'column' },
				_react2.default.createElement(
					'div',
					{ className: 'ui icon input' },
					_react2.default.createElement('input', { type: 'text', ref: 'email', placeholder: 'Email', onChange: this.onChange, defaultValue: this.props.data.email }),
					_react2.default.createElement('i', { className: 'mail outline icon' })
				)
			),
			_react2.default.createElement(
				'div',
				{ className: 'column' },
				_react2.default.createElement(
					'div',
					{ className: 'ui right icon cheat left icon input' },
					_react2.default.createElement('i', { className: 'plus icon' }),
					_react2.default.createElement('input', { type: 'text', ref: 'phone', placeholder: 'Cell #', onChange: this.onChange, defaultValue: '1' }),
					_react2.default.createElement('i', { className: 'phone icon' })
				)
			),
			_react2.default.createElement(
				'div',
				{ className: 'column' },
				delete_button,
				button
			)
		);
	}
});

var SingleEventParent = _react2.default.createClass({
	displayName: 'SingleEventParent',

	getInitialState: function getInitialState() {
		return {
			event: this.props.event
		};
	},
	handleEventUpdate: function handleEventUpdate(event) {
		this.setState({
			event: event
		});
	},
	onEditClick: function onEditClick(e) {
		e.preventDefault();
		var edit_button = $(_reactDom2.default.findDOMNode(this.refs.edit_btn));
		if (edit_button.hasClass('visible')) {
			edit_button.removeClass('visible');
			edit_button.children().last().removeClass('fa-caret-up');
			edit_button.children().last().addClass('fa-caret-down');
			_reactDom2.default.unmountComponentAtNode(document.getElementById('edit_single_event_form'));
		} else {
			_reactDom2.default.render(_react2.default.createElement(EventForm, { event: this.state.event, types: this.props.types, updateEvent: this.handleEventUpdate }), document.getElementById('edit_single_event_form'));

			edit_button.addClass('visible');
			edit_button.children().last().removeClass('fa-caret-down');
			edit_button.children().last().addClass('fa-caret-up');
		}
	},
	onListsClick: function onListsClick(e) {
		e.preventDefault();
		var lists_btn = $(_reactDom2.default.findDOMNode(this.refs.rsvpBtn));
		if (lists_btn.hasClass('visible')) {
			lists_btn.removeClass('visible');
			lists_btn.children().last().removeClass('fa-caret-up');
			lists_btn.children().last().addClass('fa-caret-down');
			_reactDom2.default.unmountComponentAtNode(document.getElementById('event_rsvp_lists'));
		} else {
			var rsvpd_players = [];
			var event = this.state.event;
			{
				Object.keys(event.get('signed_players')).map(function (key, i, arr) {
					return rsvpd_players.push(parseInt(key, 10));
				});
			}

			var users = new wp.api.collections.Users();
			users.fetch({
				'data': {
					'lckrm_users_for_group': lckrm_main.group_id,
					'per_page': 100,
					'filter': {
						'include': _.difference(lckrm_main.group_members, rsvpd_players)
					}
				}
			}).success(function () {
				_reactDom2.default.render(_react2.default.createElement(EventRsvpLists, { event: event, not_rsvpd: users }), document.getElementById('event_rsvp_lists'));
			});

			lists_btn.addClass('visible');
			lists_btn.children().last().removeClass('fa-caret-down');
			lists_btn.children().last().addClass('fa-caret-up');
		}
	},
	render: function render() {
		var event = this.state.event;
		var edit_event = null;
		var rsvpd_players = [];

		if ('yes' == lckrm_main.group_moderator) {
			edit_event = _react2.default.createElement(
				'a',
				{ href: '#', className: 'edit-single-event', ref: 'edit_btn', onClick: this.onEditClick },
				_react2.default.createElement('i', { className: 'write icon' }),
				'Edit ',
				_react2.default.createElement('i', { className: 'fa fa-caret-down' })
			);
		}

		{
			Object.keys(event.get('signed_players')).map(function (key, i, arr) {
				return rsvpd_players.push(parseInt(key, 10));
			});
		}

		var users = new wp.api.collections.Users();
		users.fetch({
			'data': {
				'lckrm_users_for_group': lckrm_main.group_id,
				'per_page': 100,
				'filter': {
					'include': _.difference(lckrm_main.group_members, rsvpd_players)
				}
			}
		}).success(function () {
			_reactDom2.default.render(_react2.default.createElement(EventRsvpLists, { event: event, not_rsvpd: users }), document.getElementById('event_rsvp_lists'));
		});

		return _react2.default.createElement(
			'div',
			null,
			_react2.default.createElement(EventMatchBlock, { single: this.props.single, event: this.state.event, types: this.props.types, updateEvent: this.handleEventUpdate }),
			edit_event,
			_react2.default.createElement(
				'a',
				{ href: '#', className: 'rsvp-lists visible', ref: 'rsvpBtn', onClick: this.onListsClick },
				_react2.default.createElement('i', { className: 'users icon' }),
				'RSVP Info ',
				_react2.default.createElement('i', { className: 'fa fa-caret-up' })
			),
			_react2.default.createElement('div', { id: 'edit_single_event_form', className: 'single-event-form' }),
			_react2.default.createElement('div', { id: 'event_rsvp_lists', className: 'event-rsvps-lists' })
		);
	}
});

var SuccessOnInvite = _react2.default.createClass({
	displayName: 'SuccessOnInvite',

	onCloseClick: function onCloseClick() {
		$(_reactDom2.default.findDOMNode(this.refs.SuccessMsg)).fadeOut('slow');
	},
	render: function render() {
		return _react2.default.createElement(
			'div',
			{ className: 'invite-created', ref: 'SuccessMsg' },
			_react2.default.createElement(
				'div',
				{ className: 'ui success message transition' },
				_react2.default.createElement('i', { className: 'close icon', onClick: this.onCloseClick }),
				_react2.default.createElement(
					'div',
					{ className: 'header' },
					'Invitations were sent successfully.'
				)
			)
		);
	}
});

var Member = _react2.default.createClass({
	displayName: 'Member',

	getInitialState: function getInitialState() {
		return {
			user: this.props.user,
			text: false,
			mail: false
		};
	},
	initFormText: function initFormText(e) {
		e.preventDefault();

		var form = $(_reactDom2.default.findDOMNode(this.refs.textForm));
		var target = $(_reactDom2.default.findDOMNode(this.refs.textBtn));
		var mail_btn = $(_reactDom2.default.findDOMNode(this.refs.mailBtn));

		if (!target.hasClass('button')) {
			target = target.parent();
		}

		if (target.hasClass('active')) {
			target.removeClass('active');
			if (!mail_btn.hasClass('active')) {
				form.slideUp("fast", function () {}).css('display', 'none').removeClass('visible');
			}
			this.setState({
				'text': false
			});
		} else {
			target.addClass('active');
			if (!mail_btn.hasClass('active')) {
				form.slideDown("fast", function () {}).css('display', 'block').addClass('visible');
			}
			this.setState({
				'text': true
			});
		}
	},
	initFormMail: function initFormMail(e) {
		e.preventDefault();

		var form = $(_reactDom2.default.findDOMNode(this.refs.textForm));
		var target = $(_reactDom2.default.findDOMNode(this.refs.mailBtn));
		var text_btn = $(_reactDom2.default.findDOMNode(this.refs.textBtn));

		if (!target.hasClass('button')) {
			target = target.parent();
		}

		if (target.hasClass('active')) {
			target.removeClass('active');
			if (!text_btn.hasClass('active')) {
				form.slideUp("fast", function () {}).css('display', 'none').removeClass('visible');
			}
			this.setState({
				'mail': false
			});
		} else {
			target.addClass('active');
			if (!text_btn.hasClass('active')) {
				form.slideDown("fast", function () {}).css('display', 'block').addClass('visible');
			}
			this.setState({
				'mail': true
			});
		}
	},
	submitMessages: function submitMessages(e) {
		e.preventDefault();
		var submit_btn = $(_reactDom2.default.findDOMNode(this.refs.submitMsg));
		var textarea = this.refs.member_message.value;
		var form = $(_reactDom2.default.findDOMNode(this.refs.textForm));
		var mail_btn = $(_reactDom2.default.findDOMNode(this.refs.mailBtn));
		var text_btn = $(_reactDom2.default.findDOMNode(this.refs.textBtn));

		if ('' != textarea) {
			submit_btn.attr('disabled', 'disabled');

			$.post(lckrm_main.ajax, { action: 'direct_memeber_messages', user_id: this.state.user.attributes.id, text: this.state.text, mail: this.state.mail, msg: textarea, group_id: lckrm_main.group_id }, function (r) {
				if (r.success) {
					form.slideUp("fast", function () {}).css('display', 'none').removeClass('visible');
					submit_btn.removeAttr('disabled');
					mail_btn.removeClass('active');
					text_btn.removeClass('active');
				}
			}.bind(this));
		}
	},
	render: function render() {
		var user = this.state.user;
		var msg_buttons = null;
		var no_phone = user.attributes.cellphone ? false : true;

		if ('yes' == lckrm_main.group_moderator) {
			msg_buttons = _react2.default.createElement(
				'div',
				{ className: 'ui two column grid' },
				_react2.default.createElement(
					'div',
					{ className: 'column' },
					_react2.default.createElement(
						'button',
						{ className: 'ui icon button send-text', ref: 'textBtn', onClick: this.initFormText, disabled: no_phone },
						_react2.default.createElement('i', { className: 'phone icon' })
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'column' },
					_react2.default.createElement(
						'button',
						{ className: 'ui icon button send-mail', ref: 'mailBtn', onClick: this.initFormMail },
						_react2.default.createElement('i', { className: 'mail outline icon' })
					)
				)
			);
		}

		return _react2.default.createElement(
			'div',
			{ className: 'ui grid' },
			_react2.default.createElement(
				'div',
				{ className: 'thirteen wide column' },
				_react2.default.createElement(
					'a',
					{ href: lckrm_main.home_permalink + 'members/' + user.attributes.slug },
					_react2.default.createElement('img', { className: 'ui middle aligned tiny image', src: user.attributes.avatar_urls[96] })
				),
				_react2.default.createElement(
					'a',
					{ href: lckrm_main.home_permalink + 'members/' + user.attributes.slug, className: 'user-nick' },
					user.attributes.name
				)
			),
			_react2.default.createElement(
				'div',
				{ className: 'three wide column' },
				msg_buttons
			),
			_react2.default.createElement(
				'div',
				{ className: 'ui form', ref: 'textForm' },
				_react2.default.createElement(
					'div',
					{ className: 'grid field member-message' },
					_react2.default.createElement('textarea', { ref: 'member_message', rows: '2' })
				),
				_react2.default.createElement(
					'button',
					{ className: 'ui basic button right floated', ref: 'submitMsg', onClick: this.submitMessages },
					_react2.default.createElement('i', { className: 'send outline icon' }),
					'Send'
				)
			)
		);
	}
});

var GroupMembers = _react2.default.createClass({
	displayName: 'GroupMembers',

	getInitialState: function getInitialState() {
		return {
			users: this.props.users,
			mail: false,
			text: false
		};
	},
	componentDidMount: function componentDidMount() {
		if (this.refs.textBtn) {
			$(_reactDom2.default.findDOMNode(this.refs.textBtn)).popup({ popup: $(_reactDom2.default.findDOMNode(this.refs.textPopup)), on: 'click' });
		}

		if (this.refs.mailBtn) {
			$(_reactDom2.default.findDOMNode(this.refs.mailBtn)).popup({ popup: $(_reactDom2.default.findDOMNode(this.refs.mailPopup)), on: 'click' });
		}
	},
	ShowTextPop: function ShowTextPop(e) {
		e.preventDefault();
	},
	ShowMailPop: function ShowMailPop(e) {
		e.preventDefault();
	},
	render: function render() {
		return _react2.default.createElement(
			'div',
			{ className: 'group-members' },
			this.state.users.map(function (user, index) {
				if (undefined != user) {

					return _react2.default.createElement(Member, { key: 'row_' + index, user: user });
				}
			}.bind(this))
		);
	}
});

var EventRsvpLists = _react2.default.createClass({
	displayName: 'EventRsvpLists',

	getInitialState: function getInitialState() {
		return {
			all_attending: false,
			attending: '',
			all_not_attending: false,
			not_attending: '',
			all_no_response: false,
			no_response: ''

		};
	},
	componentDidMount: function componentDidMount() {
		var event = this.props.event;
		var not_rsvpd = this.props.not_rsvpd;
		var _attending = null;
		var _no_attending = null;
		var _no_response = null;

		{
			Object.keys(event.get('signed_players')).map(function (key, i, arr) {
				if ('attend' == event.get('signed_players')[key].member_choice) {
					_attending = i + 1;
				}
			});
		}

		{
			Object.keys(event.get('signed_players')).map(function (key, i, arr) {
				if ('deny' == event.get('signed_players')[key].member_choice) {
					_no_attending = i + 1;
				}
			});
		}

		{
			not_rsvpd.map(function (user, i, arr) {
				_no_response = i + 1;
			});
		}

		this.setState({
			attending: _attending,
			not_attending: _no_attending,
			no_response: _no_response
		});
	},
	onMoreNoResponse: function onMoreNoResponse(e) {
		e.preventDefault();
		this.setState({
			all_no_response: true
		});
	},
	onMoreNotAttending: function onMoreNotAttending(e) {
		e.preventDefault();
		this.setState({
			all_not_attending: true
		});
	},
	onMoreAttending: function onMoreAttending(e) {
		e.preventDefault();
		this.setState({
			all_attending: true
		});
	},
	render: function render() {
		var event = this.props.event;
		var not_rsvpd = this.props.not_rsvpd;
		var attend_item = null;
		var declined_item = null;
		var response_item = null;

		if (11 <= this.state.no_response && !this.state.all_no_response) {
			response_item = _react2.default.createElement(
				'div',
				{ className: 'item show_more' },
				_react2.default.createElement(
					'div',
					{ className: 'content' },
					_react2.default.createElement(
						'a',
						{ href: '#', onClick: this.onMoreNoResponse },
						'Show ',
						this.state.no_response - 10,
						' more'
					)
				)
			);
		}
		if (11 <= this.state.not_attending && !this.state.all_not_attending) {
			declined_item = _react2.default.createElement(
				'div',
				{ className: 'item show_more' },
				_react2.default.createElement(
					'div',
					{ className: 'content' },
					_react2.default.createElement(
						'a',
						{ href: '#', onClick: this.onMoreNotAttending },
						'Show ',
						this.state.not_attending - 10,
						' more'
					)
				)
			);
		}
		if (11 <= this.state.attending && !this.state.all_attending) {
			attend_item = _react2.default.createElement(
				'div',
				{ className: 'item show_more' },
				_react2.default.createElement(
					'div',
					{ className: 'content' },
					_react2.default.createElement(
						'a',
						{ href: '#', onClick: this.onMoreAttending },
						'Show ',
						this.state.attending - 10,
						' more'
					)
				)
			);
		}

		return _react2.default.createElement(
			'div',
			{ className: 'ui stackable three column grid' },
			_react2.default.createElement(
				'div',
				{ className: 'column' },
				_react2.default.createElement(
					'div',
					{ className: 'attending-members' },
					_react2.default.createElement(
						'div',
						{ className: 'ui small header' },
						'Attending'
					),
					_react2.default.createElement(
						'div',
						{ className: 'ui middle aligned divided list' },
						Object.keys(event.get('signed_players')).map(function (key, i, arr) {
							if ('attend' == event.get('signed_players')[key].member_choice) {
								if (i <= 9 || this.state.all_attending) {
									return _react2.default.createElement(
										'div',
										{ key: i, className: 'item' },
										_react2.default.createElement('img', { className: 'ui avatar image', src: event.get('signed_players')[key].avatar }),
										_react2.default.createElement(
											'div',
											{ className: 'content' },
											_react2.default.createElement(
												'a',
												{ href: event.get('signed_players')[key].link, className: 'header' },
												event.get('signed_players')[key].name
											)
										)
									);
								}
							}
						}.bind(this)),
						attend_item
					)
				)
			),
			_react2.default.createElement(
				'div',
				{ className: 'column' },
				_react2.default.createElement(
					'div',
					{ className: 'not-attending-members' },
					_react2.default.createElement(
						'div',
						{ className: 'ui small header' },
						'Declined'
					),
					_react2.default.createElement(
						'div',
						{ className: 'ui middle aligned divided list' },
						Object.keys(event.get('signed_players')).map(function (key, i, arr) {
							if ('deny' == event.get('signed_players')[key].member_choice) {
								if (i <= 9 || this.state.all_not_attending) {
									return _react2.default.createElement(
										'div',
										{ key: i, className: 'item' },
										_react2.default.createElement('img', { className: 'ui avatar image', src: event.get('signed_players')[key].avatar }),
										_react2.default.createElement(
											'div',
											{ className: 'content' },
											_react2.default.createElement(
												'a',
												{ href: event.get('signed_players')[key].link, className: 'header' },
												event.get('signed_players')[key].name
											)
										)
									);
								}
							}
						}.bind(this)),
						declined_item
					)
				)
			),
			_react2.default.createElement(
				'div',
				{ className: 'column' },
				_react2.default.createElement(
					'div',
					{ className: 'not-rsvpd-members' },
					_react2.default.createElement(
						'div',
						{ className: 'ui small header' },
						'No Response'
					),
					_react2.default.createElement(
						'div',
						{ className: 'ui middle aligned divided list' },
						not_rsvpd.map(function (user, i, arr) {
							if (i <= 9 || this.state.all_no_response) {
								return _react2.default.createElement(
									'div',
									{ key: i, className: 'item' },
									_react2.default.createElement('img', { className: 'ui avatar image', src: user.attributes.avatar_urls[96] }),
									_react2.default.createElement(
										'div',
										{ className: 'content' },
										_react2.default.createElement(
											'a',
											{ href: lckrm_main.home_permalink + 'members/' + user.attributes.slug, className: 'header' },
											user.attributes.name
										)
									)
								);
							}
						}.bind(this)),
						response_item
					)
				)
			)
		);
	}
});

(function ($) {
	$(document).ready(function () {
		var months = { 1: "January", 2: "February", 3: "March", 4: "April", 5: "May", 6: "June", 7: "July", 8: "August", 9: "September", 10: "October", 11: "November", 12: "December" },
		    event_types = [{
			'id': 'game',
			'color': 'blue',
			'letter': 'G',
			'format': ['league', 'friendly']
		}, {
			'id': 'practice',
			'color': 'green',
			'letter': 'P',
			'format': ['practice']
		}, {
			'id': 'meeting',
			'color': 'red',
			'letter': 'M',
			'format': ['meeting']
		}, {
			'id': 'other',
			'color': 'yellow',
			'letter': 'O',
			'format': ['other']
		}],
		    events,
		    event;

		if ($('#event_react_calendar').length) {
			events = new wp.api.collections.Sp_event();
			events.fetch({
				'data': {
					'context': 'view',
					'filter': {
						'post_status': ['publish', 'future'],
						'orderby': 'date',
						'order': 'ASC',
						'nopaging': true,
						'meta_key': 'lckrm_event_created_group',
						'meta_value': lckrm_main.group_id
					}
				}
			}).success(function () {
				events.map(function (event) {
					event.start = (0, _moment2.default)(event.get('date').localEquivalent()).toDate();
					event.end = (0, _moment2.default)(event.get('date').localEquivalent()).toDate();
					event.end.setTime(event.end.getTime() + 2 * 60 * 60 * 1000);

					return event;
				});

				_reactDom2.default.render(_react2.default.createElement(EventCalendar, { events: events, types: event_types, months: months }), document.getElementById('event_react_calendar'));
			});
		}

		$('.btn-new-event').on('click', function (event) {
			event.preventDefault();
			if ($(this).closest('#calendar-body').find('.create-event-container').hasClass('visible')) {
				_reactDom2.default.unmountComponentAtNode(document.getElementById('event_react_container'));
				$(this).closest('#calendar-body').find('.create-event-container').removeClass('visible');
			} else {
				$(this).closest('#calendar-body').find('.create-event-container').addClass('visible');
				_reactDom2.default.render(_react2.default.createElement(EventForm, { types: event_types, events: events }), document.getElementById('event_react_container'));
			}
		});

		if ($('#event_single_block').length) {
			event = new wp.api.models.Sp_event({ id: lckrm_main.single_event_id });
			event.fetch().success(function () {
				var single = true;
				_reactDom2.default.render(_react2.default.createElement(SingleEventParent, { single: single, event: event, types: event_types }), document.getElementById('event_single_block'));
			});
		}

		if ($('#react_group_members').length) {
			var users = new wp.api.collections.Users();
			users.fetch({
				'data': {
					'lckrm_users_for_group': lckrm_main.group_id,
					'per_page': 100,
					'orderby': 'name',
					'order': 'asc',
					'filter': {
						// 'include': lckrm_main.group_members
					}
				}
			}).success(function () {
				var group_members = users;

				_reactDom2.default.render(_react2.default.createElement(GroupMembers, { users: group_members }), document.getElementById('react_group_members'));
			});
		}

		$('.btn-new-invite').on('click', function (event) {
			event.preventDefault();
			if ($(this).closest('#content-body').find('#react_invite_members').hasClass('visible')) {
				_reactDom2.default.unmountComponentAtNode(document.getElementById('react_invite_members'));
				$(this).closest('#content-body').find('#react_invite_members').removeClass('visible');
			} else {
				$(this).closest('#content-body').find('#react_invite_members').addClass('visible');
				_reactDom2.default.render(_react2.default.createElement(GroupInvite, null), document.getElementById('react_invite_members'));
			}
		});

		if ($('#reac_invited_members').length) {
			_reactDom2.default.render(_react2.default.createElement(GroupInvitedMembers, null), document.getElementById('reac_invited_members'));
		}
	});
})(jQuery);

var DateWithTimezone = function DateWithTimezone(moment) {
	this.moment = moment;
	moment.tz(DateWithTimezone.getTimezone());
};
DateWithTimezone.timezone = 'America/Detroit';

_.extend(DateWithTimezone.prototype, {

	toISO: function toISO() {
		return this.format();
	},

	format: function format(_format) {
		return this.moment.format(_format);
	},

	localEquivalent: function localEquivalent() {
		return (0, _moment2.default)(this.format(), "YYYY-MM-DDTHH:mm:ss").toDate();
	},

	add: function add(field, value) {
		var cloned = this.moment.clone();
		cloned.add(field, value);
		return new DateWithTimezone(cloned);
	},

	hours: function hours() {
		return this.moment.hours();
	},

	minutes: function minutes() {
		return this.moment.minutes();
	},

	isLaterThan: function isLaterThan(compareTo) {
		return this.moment.valueOf() > compareTo.moment.valueOf();
	}
});

_.extend(DateWithTimezone, {
	fromISO: function fromISO(dateTimeString) {
		return new DateWithTimezone((0, _moment2.default)(dateTimeString));
	},

	fromLocalEquivalent: function fromLocalEquivalent(localEquivalent) {
		var strWithoutTimezone = (0, _moment2.default)(localEquivalent).format("YYYY-MM-DDTHH:mm:ss");
		var timezone = _moment2.default.tz(DateWithTimezone.getTimezone()).format("Z");
		return new DateWithTimezone((0, _moment2.default)(strWithoutTimezone + timezone));
	},

	getTimezone: function getTimezone() {
		return DateWithTimezone.timezone || "Etc/UTC";
	},

	now: function now() {
		return new DateWithTimezone((0, _moment2.default)());
	}
});

// Override the default toJSON() and parse() methods as they don't work
// well when the client uses a different timezone than the server
wp.api.models.Sp_event = wp.api.models.Sp_event.extend({
	'_parseableDates': ['date', 'modified', 'date_gmt', 'modified_gmt'],
	/**
  * Serialize the entity pre-sync.
  *
  * @returns {*}.
  */
	toJSON: function toJSON() {
		var attributes = _.clone(this.attributes);

		// Serialize Date objects back into 8601 strings.
		_.each(this._parseableDates, function (key) {
			if (key in attributes) {

				// Don't convert null values
				if (!_.isNull(attributes[key])) {
					attributes[key] = (0, _moment2.default)(attributes[key]).format("YYYY-MM-DDTHH:mm:ss");

					var timezone = _moment2.default.tz(DateWithTimezone.getTimezone()).format("Z");
					attributes[key] = new DateWithTimezone((0, _moment2.default)(attributes[key] + timezone)).toISO();
				}
			}
		});

		return attributes;
	},

	/**
  * Unserialize the fetched response.
  *
  * @param {*} response.
  * @returns {*}.
  */
	parse: function parse(response) {
		var timestamp;

		// Parse dates into native Date objects.
		_.each(this._parseableDates, function (key) {
			if (!(key in response)) {
				return;
			}

			// Don't convert null values
			if (!_.isNull(response[key])) {
				// timestamp = wp.api.utils.parseISO8601( response[ key ] );
				response[key] = DateWithTimezone.fromISO(response[key]);
			}
		});

		return response;
	}
});

// Set the model used by the Sp_event collection
wp.api.collections.Sp_event = wp.api.collections.Sp_event.extend({
	'model': wp.api.models.Sp_event
});