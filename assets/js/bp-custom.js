(function($) {
	function sp_maps() {
		$maps = $('.sp-google-map');
		$maps.each(function() {
			$self = $(this);
			address = $self.attr('data-address');
			latitude = $self.attr('data-latitude');
			longitude = $self.attr('data-longitude');
			var ll = new google.maps.LatLng(latitude,longitude);
			var mapOptions = {
				scrollwheel: false,
				zoom: parseInt(vars.zoom),
				center: ll,
				mapTypeId: google.maps.MapTypeId[vars.map_type]
			};
			var map = new google.maps.Map($self[0], mapOptions)
			var marker = new google.maps.Marker({
				position: ll,
				map: map,
				animation: google.maps.Animation.DROP,
				flat: true,
				title: address
			});
		});
	}

	$( document ).ready(function() {
		if ( $('.ui.success.message.unsubscribed').length ) {
			$('.ui.success.message.unsubscribed .close').on('click', function() {
				$(this).closest('.message').remove();
			});
		}

		if ( $('#mass-message-form').length ) {
			$('body').append('<div class="ui small modal" id="mass_message_modal"><div class="content"></div></div>');

			var $mass_message_form = $('#mass-message-form'),
				$mass_message_textarea = $('#mass-message-form #mass-message'),
				$mass_message_modal = $('#mass_message_modal').modal({
					'transition': 'scale',
					'blurring'  : true
				}),
				$mass_message_send_notification = $('#mass_message_send_notifications'),
				$mass_message_loading_overlay = $('#mass-message-loading-overlay'),
				mass_message_doing_ajax = false;

			$mass_message_form.on('submit', function(e){
				e.preventDefault();

				if ( mass_message_doing_ajax ) {
					return;
				}

				var message = $.trim( $mass_message_textarea.val() ),
					data = $mass_message_form.serialize();

				if ( ! message ) {
					$mass_message_textarea.on('keyup', remove_error_class_on_keyup);
					$mass_message_textarea.parent().addClass('error');
					$mass_message_modal.find('.content').html('<div class="ui negative message">Please enter a message.</div>');
					$mass_message_modal.modal('show');
				} else {
					mass_message_doing_ajax = true;
					$mass_message_loading_overlay.addClass('active');

					$.post($mass_message_form.attr('action'), data, function(res){
						if ( res.success ) {
							$mass_message_modal.find('.content').html(res.data.message);
							$mass_message_textarea.val('');
							$mass_message_send_notification.attr('checked', 'checked');
						} else {
							$mass_message_modal.find('.content').html('<div class="ui negative message">' + res.data.message + '</div>');
						}

						$mass_message_modal.modal('show');
					}, 'json').fail(function(){
						$mass_message_modal.find('.content').html('<div class="ui negative message">Something went wrong. Please refresh the page and try again.</div>');
						$mass_message_modal.modal('show');
					}).always(function(){
						$mass_message_loading_overlay.removeClass('active');
						mass_message_doing_ajax = false;
					});
				}
			});
		}

		//Notifications related updates
		setTimeout(function(){
			$(document).on('heartbeat-tick.bb_notification_count', function(event, data) {
				if (data.hasOwnProperty('bb_notification_count')) {
					data = data['bb_notification_count'];
					/********notification type**********/
					if (data.notification_real > 0) { //has count
						$("#ab-pending-notifications").text(data.notification).removeClass("no-alert");
						$(".mobile-header-inner #ab-pending-notifications").first().text(data.notification).removeClass("no-alert");
						
						$(".ab-item[href*='/notifications/']").each(function() {
							if ($(this).find("span").attr("id") != "ab-pending-notifications") {
								$(this).find(".count").addClass('ui grey circular label count').css('float', 'right').text(data.notification_real);
							}
						});

						$('.custom-bp-user-nav-widget .item-notifications .count').text(data.notification_real);
					} else {
						$("#ab-pending-notifications").text(data.notification).addClass("no-alert");
						$(".mobile-header-inner #ab-pending-notifications").text(data.notification).addClass("no-alert");
						$(".ab-item[href*='/notifications/']").each(function() {
							if ( ! $(this).find('.count') ) {
								$(this).append('<span class="count"/>');
							}
							if ($(this).find("span").attr("id") != "ab-pending-notifications") {
							 $(this).find(".count").addClass('ui grey circular label count').css('float', 'right').text('0');
							}
						});
						$('.custom-bp-user-nav-widget .item-notifications .count').text('0');
					}
					
					$("#user-notifications").find("span").text(data.notification_real);
					
					//remove from read ..
					$("#wp-admin-bar-my-account-notifications-read").each(function() {
						$(this).find("a").find(".count").remove();
					});
					
					/**********messages type************/
					if (data.unread_message > 0) { //has count
						$("#user-messages").find("span").text(data.unread_message);
						$(".ab-item[href*='/messages/']").each(function() {
							$(this).find(".count").addClass('ui grey circular label count').css('float', 'right').text(data.unread_message);
						});
						$('.custom-bp-user-nav-widget .item-messages .count').text(data.unread_message);
					} else {
						$("#user-messages").find("span").text(data.unread_message);
						$(".ab-item[href*='/messages/']").each(function() {
							$(this).find(".count").addClass('ui grey circular label count').css('float', 'right').text('0');
						});
					}
					//remove from unwanted place ..
					$("#wp-admin-bar-my-account-messages-default, #wp-admin-bar-my-account-messages-default").find("li:not('#wp-admin-bar-my-account-messages-inbox')").each(function() {
						$(this).find("span").remove();
					});
					/**********messages type************/
					if (data.friend_request > 0) { //has count
						$(".ab-item[href*='/friends/']").each(function() {
							$(this).find(".count").text(data.friend_request);
						});
					} else {
						$(".ab-item[href*='/friends/']").each(function() {
							$(this).find(".count").addClass('ui grey circular label count').css('float', 'right').text('0');
						});
					}
					//remove from unwanted place ..
					$("#wp-admin-bar-my-account-friends-default,#wp-admin-bar-my-account-friends-default").find("li:not('#wp-admin-bar-my-account-friends-requests')").each(function() {
						$(this).find("span").remove();
					});
					
					//notification content
					$("#wp-admin-bar-bp-notifications .ab-sub-wrapper").find("li").first().html(data.notification_content);
				}
			});
		}, 100);
	});

	function remove_error_class_on_keyup() {
		$(this).parent().removeClass('error');
		$(this).off('keyup', 'remove_error_class_on_keyup');
	}
})(jQuery);