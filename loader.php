<?php
/*
Plugin Name: LCKRM
Plugin URI: http://lckrm.ca/
Description: LCKRM is a social sports management platform that allows coaches, players and parents to communicate, organize events, and collect money online.
Version: 1.0.0
Author: LCKRM
Author URI: http://lckrm.ca/
License: GPLv2
*/

if ( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'LCKRM_Framework' ) ) :


/**
 * LCKRM_Framework class.
 */
class LCKRM_Framework {


    /**
     * instance function.
     *
     * @access public
     * @static
     * @return \LCKRM_Framework $instance
     */
	public static function instance() {

		// Store the instance locally to avoid private static replication
		static $instance = null;

		// Only run these methods if they haven't been run previously
		if ( null === $instance ) {
			$instance = new LCKRM_Framework;
			$instance->actions();
		}

		// Always return the instance
		return $instance;
	}


    /**
     * __construct function.
     *
     * @access private
     * @return \LCKRM_Framework
     */
	private function __construct() { /* Do nothing here */ }


	/**
	 * actions function.
	 *
	 * @access private
	 * @return void
	 */
	private function actions() {


		add_action( 'bp_include', array( $this, 'includes' ) );

        // these are for template file overrides.
		add_action( 'bp_register_theme_packages', array( $this, 'bp_custom_templatepack_work' ) );
		add_filter( 'pre_option__bp_theme_package_id', array( $this, 'bp_custom_templatepack_package_id' ) );
		add_action( 'wp', array( $this, 'bp_templatepack_kill_legacy_js_and_css' ), 999 );
		
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}


	/**
	 * inc function.
	 *
	 * @access public
	 * @return void
	 */
	public function includes() {
        // to include a file place it in the inc directory
        foreach( glob(  plugin_dir_path(__FILE__) . 'inc/*.php' ) as $filename ) {
            include $filename;
        }
	}
	
	
	/**
	 * enqueue_scripts function.
	 * 
	 * @access public
	 * @return void
	 */
	public function enqueue_scripts() {
		
	}


	/**
	 * templatepack_work function.
	 *
	 * @access public
	 * @return void
	 */
	public function bp_custom_templatepack_work() {

		/*bp_register_theme_package( array(
				'id'		 => 'templates',
				'name'	 => __( 'BuddyPress Templates', 'buddypress' ),
				'version' => bp_get_version(),
				'dir'	 => plugin_dir_path( __FILE__ ) . '/templates',
				'url'	 => plugin_dir_url( __FILE__ ) . '/templates'
			) );*/

		bp_register_template_stack( function(){
			return plugin_dir_path( __FILE__ ) . '/templates';
		}, 11 );

	}


	/**
	 * templatepack_package_id function.
	 *
	 * @access public
	 * @param mixed $package_id
	 * @return void
	 */
	public function bp_custom_templatepack_package_id( $package_id ) {
		return 'templates';
	}


	// Proposed BP core change: see http://buddypress.trac.wordpress.org/ticket/3741#comment:43
	/**
	 * templatepack_kill_legacy_js_and_css function.
	 *
	 * @access public
	 * @return void
	 */
	public function bp_templatepack_kill_legacy_js_and_css() {
		wp_dequeue_script( 'groups_widget_groups_list-js' );
		wp_dequeue_script( 'bp_core_widget_members-js' );
	}

	public function plugin_path() {
		return plugin_dir_path( __FILE__ );
	}

	public function plugin_file() {
		return __FILE__;
	}
}


/**
 * bp_custom_template_stack function.
 *
 * @access public
 * @return void
 */
function lckrm_framework() {
	return LCKRM_Framework::instance();
}
    lckrm_framework();

endif;
