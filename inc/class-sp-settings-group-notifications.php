<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists('SP_Settings_Page')) 
    return;

if ( ! class_exists( 'SP_settings_Group_Notifications' ) ) :

/**
 * SP_settings_Group_Notifications
 */
class SP_settings_Group_Notifications extends SP_Settings_Page {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->id    = 'notifications';
		$this->label = __( 'Group Notifications', 'sportspress' );

		add_filter( 'sportspress_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );
		add_action( 'sportspress_settings_' . $this->id, array( $this, 'output' ) );
		add_action( 'sportspress_settings_save_' . $this->id, array( $this, 'save' ) );
	}

	/**
	 * Get settings array
	 *
	 * @return array
	 */
	public function get_settings() {
		
		$settings = array(
			array( 'title' => __( 'Notifications', 'sportspress' ), 'type' => 'title', 'desc' => __( 'The following options affect how email and SMS content is displayed. Usable variables: {member_First_Name}, {Event_Name}, {Event_Date}, {Event_Time}, {Event_Venue}, {Short_URL}, {Group_Name}, {Team_Message}', 'sportspress' ), 'id' => 'notifications_options' ),
		);


		$types = array(
			'New Event',
			'Event Reminder',
			'RSVP NAG',
			'Edited Event',
			'Deleted Event',
			'New Team Message',
		);

		$options = get_option( 'lckrm_group_notifications' );

		$options = array();
		foreach ( $types as $type ):
			$options[] = array(
				'title' 			=> $type,
				'id' 				=> 'lckrm_group_notifications[' . $type . ']',
				'default' 			=> '',
				'placeholder'	 	=> $type,
				'value' 			=> sp_array_value( $options, $type, null ),
				'type' 				=> 'textarea',
				'class'				=> 'widefat',
				'custom_attributes'	=> array( 'rows' => 6 ),
			);
		endforeach;

		$settings = array_merge( $settings, apply_filters( 'lckrm_notifications', $options ), array(
			array( 'type' => 'sectionend', 'id' => 'notifications_options' )
		));

		return apply_filters( 'lckrm_settings_notifications', $settings ); // End settings
	}

	/**
	 * Save settings
	 */
	public function save() {
		if ( isset( $_POST['lckrm_group_notifications'] ) )
	    	update_option( 'lckrm_group_notifications', $_POST['lckrm_group_notifications'] );
	}
}

endif;

return new SP_settings_Group_Notifications();
