<?php
/*
*
* place BP customizations here
*
*/

define('GROUP_MCRYPT_IV', 'XOB^rV*Z,= %wSkGdA1IOLH-7EO2|OO-3~Z#g6S9<>k[J{=p><A4fT6s$_?/;RrN');
define('GROUP_ENCRYPTION_KEY', '0ae7976dda775fd5f45c36187d938c5f4c61b7c833bf241efdc8ca28');


if ( function_exists( 'wp_debug_mode' ) ) {
	wp_debug_mode();
}

// Register the Cover Image feature for Users profiles
function wds_clp_register_feature() {

	bp_set_theme_compat_feature( 'templates', array(
		'name'     => 'cover_image',
		'settings' => array(
			'components'   => array( 'xprofile', 'groups' ),
			'width'        => 940,
			'height'       => 225,
			'callback'     => 'clp_cover_image',
			'theme_handle' => 'bp-legacy-css',
		),
	) );


}
add_action( 'bp_after_setup_theme', 'wds_clp_register_feature' );

/* Meta boxes */

function lckrm_remove_format_metabox() {
		remove_meta_box( 'sp_formatdiv', 'sp_event', 'side' );
}
add_action( 'do_meta_boxes',  'lckrm_remove_format_metabox' );

function lckrm_preferred_team_meta() {
	global $post;

	if ( ! empty( $post ) ) {
	$pageTemplate = get_post_meta( $post->ID, '_wp_page_template', true );

		if ( $pageTemplate == 'front-page.php' ) {
			add_meta_box(
				'preferred_team',
				__( 'Preferred Team', 'team_textdomain' ),
				'lckrm_preferred_team_callback',
				'page',
				'side'
			);
		}
	}
}
add_action( 'add_meta_boxes', 'lckrm_preferred_team_meta' );

function lckrm_preferred_team_callback( $post ) {

	wp_nonce_field( basename( __FILE__ ), 'carpathiafc_team_nonce' );

	$preferred_team = get_post_meta( $post->ID, '_preferred_team', true );
	$args = array( 'post_type' => 'sp_team', 'numberposts' => -1 );
	$teams = get_posts( $args ); ?>
	<select id="sp_preferred_team" data-placeholder="Select a team" class="chosen-select" name="preferred_team" style="width: 100%;">
		<option></option>
		<?php foreach ( $teams as $team ): ?>
			<option value="<?php echo $team->ID; ?>" <?php echo $preferred_team == $team->ID ? 'selected="selected"' : ''; ?>><?php echo get_the_title( $team->ID ); ?></option>
		<?php endforeach; ?>
	</select>
	<?php
}

function lckrm_save_preferred_team( $post_id, $post ) {
	if ( ! isset( $_POST['carpathiafc_team_nonce'] ) || ! wp_verify_nonce( $_POST['carpathiafc_team_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$preferred_team = ! empty( $_POST['preferred_team'] ) ? stripslashes( $_POST['preferred_team'] ) : '';

	update_post_meta( $post_id, '_preferred_team', $preferred_team );
}
add_action( 'save_post', 'lckrm_save_preferred_team', 10, 3 );

function lckrm_league_table_meta() {
	global $post;

	if ( ! empty( $post ) ) {
	$pageTemplate = get_post_meta( $post->ID, '_wp_page_template', true );

		if ( $pageTemplate == 'front-page.php' ) {
			add_meta_box(
				'preferred_league',
				__( 'League Table', 'league_textdomain' ),
				'lckrm_preferred_league_table_callback',
				'page',
				'side'
			);
		}
	}
}
add_action( 'add_meta_boxes', 'lckrm_league_table_meta' );

function lckrm_preferred_league_table_callback( $post ) {

	wp_nonce_field( basename( __FILE__ ), 'carpathiafc_league_nonce' );

	$preferred_league = get_post_meta( $post->ID, '_preferred_league', true );
	$args = array( 'post_type' => 'sp_table', 'numberposts' => -1 );
	$leagues = get_posts( $args ); ?>
	<select id="sp_preferred_league" data-placeholder="Select a league" class="chosen-select" name="preferred_league" style="width: 100%;">
		<option></option>
		<?php foreach ( $leagues as $league ): ?>
			<option value="<?php echo $league->ID; ?>" <?php echo $preferred_league == $league->ID ? 'selected="selected"' : ''; ?>><?php echo get_the_title( $league->ID ); ?></option>
		<?php endforeach; ?>
	</select>
	<?php
}

function lckrm_save_preferred_league( $post_id, $post ) {
	if ( ! isset( $_POST['carpathiafc_league_nonce'] ) || ! wp_verify_nonce( $_POST['carpathiafc_league_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$preferred_league = ! empty( $_POST['preferred_league'] ) ? stripslashes( $_POST['preferred_league'] ) : '';

	update_post_meta( $post_id, '_preferred_league', $preferred_league );
}
add_action( 'save_post', 'lckrm_save_preferred_league', 10, 3 );

function lckrm_video_meta() {
	global $post;

	if ( ! empty( $post ) ) {
	$pageTemplate = get_post_meta( $post->ID, '_wp_page_template', true );

		if ( $pageTemplate == 'front-page.php' ) {
			add_meta_box(
				'carpathiafc_video',
				__( 'Video', 'video_textdomain' ),
				'lckrm_video_callback'
			);
		}
	}
}
add_action( 'add_meta_boxes', 'lckrm_video_meta' );

function lckrm_video_callback( $post ) {

	wp_nonce_field( basename( __FILE__ ), 'lckrm_video_nonce' );

	$video_iframe = get_post_meta( $post->ID, '_carpathiafc_embed_video', true );
	$btn_link = get_post_meta( $post->ID, '_carpathiafc_btn_link', true );
	?> Video Iframe 
	<input class="widefat" type="text" name="carpathiafc_video" value="<?php echo esc_attr( $video_iframe ); ?>"><br />
	Button link URL
	<input class="widefat" type="text" name="carpathiafc_btn_link" value="<?php echo esc_attr( $btn_link ); ?>">
	<?php
}

function lckrm_save_video( $post_id, $post ) {
	if ( ! isset( $_POST['lckrm_video_nonce'] ) || ! wp_verify_nonce( $_POST['lckrm_video_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$embed_url = ! empty( $_POST['carpathiafc_video'] ) ? stripslashes( $_POST['carpathiafc_video'] ) : '';
	$btn_link = ! empty( $_POST['carpathiafc_btn_link'] ) ? stripslashes( $_POST['carpathiafc_btn_link'] ) : '';

	update_post_meta( $post_id, '_carpathiafc_embed_video', $embed_url );
	update_post_meta( $post_id, '_carpathiafc_btn_link', $btn_link );
}
add_action( 'save_post', 'lckrm_save_video', 10, 3 );

function lckrm_player_of_the_week() {
  global $post;

  if ( ! empty( $post ) ) {
	$pageTemplate = get_post_meta( $post->ID, '_wp_page_template', true );

	if ( $pageTemplate == 'front-page.php' ) {
		add_meta_box(
			'player-meta-box',
			__( 'Player of the week', 'player_of_the_week_textdomain' ),
			'lckrm_player_of_the_week_callback',
			'page',
			'side'
		);
	}
  }
}

add_action( 'add_meta_boxes', 'lckrm_player_of_the_week' );

function lckrm_player_of_the_week_callback( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'player_of_the_week_nonce' );

	$selected_player = get_post_meta( $post->ID, 'player_of_the_week', true );
	$preferred_team = get_post_meta( $post->ID, '_preferred_team', true );

	$args = array( 'post_type' => 'sp_player', 'numberposts' => -1 );
	$players = get_posts( $args );

	/*var_dump( $players );*/  ?>
	<select id="player_of_the_week" data-placeholder="Select a player" class="chosen-select" name="player_of_the_week" style="width: 100%;">
		<option></option>
		<?php foreach ( $players as $player ) {
		$player_current = get_post_meta( $player->ID, 'sp_current_team', true ); ?>
				<option value="<?php echo $player->ID; ?>" <?php echo $selected_player == $player->ID ? 'selected="selected"' : ''; ?>><?php echo get_the_title( $player->ID ); ?></option>
			<?php } ?>
	</select>
	<script type="text/javascript">
	(function($){
		$(document).ready(function() {
			$(".chosen-select").chosen();
		});
	})(jQuery);
	</script>
	<?php
}

function lckrm_save_player_of_the_week( $post_id, $post ) {
	if ( ! isset( $_POST['player_of_the_week_nonce'] ) || ! wp_verify_nonce( $_POST['player_of_the_week_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$player = ! empty( $_POST['player_of_the_week'] ) ? stripslashes( $_POST['player_of_the_week'] ) : '';

	update_post_meta( $post_id, 'player_of_the_week', $player );
}
add_action( 'save_post', 'lckrm_save_player_of_the_week', 10, 3 );

function lckrm_create_bp_group() {
	global $post;

	if ( ! empty( $post ) ) {
		$_post_type = get_post_type( $post );

		if ( 'sp_team' == $_post_type && 'yes' == get_post_meta( $post->ID, 'sp_is_club_team', true ) ) {
			add_meta_box(
				'create_bp_group',
				__( 'BuddyPress Group', 'group_textdomain' ),
				'lckrm_sp_team_create_bp_group',
				'sp_team',
				'side',
				''
			);
		}
	}
}
add_action( 'add_meta_boxes', 'lckrm_create_bp_group' );

function lckrm_sp_team_create_bp_group( $post ) {

	wp_nonce_field( basename( __FILE__ ), 'team_bp_group_nounce' );


	$bpgmq_querystring['meta_query'] = array(
		array(
			'key'		=> 'team_bp_group_id',
			'value'		=>  $post->ID,
			'compare'	=> '='
		),
	);

	$group_arr = BP_Groups_Group::get(array(
		'per_page'		=> -1,
		'meta_query'	=> $bpgmq_querystring
	));

	foreach ( $group_arr['groups'] as $group ) {
		$group_permalink = bp_get_group_permalink( $group );
		$name = $group->name;
	}

	if ( ! empty( $group_arr['groups'] ) ) { ?>
		<p><strong><?php _e( 'BP Group Created', 'sportspress' ); ?></strong></p>
		<p><a href="<?php echo esc_url( $group_permalink ); ?>" target="_blank">Group Permalink</a></p>
	<?php } else { 
		$sp_create_group = get_post_meta( $post->ID, 'sp_create_group', true ); ?>
		<p><strong><?php _e( 'Create BP group for this team?', 'sportspress' ); ?></strong></p>
		<p><input type="checkbox" id="sp_create_group" name="sp_create_group"
		value="yes" <?php selected( $sp_create_group, 'yes' ); ?> /></p> 
		<?php
	}
}

function lckrm_maybe_save_bp_group( $post_id, $post ) {
	if ( ! isset( $_POST['team_bp_group_nounce'] ) || ! wp_verify_nonce( $_POST['team_bp_group_nounce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$sp_team_group = ! empty( $_POST['sp_create_group'] ) ? stripslashes( $_POST['sp_create_group'] ) : '';

	update_post_meta( $post_id, 'sp_create_group', $sp_team_group );
}
add_action( 'save_post', 'lckrm_maybe_save_bp_group', 10, 3 );

function lckrm_recreate_sp_formatdiv() {
	global $post;

	if ( ! empty( $post ) ) {
		$_post_type = get_post_type( $post );

		if ( 'sp_event' == $_post_type ) {
			add_meta_box(
				'sp_format',
				__( 'Format', 'format_textdomain' ),
				'lckrm_sp_format_recreate_metabox',
				'sp_event',
				'side',
				'high'
			);
		}
	}
}
add_action( 'add_meta_boxes', 'lckrm_recreate_sp_formatdiv' );

function lckrm_sp_format_recreate_metabox( $post ) {
	wp_nonce_field( 'sportspress_save_data', 'sportspress_meta_nonce' );
	$the_format = get_post_meta( $post->ID, 'sp_format', true );
	$formats = array(
		'league' => 'League Game',
		'friendly' => 'Friendly Game',
		'practice' => 'Practice',
		'meeting' => 'Meeting',
		'other'	=> 'Other',
	); ?>
	<div id="post-formats-select">
		<?php foreach ( $formats as $key => $format ): ?>
			<input type="radio" name="sp_format" class="post-format" id="post-format-<?php echo $key; ?>" value="<?php echo $key; ?>" <?php checked( true, $the_format == $key ); ?>> <label for="post-format-<?php echo $key; ?>" class="post-format-icon post-format-<?php echo $key; ?>"><?php echo $format; ?></label><br>
		<?php endforeach; ?>
	</div> <?php
}

function lckrm_on_save_sp_formatdiv( $post_id, $post ) {
	if ( ! isset( $_POST['format_formatdiv_nounce'] ) || ! wp_verify_nonce( $_POST['format_formatdiv_nounce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$format = ! empty( $_POST['sp_format'] ) ? stripslashes( $_POST['sp_format'] ) : '';

	update_post_meta( $post_id, 'sp_format', $format );
}
add_action( 'save_post', 'lckrm_on_save_sp_formatdiv', 10, 3 );

function lckrm_users_to_players_metabox() {
	global $post;

	if ( ! empty( $post ) ) {
		$_post_type = get_post_type( $post );

		if ( 'sp_player' == $_post_type ) {
			add_meta_box(
				'user_to_player',
				__( 'Assign User to Player', 'utp_textdomain' ),
				'lckrm_sp_user_to_player_metabox',
				'sp_player',
				'side',
				'high'
			);
		}
	}
}
add_action( 'add_meta_boxes', 'lckrm_users_to_players_metabox' );

function lckrm_sp_user_to_player_metabox( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'sp_user_to_player_nounce' );
	$sp_utp = get_post_meta( $post->ID, '_assigned_user_to_player', true );

	$args = array(
		'role' => 'sp_player',
		'orderby' => 'display_name',
		'order' => 'ASC'
	);

	$users = get_users( $args );

	?>
	<select id="sp_user_to_player" data-placeholder="Assign user" class="chosen-select" name="chosen_user" style="width: 100%;">
		<option></option>
		<?php foreach ( $users as $user ): ?>
			<option value="<?php echo $user->ID; ?>" <?php echo $sp_utp == $user->ID ? 'selected="selected"' : ''; ?>><?php echo $user->display_name; ?></option>
		<?php endforeach; ?>
	</select>
	<?php
}

function lckrm_on_save_user_to_player_meta( $post_id, $post ) {
	if ( ! isset( $_POST['sp_user_to_player_nounce'] ) || ! wp_verify_nonce( $_POST['sp_user_to_player_nounce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$user_id = ! empty( $_POST['chosen_user'] ) ? stripslashes( $_POST['chosen_user'] ) : '';

	update_post_meta( $post_id, '_assigned_user_to_player', $user_id );
}
add_action( 'save_post', 'lckrm_on_save_user_to_player_meta', 10, 3 );

/* Shortcodes */

function lckrm_player_of_the_week_shortcode( $atts ){
	global $post;

	$selected_player = get_post_meta( $post->ID, 'player_of_the_week', true );

	if ( empty( $selected_player ) ) {
		return;
	}

	$player = get_post( $selected_player );
	$player_num = get_post_meta( $player->ID, 'sp_number', true );
	$thumbnail_id = get_post_thumbnail_id( $player->ID );
	$player_image = wp_get_attachment_image_src ( $thumbnail_id, 'medium' );

	ob_start();
	?>
	<div id="sp-player-shortcode" class="player-of-the-week">
		<h3>PLAYER OF THE WEEK</h4>
		<div class="image-wrap">
			<img class="player-image" src="<?php echo esc_attr( $player_image[0] ); ?>">
		</div>
		<p class="player-info">
			<span><?php echo get_the_title( $player->ID ) . ' #' . $player_num; ?></span>
		</p>
	</div>
	<?php

	return ob_get_clean();
}
add_shortcode( 'player_of_the_week', 'lckrm_player_of_the_week_shortcode' );

function lckrm_video_shortcode( $args ) {
	global $post;

	$video_iframe = get_post_meta( $post->ID, '_carpathiafc_embed_video', true );
	$btn_link = get_post_meta( $post->ID, '_carpathiafc_btn_link', true );

	ob_start(); ?>
	<?php if ( ! empty( $video_iframe ) || ! empty( $btn_link ) ): ?>
		<div id="lckrm_video" class="lckrm-video-wrap">
		<?php if ( ! empty( $video_iframe ) ): ?>
			<div class="video-iframe">
				<?php echo $video_iframe; ?>
			</div>
		<?php endif; 
			if ( ! empty( $btn_link ) ): ?>
				<a href="<?php echo esc_url( $btn_link ); ?>" class="video-btn" target="_blank">View Videos</a>
		<?php endif; ?>
		</div>
	<?php endif; ?>

	<?php
	return ob_get_clean();
}
add_shortcode( 'lckrm_video', 'lckrm_video_shortcode' );

function lckrm_stats_shortcode() {
	global $post;

	$args = array( 'post_type' => 'sp_table', 'numberposts' => -1 );
	$leagues = get_posts( $args );

	$preferred_league = get_post_meta( $post->ID, '_preferred_league', true );
	$preferred_team = get_post_meta( $post->ID, '_preferred_team', true );

	$results = array();
	foreach ( $leagues as $league ) {
		if ( $preferred_league != $league->ID ) {
			continue;
		}
		$permalink = get_post_permalink( $league, false, true ); 
		$column = get_post_meta( $league->ID, 'sp_teams', false );
		$results = array_shift( $column );
	}

	if ( $results[ $preferred_team ] ) {
		foreach ( $results[ $preferred_team ] as $key => $result ) {
			if ( '' == $result ) {
				$results[ $preferred_team ][ $key ] = '0';
			}
		}
	}

	ob_start();
	?>
	<?php if ( $results ) : ?>
		
		<div id="lckrm_stats" class="stats-round-up">
			<h3 class="stats-title">Carpathia FC Stats</h3>
			<div class="team-stats">
				<div class="stats-row1">
					<div class="played"><?php echo $results[ $preferred_team ]['p']; ?><span>Played</span></div>
					<div class="goals"><?php echo $results[ $preferred_team ]['f']; ?><span>Goals</span></div>
				</div>
				<div class="stats-row2">
					<div class="wins"><?php echo $results[ $preferred_team ]['w']; ?><span>Wins</span></div>
					<div class="draws"><?php echo $results[ $preferred_team ]['d']; ?><span>Draws</span></div> 
					<div class="losses"><?php echo $results[ $preferred_team ]['l']; ?><span>Losses</span></div>
				</div>
			</div>
			<a class="more-results" href="<?php echo esc_url( $permalink ); ?>">View More Results</a>
		</div>
	<?php endif; ?>
	<?php
	return ob_get_clean();
}
add_shortcode( 'lckrm_stats', 'lckrm_stats_shortcode' );

function lckrm_featured_news_shortcode( $args ) {
	global $post;

	$args = array(
		'post_type' => 'post',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => array('featured'),
			)
		),
		'numberposts' => 3,
		'orderby' => 'date',
		'order'	=> 'DESC',
	);
	$category_id = get_cat_ID( 'featured' );
    $category_link = get_category_link( $category_id );
	$news = get_posts( $args );
	$string_limit = 100;
	ob_start();?>
	<?php if ( $news ): ?>
		<div id="lckrm_featured" class="sp-featured-wrap">
		<?php foreach ($news as $key => $featured):
			$thumbnail_id = get_post_thumbnail_id( $featured->ID );
			$featured_image = wp_get_attachment_image_src ( $thumbnail_id, 'medium' ); ?>
			<div class="featured-block">
				<div class="featured-img">
					<a href="<?php echo esc_url( get_post_permalink( $featured->ID ) ); ?>" style="background-image: url(<?php echo esc_url( $featured_image[0] ); ?>); overflow: hidden; background-size: cover; background-position: center; max-height: 150px; height: 200px; display:block;"></a>
				</div>
				<div>
					<a href="<?php echo esc_url( get_post_permalink( $featured->ID ) ); ?>" class="featured-title"><?php echo get_the_title( $featured->ID ); ?></a>
				</div>
				<p class="featured-desc">
					<?php echo $string_limit > strlen( $featured->post_content ) ? $featured->post_content : substr( $featured->post_content, 0, 100 ) . '...'; ?>
				</p>
			</div>
		<?php endforeach; ?>	
			<a class="featured-more" href="<?php echo esc_url( $category_link ); ?>">Read More News</a>
		</div>
	<?php endif; ?>
	<?php
	return ob_get_clean();
}
add_shortcode( 'lckrm_featured_news', 'lckrm_featured_news_shortcode'  );

function lckrm_events_slider_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'team_id' => false,
	), $atts, 'events_slider' ) );

	$args = array(
		'post_type' => 'sp_event',
		'numberposts' => -1,
		'post_status' => 'future,publish',
		'orderby' => 'date',
		'order'	=> 'ASC',
		'meta_query' => array(
			array(
				'key' => 'sp_format',
				'value' => array( 'league', 'friendly' ),
				'compare' => 'IN',
			),
		),
	);
	if ( $team_id ) {

		$group_args = array(
			'meta_query' => array(
					array(
					'key' => 'team_bp_group_id',
					'value' => $team_id,
					'compare' => '=',
				),
			)
		);

		$groups = groups_get_groups( $group_args );

		if ( ! empty( $groups['groups'] ) ) {
			$args['meta_query'][] = array(
				'key' => 'lckrm_event_created_group',
				'value' => $groups['groups'][0]->id,
				'compare' => '=',
			);
		}
	}

	$events = get_posts( $args );

	ob_start();
	
	if ( $events ): ?>
		<div id="events_slider" class="sp-event-blocks">
			<?php foreach ( $events as $event ):

				$teams = get_post_meta( $event->ID, 'sp_team', false );
				$results = get_post_meta( $event->ID, 'sp_results', true );

				$team1_id = array_shift( $teams );
				$team2_id = array_shift( $teams );
				
				$team1 = is_array( $results ) ? $results[ $team1_id ] : false;
				$team2 = is_array( $results ) ? $results[ $team2_id ] : false;

				// $team1_id = key( $team1 );
				if ( $team1_id ) {
					$team1_thumbnail_id = get_post_thumbnail_id( $team1_id );
					$featured_image1 = wp_get_attachment_image_src ( $team1_thumbnail_id, 'medium' );
				}

				if ( $team2_id ) {
					$team2_thumbnail_id = get_post_thumbnail_id( $team2_id );
					$featured_image2 = wp_get_attachment_image_src ( $team2_thumbnail_id, 'medium' );
				} ?>
			<div class="event-block <?php echo 'future' == $event->post_status ? 'futured' : 'published'; ?>">
				<a href="<?php echo esc_url( get_post_permalink( $event, false, true ) ); ?>" class="team-logo logo-odd">
					<img src="<?php echo esc_attr( $featured_image1[0] ); ?>" class="attachment-sportspress-fit-icon size-sportspress-fit-icon wp-post-image">
				</a>
				<a href="<?php echo esc_url( get_post_permalink( $event, false, true ) ); ?>" class="team-logo logo-even" class="attachment-sportspress-fit-icon size-sportspress-fit-icon wp-post-image">
					<img src="<?php echo esc_attr( $featured_image2[0] ); ?>" class="attachment-sportspress-fit-icon size-sportspress-fit-icon wp-post-image">
				</a>
				<?php if ( 'future' == $event->post_status || ! ( $team1 && $team2 ) ) : ?>
					<time class="sp-event-date" datetime="<?php echo date( 'M jS,Y', strtotime( $event->post_date ) ); ?>">
						<a href="<?php echo esc_url( get_post_permalink( $event, false, true ) ); ?>"><?php echo date( 'M jS,Y', strtotime( $event->post_date ) ); ?></a>							
					</time>
					<h5 class="sp-event-results">
						<a href="<?php echo esc_url( get_post_permalink( $event, false, true ) ); ?>"><span class="sp-result"><?php echo date( 'g:iA', strtotime( $event->post_date ) ); ?></span>
						</a>
					</h5>
				<?php else : ?>
					<time class="sp-event-date" datetime="<?php echo date( 'M jS,Y', strtotime( $event->post_date ) ); ?>">
						<a href="<?php echo esc_url( get_post_permalink( $event, false, true ) ); ?>"><?php echo date( 'M jS,Y', strtotime( $event->post_date ) ); ?></a>							
					</time>
					<h5 class="sp-event-results">
						<a href="<?php echo esc_url( get_post_permalink( $event, false, true ) ); ?>"><span class="sp-result"><?php echo $team1_id == $team_id ? strtoupper( substr( $team1['outcome'][0], 0, 1 ) ) : strtoupper( substr( $team2['outcome'][0], 0, 1 ) ); ?> <?php echo $team1['goals']; ?></span> - <span class="sp-result"><?php echo $team2['goals']; ?></span></a>
					</h5>
				<?php endif; ?>
				<h4 class="sp-event-title">
					<a href="<?php echo esc_url( get_post_permalink( $event, false, true ) ); ?>"><?php echo get_the_title( $event->ID ); ?></a>
				</h4>
			</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<script type="text/javascript">
		(function($){
			$(document).ready(function() {
				$('.sp-event-blocks').slick({
					infinite : false,
					slidesToShow: 4,
					slidesToScroll : 1,
					responsive: [
						{
						breakpoint: 900,
						settings: {
						slidesToShow: 3,
						slidesToScroll: 1
						}
					},
					{
						breakpoint: 700,
						settings: {
						slidesToShow: 1,
						slidesToScroll: 1
						}
					}
					]
				});		
			});

		})(jQuery);
	</script>
	<?php
	return ob_get_clean();
}
add_shortcode( 'events_slider', 'lckrm_events_slider_shortcode' );

/* End Shortcodes */

add_filter( 'sportspress_meta_boxes', 'lckrm_filter_sp_meta_boxes', 10 );
add_action( 'sportspress_process_sp_team_meta', 'lckrm_save_sp_team_extra_info', 10, 2 );

function lckrm_filter_sp_meta_boxes( $meta_boxes ) {
	$meta_boxes['sp_team']['details']['output'] = 'lckrm_render_sp_team_details_meta_box';

	return $meta_boxes;
}

function lckrm_save_sp_team_extra_info( $post_id, $post ) {
	update_post_meta( $post_id, 'sp_is_club_team', ! empty( $_POST['sp_is_club_team'] ) ? 'yes' : '' );
}

function lckrm_render_sp_team_details_meta_box( $post ) {
	SP_Meta_Box_Team_Details::output( $post );

    $sp_is_club_team = get_post_meta( $post->ID, 'sp_is_club_team', true ); ?>
<p><strong><?php _e( 'Is Club Team?', 'sportspress' ); ?></strong></p>
<p><input type="checkbox" id="sp_is_club_team" name="sp_is_club_team"
value="yes" <?php checked( $sp_is_club_team, 'yes' ); ?> /></p>
<?php
}

/* Change the event title (HOME or AWAY match) */
function lckrm_teams_filter_event_title( $title, $post_id ) {
	if ( 'sp_event' != get_post_type( $post_id ) ) {
		return $title;
	}

	if ( is_admin() && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
		return $title;
	}

	$teams = get_post_meta( $post_id, 'sp_team', false );
	$event_type = get_post_meta( $post_id, 'sp_format', true );
	
	if ( ! $teams || ! $event_type) {
		return $title;
	}

	if ( 'league' == $event_type || 'friendly' == $event_type ) {
		$team1_id = array_shift( $teams );
		$team2_id = array_shift( $teams );


		if ( 'yes' == get_post_meta( $team1_id, 'sp_is_club_team', true ) ) {
			$title = 'HOME vs ' . get_the_title( $team2_id );
		} else if ( 'yes' == get_post_meta( $team2_id, 'sp_is_club_team', true ) ) {
			$title = 'AWAY @ ' . get_the_title( $team1_id );
		} else {
			$title = get_the_title( $team1_id ) . ' vs ' . get_the_title( $team2_id );
		}
	}

	return $title;
}
add_filter( 'the_title', 'lckrm_teams_filter_event_title', 10, 2 );

/* Reverse the sp_team post meta */
function lckrm_get_custom_meta_team_values( $metadata, $object_id, $meta_key, $single ) {
	if ( 'sp_team' != $meta_key || is_admin() || true == $singe ) {
		return $metadata;
	}

	$args = array( 'post_type' => 'sp_team', 'numberposts' => -1 );
	$teams = get_posts( $args );
	foreach ( $teams as $team ) {
		$sp_is_club_team = get_post_meta( $team->ID, 'sp_is_club_team', true );
		if ( 'yes' == $sp_is_club_team ) {
			$meta_needed = 'sp_team';
			if ( isset( $meta_key ) && $meta_needed == $meta_key ){
				remove_filter( 'get_post_metadata', 'lckrm_get_custom_meta_team_values', 100 );
				$current_meta = get_post_meta( $object_id, $meta_needed, false );
				add_filter('get_post_metadata', 'lckrm_get_custom_meta_team_values', 100, 4);
				
				if ( false !== array_search( $team->ID , $current_meta ) && 0 != array_search( $team->ID , $current_meta ) ) {
					$current_meta = array_reverse( $current_meta );
				}
				
				return $current_meta;
			}
		}
	}

	// Return original if the check does not pass
	return $metadata;

}
/*add_filter( 'get_post_metadata', 'lckrm_get_custom_meta_team_values', 100, 4 ); */

function lckrm_custom_sports_press_template( $template, $template_name, $template_path ) {
	$sportspress_path = SP()->plugin_path() . '/templates/';

	// If template was obtained from the SportsPress plugin
	if ( false !== stripos( $template, $sportspress_path ) && file_exists( lckrm_get_templates_path() . '/sportspress/' . $template_name ) ) {
		$template = lckrm_get_templates_path() . '/sportspress/' . $template_name;
	}

	return $template;
}
add_filter( 'sportspress_locate_template', 'lckrm_custom_sports_press_template', 10, 3 );

function lckrm_get_templates_path() {
	return dirname( dirname( __FILE__ ) ) . '/templates';
}

function lckrm_get_posts_info( $type, $term ) {
	if ( empty( $type ) && empty( $term ) ) {
		return 'Empty args!';
	}

	$store = array();
	if ( ! empty( $type ) && empty( $term ) ) {
		$args = array( 'post_type' => $type, 'numberposts' => -1 );
		$cpt = get_posts( $args );

		if ( $cpt ) {
			foreach ( $cpt as $post_item ) {
				$store[ $post_item->ID ] = get_the_title( $post_item->ID );
			}
		}

	} elseif ( ! empty( $term ) && empty( $type ) ) {
		$terms = get_terms( $term, array( 'hide_empty' => false ) );

		if ( ! empty( $terms ) ) {
			foreach ( $terms as $_term ) {
				$store[ $_term->term_id ] = $_term->name;
			}
		}
	}
	
	return $store;
}


/* Disable group avatars from Gravatar */
/*add_filter('bp_core_fetch_avatar_no_grav', '__return_true');*/


/**
 * Default group avatar
 */

function lckrm_default_get_group_avatar( $avatar, $r ) {
	global $bp, $groups_template;

	if ( strpos( $avatar, 'group-avatars' ) ) {
		return $avatar;
	} else {
		$team_id = groups_get_groupmeta( bp_get_group_id(), 'team_bp_group_id', true );

		$custom_avatar = wp_get_attachment_image_src( get_post_thumbnail_id( $team_id ), 'thumbnail' );

		return '<img class="avatar" alt="' . esc_attr( $groups_template->group->name ) . '" src="' . $custom_avatar[0] . '" width="' . BP_AVATAR_FULL_WIDTH . '" height="' . BP_AVATAR_FULL_HEIGHT . '" />';
	}
}
add_filter( 'bp_get_group_avatar',  'lckrm_default_get_group_avatar', 1000, 2 );

/* Change default group cover image */

function lckrm_profile_cover_image( $settings = array() ) {
	global $bp;

	$team_id = groups_get_groupmeta(  $bp->groups->current_group->id, 'team_bp_group_id', true );

	$team_photo = get_post_meta( $team_id, 'sp_team_team-header-image_thumbnail_id', true );
	$team_photo_src = wp_get_attachment_image_src( $team_photo, 'large' );
	
    $settings['default_cover'] = $team_photo_src[0];

    return $settings;
}
add_filter( 'bp_before_groups_cover_image_settings_parse_args', 'lckrm_profile_cover_image', 10, 1 );

function lckrm_bp_is_group_mass_message() {
	return (bool) ( bp_is_groups_component() && bp_is_current_action( 'message' ) );
}

function lckrm_bp_is_group_calendar() {
	return (bool) ( bp_is_groups_component() && bp_is_current_action( 'calendar' ) );
}

function lckrm_bp_is_group_invite() {
	return (bool) ( bp_is_groups_component() && bp_is_current_action( 'invite' ) );
}

function lckrm_bp_is_group_single_event() {
	return (bool) ( bp_is_groups_component() && bp_is_current_action( 'event' ) );
}

// Encrypt Function
function lckrm_encrypted_string($pure_string) {
    $dirty = array("+", "/", "=");
    $clean = array("_PLS_", "_SLSH_", "_EQLS_");
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, GROUP_ENCRYPTION_KEY, utf8_encode($pure_string), MCRYPT_MODE_ECB, GROUP_MCRYPT_IV);
    $encrypted_string = base64_encode($encrypted_string);
    return str_replace($dirty, $clean, $encrypted_string);
}

// Decrypt Function
function lckrm_decrypted_string($encrypted_string) { 
    $dirty = array("+", "/", "=");
   	$clean = array("_PLS_", "_SLSH_", "_EQLS_");

    $string = base64_decode(str_replace($clean, $dirty, $encrypted_string));

    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, GROUP_ENCRYPTION_KEY,$string, MCRYPT_MODE_ECB, GROUP_MCRYPT_IV);
    return rtrim( $decrypted_string, "\0" );
}

function lckrm_get_group_permalink_by_id( $group_id ) {
	global $bp;
	$group = groups_get_group( array( 'group_id' => $group_id ) );
	$url = trailingslashit( bp_get_root_domain() . '/' . bp_get_groups_root_slug() . '/' . $group->slug . '/' );
	return $url;
}


function lckrm_send_group_invitations( $user_id, $group_id, $message ) {
	$user = get_userdata( $user_id );

	$user_login_url = lckrm_generate_user_auth_url( $user->ID, array( 'location' => 'new_invite' ) );
	$group = groups_get_group( array( 'group_id' => $group_id ) );

	$_message = lckrm_get_message_contents( false, array( 'user' => $user, 'group' => $group, 'message' => $message, 'url' => $user_login_url ) );
	
	add_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
	wp_mail( $user->user_email, 'Invitation from ' . $group->name, $_message );
	remove_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');

	$user_number = bp_get_user_meta( $user->ID, 'lckrm_member_cellphone', true );

	if ( LCKRM_Twilio::is_available() && 'no' != bp_get_user_meta( $user->ID, 'lckrm_sms_notifications', true ) && $user_number ) {
		$user_login_url = lckrm_generate_user_auth_url( $user->ID, array( 'location' => 'new_invite' ) );
		LCKRM_Twilio::send_sms( $user_number, lckrm_get_message_contents( false, array( 'user' => $user, 'group' => $group, 'message' => $message, 'type' => 'SMS', 'url' => $user_login_url ) ) );
	}
}

/**
 * @param  user ID
 * @param  array( $args )
 * @return URL with Encrypted token 
 */
function lckrm_generate_user_auth_url( $user_id, $args = array() ) {
	if ( empty( $user_id ) || empty( $args ) ) {
		return 'NYAMA';
	}

	/* Generate random string */
    $random_string = '';
    $random_string = substr( str_shuffle( "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ), 0, 5 );
    
	$user = get_userdata( $user_id );
	$tokens = get_user_meta( $user->ID, '_lckrm_auth_tokens', true );
	
	$random_string = base64_encode( $random_string . time() );
    $random_string .= '|' . $user->ID;
	$expiration_date = strtotime( '+7 days' );
	
	$tokens = is_array( $tokens ) ? $tokens : array();
	$tokens[] = array(
		'token'		=> $random_string,
		'expires'	=> $expiration_date,
	);

	update_user_meta( $user->ID, '_lckrm_auth_tokens', $tokens );

	$redirect = '';
	$location = ! empty( $args['location'] ) ? $args['location'] : false;
	if ( 'event' == $location ) {
		$redirect = lckrm_get_group_permalink_by_id( $args['group_id'] ) . 'event/?event_id=' . $args['event_id'];
	} elseif ( 'new_invite' == $location ) {
		$redirect = bp_core_get_user_domain( $user->ID ) . bp_get_settings_slug();
	} elseif ( 'unsubscribe' == $location ) {
		$redirect = bp_core_get_user_domain( $user->ID ) . 'settings/notifications/';
	} elseif ( ! empty( $args['redirect_to'] ) ) {
		$redirect = $args['redirect_to'];
	}

    /* Encrypt the random generated string */
	$u_auth = lckrm_encrypted_string( $random_string );

	return wp_login_url() . '/?u_auth=' . $u_auth . '&redirect_to=' . $redirect;
}

function lckrm_send_2h_notify_before_event( $event, $group_id ) {
	if ( ! $event || empty( $group_id ) ) {
		return;
	}

	global $bp;

	$group_args = array(
		'group_id' => $group_id,
		'per_page' => 99999, // -1 is not supported :(
		'exclude_admins_mods' => false,
	);

	if ( bp_group_has_members( $group_args ) ) :
		while ( bp_group_members() ) : bp_group_the_member();
			$user = get_userdata( bp_get_member_user_id() );
			if ( 'no' != bp_get_user_meta( $user->ID, 'lckrm_email_notifications', true ) ) {
				$subject = 'Upcoming event: ' . get_the_title( $event->ID );
				$message = lckrm_get_message_contents( 'Event Reminder', array( 'event' => $event, 'group' => $group_id, 'user' => $user, 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'location' => 'event', 'group_id' => $group_id, 'event_id' => $event->ID ) ) ) );

				add_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
				wp_mail( $user->user_email, $subject, $message );
				remove_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
			}

			$user_number = bp_get_user_meta( $user->ID, 'lckrm_member_cellphone', true );

			if ( LCKRM_Twilio::is_available() && 'no' != bp_get_user_meta( $user->ID, 'lckrm_sms_notifications', true ) && $user_number ) {
				LCKRM_Twilio::send_sms( $user_number, $message = lckrm_get_message_contents( 'Event Reminder', array( 'event' => $event, 'group' => $group_id, 'user' => $user, 'type' => 'SMS', 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'location' => 'event', 'group_id' => $group_id, 'event_id' => $event->ID ) ) ) ) );
			}

		endwhile;
	endif;
}

function lckrm_send_24h_rsvp_before_event( $event, $user, $group_id ) {
	if ( ! $event || ! $user || empty( $group_id ) ) {
		return;
	}

	if ( 'no' != bp_get_user_meta( $user->ID, 'lckrm_email_notifications', true ) ) {
		$subject = 'Will you be able to make ' . get_the_title( $event->ID );
		$message = lckrm_get_message_contents( 'RSVP NAG', array( 'event' => $event, 'group' => $group_id, 'user' => $user, 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'location' => 'event', 'group_id' => $group_id, 'event_id' => $event->ID ) ) ) );

		add_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
		wp_mail( $user->user_email, $subject, $message );
		remove_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
	}

	$user_number = bp_get_user_meta( $user->ID, 'lckrm_member_cellphone', true );

	if ( LCKRM_Twilio::is_available() && 'no' != bp_get_user_meta( $user->ID, 'lckrm_sms_notifications', true ) && $user_number ) {
		LCKRM_Twilio::send_sms( $user_number, $message = lckrm_get_message_contents( 'RSVP NAG', array( 'event' => $event, 'group' => $group_id, 'user' => $user, 'type' => 'SMS', 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'location' => 'event', 'group_id' => $group_id, 'event_id' => $event->ID ) ) ) ) );
	}
}

/**
 * Changes the notification output
 * @param  [string] $message_id [ Can be one of the following -> 'New Events', 'Event Reminder', 'RSVP NAG', 'Edited Events', 'Deleted Events', 'New Team Message',]
 * @param  [array] $args
 * @return [formated string]
 */
function lckrm_get_message_contents( $message_id, $args ) {
	$defaults = array(
		'event'			=> false,
		'user'			=> false,
		'group'			=> false,
		'url' 			=> false,
		'team_message'	=> false,
		'type'			=> 'email',
		'message'		=> false,
	);

	$args = wp_parse_args( $args, $defaults );

	if ( ! $message_id ) {
		if ( ! empty( $args['message'] ) ) {
			$message = $args['message'];
		} else {
			return false;
		}
	} else {
		$message = get_option( 'lckrm_group_notifications' );

		if ( empty( $message[ $message_id ] ) ) {
			return false;
		}

		$message = $message[ $message_id ];
	}

	if ( $args['event'] ) {
		$args['event'] = is_object( $args['event'] ) ? $args['event'] : get_post( $args['event'] );

		$message = str_ireplace( '{Event_Name}', get_the_title( $args['event']->ID ), $message );

		$date = new DateTime( $args['event']->post_date );
		$message = str_ireplace( '{Event_Date}', $date->format( 'D F d' ), $message );
		$message = str_ireplace( '{Event_Time}', $date->format( 'g:i A' ), $message );

		$venues = get_the_terms( $args['event']->ID, 'sp_venue' );
		
		if ( $venues ) {
			foreach ( $venues as $venue ) {
				$message = str_ireplace( '{Event_Venue}', $venue->name, $message );
			}
		}
	}

	if ( $args['user'] ) {
		$args['user'] = is_object( $args['user'] ) ? $args['user'] : get_userdata( $args['user'] );

		$message = str_ireplace( '{member_First_Name}', $args['user']->first_name, $message );

		if ( 'SMS' != $args['type'] ) {
			$unsubscribe_url = lckrm_generate_user_auth_url( $args['user']->ID, array( 'location' => 'unsubscribe' ) );
			$unsubscribe = '<a href="' . esc_url( $unsubscribe_url ) . '" style="text-decoration: none; margin-top: 20px; display: block; text-align: center;">Unsubscribe</a>';
			$message .= $unsubscribe;
		}
	}

	if ( $args['group'] ) {
		$args['group'] = is_object( $args['group'] ) ? $args['group'] : groups_get_group( array( 'group_id' => $args['group'] ) );

		$message = str_ireplace( '{Group_Name}', $args['group']->name, $message );
	}

	if ( $args['url'] ) {

		if ( function_exists( 'prli_create_pretty_link' ) ) {
			$link_id = prli_create_pretty_link( $args['url'], $slug = '', $name = 'LCRKM Auto Generated Link', '', 0, '', '', 'pixel' );
			$link = str_ireplace( '//www.', '//', prli_get_pretty_link_url( $link_id ) );

			if ( false !== $link_id ) {
				if ( 'SMS' != $args['type'] ) {
					$args['url'] = '<a href="' . esc_url( $link ) . '">' . $link . '</a>'; 
				} else {
					$args['url'] = $link;
				}
			}
		}

		$message = str_ireplace( '{Short_URL}', $args['url'], $message );
	}

	if ( $args['team_message'] ) {
		$message = str_ireplace( '{Team_Message}', $args['team_message'], $message );
	}

	return 'email' == $args['type'] ? wpautop( $message ) : $message;
}

function lckrm_get_group_members( $group_id ) {
	if ( ! $group_id ) {
		 return;
	}

	global $bp;

	$members = array();
	$group_args = array(
		'group_id' => $group_id,
		'per_page' => 99999,
		'exclude_admins_mods' => false,
	);

	if ( bp_group_has_members( $group_args ) ) {
		while ( bp_group_members() ) { bp_group_the_member();
			$user = get_userdata( bp_get_member_user_id() );
			$members[] = $user->ID;
		}
	}

	return $members;
}

function lckrm_get_group_calendar_feeds() {
	global $bp;

	$calendar_id = groups_get_groupmeta( $bp->groups->current_group->id, 'group_calendar_export', true );
	$calendar = get_post( $calendar_id );

	if ( empty( $calendar_id ) || ! $calendar ) {
		return '';
	}


	if ( class_exists( 'SP_Feeds' ) ) {

		$feeds = new SP_Feeds();
		$calendar_feeds = $feeds->calendar;
		$links = array();
		
		foreach ( $calendar_feeds as $slug => $formats ) {
			$link = add_query_arg( 'feed', 'sp-' . $slug, untrailingslashit( get_post_permalink( $calendar ) ) );

			foreach ( $formats as $format ) {
				$protocol = sp_array_value( $format, 'protocol' );
				
				if ( $protocol ) {
					$feed = str_replace( array( 'http:', 'https:' ), 'webcal:', $link );
				} else {
					$feed = $link;
				}
				$prefix = sp_array_value( $format, 'prefix' );
				
				if ( $prefix ) {
					$feed = $prefix . urlencode( $feed );
				}

				$links[ sp_array_value( $format, 'name' ) ] = $feed;
			}
		}

		return ! empty( $links ) ? $links : '';
	}
}