<?php

function lckrm_on_bp_include() {

	remove_action( 'bp_setup_admin_bar', 'bp_members_admin_bar_my_account_menu', 4 );
}
add_action( 'bp_include', 'lckrm_on_bp_include', 100 );

function lckrm_admin_bar_render() {
    global $wp_admin_bar;
	$wp_admin_bar->remove_node('my-account-activity');
	$wp_admin_bar->remove_node('my-account-xprofile');
	$wp_admin_bar->remove_node('my-account-notifications');
	$wp_admin_bar->remove_node('my-account-messages');
	$wp_admin_bar->remove_node('my-account-groups');
	$wp_admin_bar->remove_node('my-account-settings');
	$wp_admin_bar->remove_node('logout');
    
}
add_action( 'wp_before_admin_bar_render', 'lckrm_admin_bar_render' );

function lckrm_register_scripts() {
	/* Styles */

	wp_register_style( 'semantic-ui', plugins_url( 'assets/semantic-ui/semantic.min.css', LCKRM_Framework::instance()->plugin_file() ) );

	wp_register_style( 'slick-css', plugins_url( 'assets/js/slick/slick.css', LCKRM_Framework::instance()->plugin_file() ) );

	wp_register_style( 'slick-theme', plugins_url( 'assets/js/slick/slick-theme.css', LCKRM_Framework::instance()->plugin_file() ) );

	wp_register_style( 'pickadate', plugins_url( 'assets/js/pickadate/css/classic.css', LCKRM_Framework::instance()->plugin_file() ) );

	wp_register_style( 'pickadate-date', plugins_url( 'assets/js/pickadate/css/classic.date.css', LCKRM_Framework::instance()->plugin_file() ) );

	wp_register_style( 'pickadate-time', plugins_url( 'assets/js/pickadate/css/classic.time.css', LCKRM_Framework::instance()->plugin_file() ) );

	wp_register_style( 'bp-custom-css', plugins_url( 'assets/css/bp-custom.css', LCKRM_Framework::instance()->plugin_file() ), array(), 'v1.0.0' );

	wp_register_style( 'font-awesome', plugins_url( 'assets/css/font-awesome.css', LCKRM_Framework::instance()->plugin_file() ) );

	wp_register_style( 'react-big-calendar', plugins_url( 'assets/css/react-big-calendar.css', LCKRM_Framework::instance()->plugin_file() ) );

	wp_enqueue_style( 'semantic-ui' );

	wp_enqueue_style( 'react-big-calendar' );

	wp_enqueue_style( 'pickadate' );

	wp_enqueue_style( 'pickadate-date' );

	wp_enqueue_style( 'pickadate-time' );

	wp_enqueue_style( 'slick-css' );

	wp_enqueue_style( 'slick-theme' );

	wp_enqueue_style( 'bp-custom-css' );

	wp_enqueue_style( 'font-awesome' );

	/* Scripts */

	$min = ! defined( 'SCRIPT_DEBUG' ) || ! SCRIPT_DEBUG ? '.min' : '';

	wp_register_script( 'semantic-script', plugins_url( 'assets/semantic-ui/semantic.min.js', LCKRM_Framework::instance()->plugin_file() ), array('jquery'),'1.0', true );	

	wp_register_script( 'bp-slick', plugins_url( 'assets/js/slick/slick.min.js', LCKRM_Framework::instance()->plugin_file() ), array('jquery'),'1.1', true );

	wp_register_script( 'react', plugins_url( 'assets/js/react' . $min . '.js', LCKRM_Framework::instance()->plugin_file() ), array(), 'v0.14.7', true );

	wp_register_script( 'react-dom', plugins_url( 'assets/js/react-dom' . $min . '.js', LCKRM_Framework::instance()->plugin_file() ), array( 'react' ), 'v0.14.7', true );

	wp_register_script( 'react-calendar', plugins_url( 'node_modules/react-big-calendar/lib/index.js', LCKRM_Framework::instance()->plugin_file() ), array( 'react-dom' ), 'v0.9.8', true );

	wp_register_script( 'requirejs', plugins_url( 'assets/js/require.js', LCKRM_Framework::instance()->plugin_file() ), array(), '2.1.11', true );

	// wp_register_script( 'moment-js', plugins_url( 'assets/js/moment.js', LCKRM_Framework::instance()->plugin_file() ), array(), 'v2.12.0', true );

	// wp_register_script( 'lckrm-react', plugins_url( 'react/build/lckrm.js', LCKRM_Framework::instance()->plugin_file() ), array( 'react-dom', 'moment-tz-with-data', 'wp-api' ), 'v0.1.0', true );
	
	/*wp_register_script( 'moment-tz-with-data', plugins_url( 'assets/js/moment-timezone-with-data.min.js', LCKRM_Framework::instance()->plugin_file() ), array( 'lckrm-bundle' ), '0.5.2', true );*/



	wp_register_script( 'pickadate-js', plugins_url( 'assets/js/pickadate/picker.js', LCKRM_Framework::instance()->plugin_file() ), array(), 'v0.1.0', true );

	wp_register_script( 'pickadate-date-js', plugins_url( 'assets/js/pickadate/picker.date.js', LCKRM_Framework::instance()->plugin_file() ), array(), 'v0.1.0', true );

	wp_register_script( 'pickadate-time-js', plugins_url( 'assets/js/pickadate/picker.time.js', LCKRM_Framework::instance()->plugin_file() ), array(), 'v0.1.0', true );

	wp_register_script( 'bp-custom', plugins_url( 'assets/js/bp-custom.js', LCKRM_Framework::instance()->plugin_file() ), array(), 'v1.1.1', true );

	wp_enqueue_script( 'bp-slick' );
	wp_enqueue_script( 'semantic-script' );
	wp_enqueue_script( 'pickadate-js' );
	wp_enqueue_script( 'pickadate-date-js' );
	wp_enqueue_script( 'pickadate-time-js' );
	wp_register_script( 'lckrm-bundle', plugins_url( 'assets/js/lckrm-bundle.js', LCKRM_Framework::instance()->plugin_file() ), array( 'jquery', 'wp-api' ), 'v0.1.1', true );
	wp_enqueue_script( 'bp-custom' );

	if ( bp_is_group() ) {

		wp_enqueue_script( 'lckrm-bundle' );
		wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', array(), '3.exp', true );
		wp_enqueue_script( 'sp-maps', plugin_dir_url( SP_PLUGIN_FILE ) .'assets/js/sp-maps.js', LCKRM_Framework::instance()->plugin_file(), array( 'jquery', 'google-maps' ), time(), true );
		wp_localize_script( 'sp-maps', 'vars', array( 'map_type' => strtoupper( get_option( 'sportspress_map_type', 'ROADMAP' ) ), 'zoom' => get_option( 'sportspress_map_zoom', 15 ) ) );
		
		$group_permalink = lckrm_get_group_permalink_by_id( bp_get_current_group_id() );
		$team_id = groups_get_groupmeta( bp_get_current_group_id(), 'team_bp_group_id', true );
		wp_localize_script( 'lckrm-bundle', 'lckrm_main', array(
			'teams' 		=> lckrm_get_posts_info( 'sp_team', '' ),
			'events' 		=> lckrm_get_posts_info( 'sp_event', '' ),
			'venues' 		=> lckrm_get_posts_info( '', 'sp_venue' ),
			'ajax'			=> admin_url('admin-ajax.php'),
			'group_team' 	=>	$team_id,
			'group_id'		=> bp_get_current_group_id(),
			'member_id'		=> get_current_user_id(),
			'group_permalink' => $group_permalink,
			'single_event_id' => isset( $_GET['event_id'] ) ? $_GET['event_id'] : '',
			'group_moderator' => is_super_admin() || bp_group_is_admin() ? 'yes' : 'no',
			'group_members'	  => lckrm_get_group_members( bp_get_current_group_id() ),
			'home_permalink'  => esc_url( home_url('/') ),
			'calendar_export' => lckrm_get_group_calendar_feeds(),
		) );
	}
}
add_action( 'wp_enqueue_scripts', 'lckrm_register_scripts', 20 );

function lckrm_init_rest_api_js(){
	if ( function_exists( 'json_api_client_js' ) && ! has_action( 'wp_enqueue_scripts', 'json_api_client_js' ) ) {
		add_action( 'wp_enqueue_scripts', 'json_api_client_js' );
		add_action( 'admin_enqueue_scripts', 'json_api_client_js' );
	}
}
add_action( 'init', 'lckrm_init_rest_api_js' );

function lckrm_add_role_capabilities() {
	/* Styles */
    wp_register_style( 'chosen-css', plugins_url( 'assets/css/chosen.css' ) );

	wp_enqueue_style( 'chosen-css' );
	/* Scripts */
    wp_register_script('chosen', plugins_url( 'assets/js/chosen.jquery.js', LCKRM_Framework::instance()->plugin_file() ), array('jquery'),'1.1', true);

	wp_register_script('chosen-proto', plugins_url( 'assets/js/chosen.proto.js', LCKRM_Framework::instance()->plugin_file() ), array('jquery'),'1.1', true);

    wp_enqueue_script( 'chosen' );
	wp_enqueue_script( 'chosen-proto' );

	$role_admin = get_role( 'administrator' );
	$caps = array( 'edit_sp_event', 'read_sp_event', 'delete_sp_event', 'edit_others_sp_events', 'publish_sp_events', 'read_private_sp_events', 'delete_sp_events', 'delete_private_sp_events', 'delete_published_sp_events', 'delete_others_sp_events', 'edit_private_sp_events', 'edit_published_sp_events', 'edit_sp_events' );
	
	foreach ( $caps as $cap ) {
		if ( ! $role_admin->has_cap( $cap ) ) {
    		$role->add_cap( $cap, true );
		}
	}

	$role_players = get_role( 'sp_player' );
	$player_caps = array( 'read_private_sp_events', 'read_sp_event' );
	foreach ( $player_caps as $cap ) {
		if ( ! $role_players->has_cap( $cap ) ) {
			$role_players->add_cap( $cap, true );
		}
	}
}
add_action( 'admin_init', 'lckrm_add_role_capabilities' );

function lckrm_add_cpts_to_api( $args ) {

	$args['show_in_rest'] = true;

	return $args;
	global $wp_post_types;

	// Add CPT slugs here
	$cpts = array( 'sp_event', 'sp_team', 'sp_player', 'sp_calendar' );

	foreach( $cpts as $key ) {
	// If the post type doesn't exist, skip it
		if( ! $wp_post_types[ $key ] )
			continue;

		$wp_post_types[ $key ]->show_in_rest = true;
		$wp_post_types[ $key ]->rest_base = $key;
	}
}
add_filter( 'sportspress_register_post_type_event', 'lckrm_add_cpts_to_api', 10 );
add_filter( 'sportspress_register_post_type_team', 'lckrm_add_cpts_to_api', 10 );
add_filter( 'sportspress_register_post_type_player', 'lckrm_add_cpts_to_api', 10 );
add_filter( 'sportspress_register_post_type_calendar', 'lckrm_add_cpts_to_api', 10 );

function lckrm_include_setting_page( $settings ) {
	$settings[] = include( 'class-sp-settings-group-notifications.php' );
	return $settings;
}
add_filter( 'sportspress_get_settings_config_pages', 'lckrm_include_setting_page', 10 );

function lckrm_enable_taxonomy_for_rest( $args ) {
	$args['show_in_rest'] = true;

	return $args;
}
add_filter( 'register_taxonomy_args', 'lckrm_enable_taxonomy_for_rest', 10 );

/**
 * Add the meta fields to REST API responses for posts read and write
 */
add_action( 'rest_api_init', 'lckrm_register_rest_api_fields' );
function lckrm_register_rest_api_fields() {

	require_once( dirname( dirname( __FILE__ ) ) . '/rest-api/events.php' );

	register_rest_field( 'sp_event',
		'teams',
		array(
			'get_callback'    => 'lckrm_get_teams',
			'update_callback' => 'lckrm_update_teams',
			'schema'          => null,
		)
	);

	register_rest_field( 'sp_event',
		'format',
		array(
			'get_callback'    => 'lckrm_get_sp_format',
			'update_callback' => 'lckrm_update_sp_format',
			'schema'          => null,
		)
	);

	register_rest_field( 'sp_event',
		'signed_players',
		array(
			'get_callback'    => 'lckrm_get_signed_players',
			'update_callback' => 'lckrm_update_signed_players',
			'schema'          => null,
		)
	);

	register_rest_field( 'sp_event',
		'venue',
		array(
			'get_callback'    => 'lckrm_get_sp_venue',
			'update_callback' => 'lckrm_update_sp_venue',
			'schema'          => null,
		)
	);

	register_rest_field( 'sp_event',
		'is_recurring',
		array(
			'get_callback'    => 'lckrm_get_recurring_event',
			'update_callback' => 'lckrm_update_recurring_event',
			'schema'          => null,
		)
	);

	register_rest_field( 'sp_event',
		'event_notifications',
		array(
			'get_callback'    => 'lckrm_get_event_notifications',
			'update_callback' => 'lckrm_update_event_notifications',
			'schema'          => null,
		)
	);

	register_rest_field( 'sp_event',
		'group_id_on_event',
		array(
			'get_callback'    => 'lckrm_get_group_id_on_event',
			'update_callback' => 'lckrm_update_group_id_on_event',
			'schema'          => null,
		)
	);

	register_rest_field( 'user',
		'cellphone',
		array(
			'get_callback'    => 'lckrm_get_member_cellphone',
			'update_callback' => 'lckrm_update_member_cellphone',
			'schema'          => null,
		)
	);

	add_filter( 'rest_prepare_sp_event', 'add_timezone_to_rest_dates', 10, 2 );
}

function lckrm_allow_meta_query( $valid_vars ) {
	
	$valid_vars = array_merge( $valid_vars, array( 'meta_key', 'meta_value' ) );
	return $valid_vars;
}
add_filter( 'rest_query_vars', 'lckrm_allow_meta_query' );

function add_timezone_to_rest_dates( $response, $post ){
	$tz_str = get_option( 'timezone_string' );
	$gmt_offset = get_option( 'gmt_offset' );

	if ( $tz_str ) {
		$tz = new DateTimeZone( $tz_str );
		$_date = new DateTime( $post->post_date, $tz );
		$offset_str = $_date->format( 'P' );
	} else if ( $gmt_offset ) {
		$gmt_offset *= 60;
		$hours = floor( abs( $gmt_offset ) / 60 );
		$minutes = ( abs( $gmt_offset ) % 60 );
		$offset_str = ( $gmt_offset < 0 ? '-' : '+' );
		$offset_str .= sprintf( '%02d:%02d', $hours, $minutes );
	} else {
		$offset_str = 'Z';
	}

	$response->data['date'] .= $offset_str;
	$response->data['date_gmt'] .= 'Z';

	return $response;
}

/**
 * Handlers for getting & updating custom field data.
 *
 *
 * Post meta SP_TEAMS
 */
function lckrm_get_teams( $object, $field_name, $request ) {
    return get_post_meta( $object[ 'id' ], 'sp_team', false );
}

function lckrm_update_teams( $value, $object, $field_name ) {
	if ( ! $value ) {
		return;
	}

	delete_post_meta( $object->ID, 'sp_team' );

	foreach ( $value as $team_id ) {
		add_post_meta( $object->ID, 'sp_team', $team_id );
	}
}

/**
 * User cellphone
 */

function lckrm_get_member_cellphone( $object, $field_name, $request ) {
	return bp_get_user_meta( $object[ 'id' ], 'lckrm_member_cellphone', true );
}

function lckrm_update_member_cellphone( $value, $object, $field_name ) {
	if ( ! $value ) {
		return;
	}

	return bp_update_user_meta( $object->ID, 'lckrm_member_cellphone', $value );
}

/**
 * Post meta group id on event post
 */

function lckrm_get_group_id_on_event( $object, $field_name, $request ) {
	return get_post_meta( $object[ 'id' ], 'lckrm_event_created_group', true );
}

function lckrm_update_group_id_on_event( $value, $object, $field_name ) {
	if ( ! $value ) {
		return;
	}

	return update_post_meta( $object->ID, 'lckrm_event_created_group', $value );
}


/**
 * Post meta Event NOTIFICATIONS
 */

function lckrm_get_event_notifications( $object, $field_name, $request ) {
	return get_post_meta( $object[ 'id' ], '_maybe_send_event_notifications', true );
}

function lckrm_update_event_notifications( $value, $object, $field_name ) {
	if ( ! $value ) {
		return;
	}

	return update_post_meta( $object->ID, '_maybe_send_event_notifications', $value );
}

/**
 * Post meta RECURRING EVENT
 */

function lckrm_get_recurring_event( $object, $field_name, $request ) {
	return get_post_meta( $object[ 'id' ], '_maybe_event_is_recurring', true );
}

function lckrm_update_recurring_event( $value, $object, $field_name ) {
	if ( ! $value ) {
		return;
	}

	return update_post_meta( $object->ID, '_maybe_event_is_recurring', $value );
}

/**
 * Post meta SP_FORMAT
 */
function lckrm_get_sp_format( $object, $field_name, $request ) {
    return get_post_meta( $object[ 'id' ], 'sp_format', true );
}


function lckrm_update_sp_format( $value, $object, $field_name ) {
	if ( ! $value ) {
		return;
	}

	return update_post_meta( $object->ID, 'sp_format', $value );
}

/**
 * Post meta SIGNED_PLAYERS
 */
function lckrm_get_signed_players( $object, $field_name, $request ) {
	return get_post_meta( $object['id'], '_event_signed_players', true );
}

function lckrm_update_signed_players( $value, $object, $field_name ) {
	if ( ! $value ) {
		return;
	}

	return update_post_meta( $object->ID, '_event_signed_players', $value );
}

/**
 * Tax SP_VENUE
 */
function lckrm_get_sp_venue( $object, $field_name, $request ) {
    $terms = get_the_terms( $object[ 'id' ], 'sp_venue', null);

    if ( ! $terms ) {
    	return 0;
    }
	$term = array_shift( $terms );
	$term->term_link = get_term_link( $term->term_id, 'sp_venue' );
	$term->term_link = is_wp_error( $term->term_link ) ? '' : $term->term_link;
	return $term ? $term : 0;
}


function lckrm_update_sp_venue( $value, $object, $field_name ) {
	if ( ! $value ) {
		return;
	}

	if ( is_object( $value ) ) {
		$terms = array( intval( $value->term_id ) );
	} else {
		$terms = array( intval( $value ) );
	}
	
	wp_set_post_terms( $object->ID, $terms, 'sp_venue' );

	return lckrm_get_sp_venue( array( 'id' => $object->ID ), 'venue', false );
}

function meta_box_team_instruction() {
	global $post;

	if ( ( ! empty( $post->post_type ) && $post->post_type == 'sp_event' ) || ( ! empty( $_GET['post_type'] ) && $_GET['post_type'] == 'sp_event' ) ) : ?>

		<script type='text/javascript'>
		(function($){
			$(document).ready(function(){
				if ( $('#sp_teamdiv').length ) {
					var home_team = $('#sp_teamdiv .sp-instance .sp-tab-select').eq(0),
						away_team = $('#sp_teamdiv .sp-instance .sp-tab-select').eq(1);
					$('<p>HOME</p>').insertBefore( home_team );
					$('<p>AWAY</p>').insertBefore( away_team );
				}
			});
		})(jQuery);
		</script>

	<?php
	endif;
}
add_action( 'admin_head', 'meta_box_team_instruction' );

function lckrm_register_sample_user_menu_item() {
	global $bp;

	if( bp_is_group() ) {

		bp_core_new_subnav_item( 
			array( 
				'name' => 'Message', 
				'slug' => 'message', 
				'parent_slug' => $bp->groups->current_group->slug, 
				'parent_url' => bp_get_group_permalink( $bp->groups->current_group ), 
				'position' => 10, 
				'item_css_id' => 'nav-message',
				'screen_function' => function(){
					bp_core_load_template( apply_filters( 'groups_template_group_message', 'groups/single/mass-message' ) );
				},
				'user_has_access' => is_super_admin() || bp_group_is_admin()
			) 
		);

		bp_core_new_subnav_item( 
			array( 
				'name' => 'Calendar', 
				'slug' => 'calendar', 
				'parent_slug' => $bp->groups->current_group->slug, 
				'parent_url' => bp_get_group_permalink( $bp->groups->current_group ), 
				'position' => 10, 
				'item_css_id' => 'nav-calendar',
				'screen_function' => create_function('',"bp_core_load_template( apply_filters( 'groups_template_group_calendar', 'groups/single/events-calendar' ) );"),
				'user_has_access' => true
			) 
		);

		bp_core_new_subnav_item( 
			array( 
				'name' => 'Invite', 
				'slug' => 'invite', 
				'parent_slug' => $bp->groups->current_group->slug, 
				'parent_url' => bp_get_group_permalink( $bp->groups->current_group ), 
				'position' => 80, 
				'item_css_id' => 'nav-invite',
				'screen_function' => create_function('',"bp_core_load_template( apply_filters( 'groups_template_group_invite', 'groups/single/groups-invite' ) );"),
				'user_has_access' => is_super_admin() || bp_group_is_admin()
			) 
		);

		bp_core_new_subnav_item( 
			array( 
				'name' => 'Event', 
				'slug' => 'event', 
				'parent_slug' => $bp->groups->current_group->slug, 
				'parent_url' => bp_get_group_permalink( $bp->groups->current_group ), 
				'position' => 10, 
				'item_css_id' => 'nav-event',
				'screen_function' => create_function('',"bp_core_load_template( apply_filters( 'groups_template_single_event', 'groups/single/single-event' ) );"),
				'user_has_access' => true
			) 
		);
 
		if ( bp_is_current_action( 'calendar' ) ) {
			add_action( 'bp_template_content_header', create_function( '', 'echo "' . esc_attr( '
				Calendar' ) . '";' ) );
			add_action( 'bp_template_title', create_function( '', 'echo "' . esc_attr( 'Calendar' ) . '";' ) );
		}

		if ( bp_is_current_action( 'invite' ) ) {
			add_action( 'bp_template_content_header', create_function( '', 'echo "' . esc_attr( '
				Invite' ) . '";' ) );
			add_action( 'bp_template_title', create_function( '', 'echo "' . esc_attr( 'Invite' ) . '";' ) );
		}

		if ( bp_is_current_action( 'event' ) ) {
			add_action( 'bp_template_content_header', create_function( '', 'echo "' . esc_attr( '
				Event' ) . '";' ) );
			add_action( 'bp_template_title', create_function( '', 'echo "' . esc_attr( 'Event' ) . '";' ) );
		}
	}
}
add_action( 'bp_actions', 'lckrm_register_sample_user_menu_item', 10 );

/*
function lckrm_modify_user_menu_item() {

	$nav_items = array( 'home', 'members', 'calendar', 'invite', 'admin' );

	foreach ( $nav_items as $nav_item ) {
		add_filter( 'bp_get_options_nav_' . $nav_item, function( $html ){
			return str_ireplace( array( '<li', '</li>' ), array( '<div', '</div>' ), $html );
		} );
	}
}
add_action( 'bp_setup_nav', 'lckrm_modify_user_menu_item', 10 );*/

function lckrm_get_new_members_ajax() {
	
	if( empty( $_POST['new_members'][0]['email'] ) ) {
		wp_send_json_error( array( 'response' => 'Empty request!' ) );
	}

	$new_members = $_POST['new_members'];
	$team_id = $_POST['team_id'];
	$group_id = $_POST['group_id'];
	$message = stripslashes( $_POST['message'] );
	
	foreach ( $new_members as $key => $member ) {
		if ( ! username_exists( $member['email'] ) ) {
			$userdata = array(
				'user_login'	=> $member['email'],
				'user_pass'		=> wp_generate_password( 20, true, false ),
				'user_email'	=> $member['email'],
				'display_name'	=> $member['name'],
				'first_name'	=> current( explode( ' ', $member['name'] ) ),
				'role'			=> 'sp_player'
			);
			$user_id = wp_insert_user( $userdata );

			if ( ! is_wp_error( $user_id ) ) {
				bp_update_user_meta( $user_id, 'lckrm_email_notifications', 'yes' );
				bp_update_user_meta( $user_id, 'lckrm_generated_pass', 'yes' );
				if ( ! empty( $member['phone'] ) && '1' !== $member['phone'] ) {
					bp_update_user_meta( $user_id, 'lckrm_member_cellphone', '+' . preg_replace( '/\D/', '', $member['phone'] ) );
					bp_update_user_meta( $user_id, 'lckrm_sms_notifications', 'yes' );
				}
				automatic_group_membership( $user_id, $group_id );
				lckrm_send_group_invitations( $user_id, $group_id, $message );
			}
		}
	}

	wp_send_json_success();
}
add_action( 'wp_ajax_get_new_members', 'lckrm_get_new_members_ajax', 10 );

function automatic_group_membership( $user_id, $group_id ) {
	if ( ! $user_id & ! $group_id ) {
		return false;
	}

	groups_accept_invite( $user_id, $group_id );
}
add_action( 'bp_core_activated_user', 'automatic_group_membership_on_invite' );

function lckrm_check_for_login_token() {
	if ( isset( $_GET['u_auth'] ) && ! empty( $_GET['u_auth'] ) ) {
		$u_auth = $_GET['u_auth'];
		$result = array();

		$decrypted_string = lckrm_decrypted_string( $u_auth );
		$user_id = substr( $decrypted_string, strpos( $decrypted_string, '|' ) + 1 );
		$redirect_to = ! empty( $_GET['redirect_to'] ) ? $_GET['redirect_to'] : home_url( '/' );

		if ( is_user_logged_in() && $user_id != get_current_user_id() ) {
			wp_safe_redirect( $redirect_to );
			exit;
		}

		$user = get_userdata( $user_id );

		$tokens = get_user_meta( $user->ID, '_lckrm_auth_tokens', true );
		if ( $tokens ) {
			foreach ( $tokens as $key => $token ) {
				$now = time();
				if ( $token['expires'] < $now ) {
					unset( $tokens[ $key ] );
					update_user_meta( $user->ID, '_lckrm_auth_tokens', $tokens );
				} else {
					if ( $decrypted_string == $token['token'] ) {
						$result['ID'] = $user->ID;
						$result['user_login'] = $user->user_login;

						unset( $tokens[ $key ] );
						update_user_meta( $user->ID, '_lckrm_auth_tokens', $tokens );

						break;
					}
				}
			}
		}

		// Auto log the user if result 
		if ( ! is_user_logged_in() && isset( $result['ID'] ) && isset( $result['user_login'] ) ) {
			wp_set_current_user( $result['ID'], $result['user_login'] );
			wp_set_auth_cookie( $result['ID'] );
			wp_safe_redirect( $redirect_to );
			exit;
		} elseif ( is_user_logged_in() ) {
			wp_safe_redirect( $redirect_to );
			exit;
		}
	}
}
add_action( 'login_init',  'lckrm_check_for_login_token', 10 );

function lckrm_set_mail_content_type( $content_type ) {
	return 'text/html';
}

add_filter( 'wp_mail_from', function( $from_email ) {
	$domain = parse_url( home_url(), PHP_URL_HOST );
	if ( $domain ) {
		return 'noreply@' . $domain;
	}

	return $from_email;
});

function event_members_attendance_ajax() {
	if ( empty( $_POST['event_id'] ) && empty( $_POST['member_choice'] ) ) {
		wp_send_json_error();
	}

	$event_id = $_POST['event_id'];
	$member_id = get_current_user_id();
	$member_choice = $_POST['member_choice'];
	$group_id = $_POST['group_id'];
	$signed_players = get_post_meta( $event_id, '_event_signed_players', true );

	$signed_players = $signed_players ? $signed_players : array();
	$signed_players[ $member_id ] = array(
		'member_choice' => $member_choice,
		'avatar' => bp_core_fetch_avatar( array( 'item_id' => $member_id, 'html' => false ) ),
		'name'	 => bp_core_get_user_displayname( $member_id ),
		'link'	 => bp_core_get_user_domain( $member_id ),
	);

	update_post_meta( $event_id, '_event_signed_players', $signed_players );

	$group_admins = groups_get_group_admins( $group_id );
	$event = get_post( $event_id );
	$msg = '<div style="display: block; width: 100%;">' . $signed_players[ $member_id ]['name'] . ' has chosen to ' . $signed_players[ $member_id ]['member_choice'] . ' the event ' . $event->post_title . ' with ID# ' . $event_id . ' on ' . $event->post_date . ' </div><a href="' . lckrm_get_group_permalink_by_id( $group_id ) . 'event/?event_id=' . $event_id . '">Event Link</a>';

	foreach ( $group_admins as $admin ) {
		$user_to_notify = get_userdata( $admin->user_id );

		add_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
		wp_mail( $user_to_notify->user_email, 'Player has chosen to ' . $signed_players[ $member_id ]['member_choice'] . ' event!', $msg );
		remove_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
	}

	wp_send_json_success();
}
add_action( 'wp_ajax_maybe_attend_event', 'event_members_attendance_ajax' );

function lckrm_send_notifications_on_event() {
	if ( empty( $_POST['event_action'] ) || empty( $_POST['group_id'] ) || empty( $_POST['event_id'] ) ) {
		wp_send_json_error();
	}

	global $bp;

	$action = $_POST['event_action'];
	$group_id = $_POST['group_id'];
	$event_id = $_POST['event_id'];
	$event = get_post( $event_id );
	$group = groups_get_group( array( 'group_id' => $group_id ) );
	$group_args = array(
		'group_id' => $group_id,
		'per_page' => 99999, // -1 is not supported :(
		// 'exclude' => array( get_current_user_id() ),
		'exclude_admins_mods' => false,
	);

	if ( bp_group_has_members( $group_args ) ) {
		$event_title = get_the_title( $event->ID );
		$event_date = date( 'j/n/Y g:i a', strtotime( $event->post_date ) );
		$event_link = lckrm_get_group_permalink_by_id( $group_id ) . 'event/?event_id=' . $event->ID;

		while ( bp_group_members() ) {
			bp_group_the_member();
			$user = get_userdata( bp_get_member_user_id() );

			if ( 'no' != bp_get_user_meta( $user->ID, 'lckrm_email_notifications', true ) ) {
				$to = $user->user_email;
				$subject = 'Event from ' . $group->name . ' group has been ' . $action;
				if ( 'cancelled' == $action  ) {
					$message = lckrm_get_message_contents( 'Deleted Event', array( 'event' => $event, 'group' => $group, 'user' => $user ) );
				} elseif ( 'created' == $action ) {
					$message = lckrm_get_message_contents( 'New Event', array( 'event' => $event, 'group' => $group, 'user' => $user, 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'location' => 'event', 'group_id' => $group_id, 'event_id' => $event_id ) ) ) );
				} else {
					$message = lckrm_get_message_contents( 'Edited Event', array( 'event' => $event, 'group' => $group, 'user' => $user, 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'location' => 'event', 'group_id' => $group_id, 'event_id' => $event_id ) ) ) );
				}	

				add_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
				wp_mail( $to, $subject, $message );
				remove_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
			}

			$user_number = bp_get_user_meta( $user->ID, 'lckrm_member_cellphone', true );

			if ( LCKRM_Twilio::is_available() && 'no' != bp_get_user_meta( $user->ID, 'lckrm_sms_notifications', true ) && $user_number ) {
				$to = $user_number;
				if ( 'cancelled' == $action  ) {
					$message = lckrm_get_message_contents( 'Deleted Event', array( 'event' => $event, 'group' => $group, 'user' => $user, 'type' => 'SMS' ) );
				} elseif ( 'created' == $action ) {
					$message = lckrm_get_message_contents( 'New Event', array( 'event' => $event, 'group' => $group, 'user' => $user, 'type' => 'SMS', 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'location' => 'event', 'group_id' => $group_id, 'event_id' => $event_id ) ) ) );
				} else {
					$message = lckrm_get_message_contents( 'Edited Event', array( 'event' => $event, 'group' => $group, 'user' => $user, 'type' => 'SMS', 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'location' => 'event', 'group_id' => $group_id, 'event_id' => $event_id ) ) ) );
				}

				LCKRM_Twilio::send_sms( $to, $message );
			}
		}
	}
		
	wp_send_json_success();
}
add_action( 'wp_ajax_notify_memebers_on_event', 'lckrm_send_notifications_on_event', 10 );

function maybe_create_sp_team_bp_group( $post_id ) {
	
	$post = get_post( $post_id );

	if ( 'sp_team' == $post->post_type && 'yes' == get_post_meta( $post_id, 'sp_create_group', true ) ) {
		$bpgroups = $groups = BP_Groups_Group::get( array(
			'type'=>'alphabetical',
			'per_page'=> -1,
		));

		foreach ( $bpgroups['groups'] as $group ) {
			$group_team = groups_get_groupmeta( $group->id, 'team_bp_group_id', true );
			if ( $group_team && $group_team == $post_id ) {
				update_post_meta( $post_id, '_lckrm_team_group_id', $group->id );

				return;
			}
		}

		$args = array(
			'group_id'     => 0,
			'creator_id'   => get_current_user_id(),
			'name'         => get_the_title( $post_id ),
			'description'  => '',
			'slug'         => $post->post_name,
			'status'       => 'private',
			'enable_forum' => 0,
			'date_created' => bp_core_current_time(),
		);

		$group_id = groups_create_group( $args );
		groups_update_groupmeta( $group_id, 'team_bp_group_id', $post_id );

		update_post_meta( $post_id, '_lckrm_team_group_id', $group_id );

		// $players_list_id = get_post_meta( $post_id, 'sp_list', true );
		// $players_list = get_post_meta( $players_list_id, 'sp_players', true );

		// if ( $players_list ) {
		// 	foreach ( $players_list as $key => $players ) {
		// 		groups_join_group( $group_id, $key );
		// 	}
		// }
	}
}
add_action( 'save_post', 'maybe_create_sp_team_bp_group', 20 );


function events_notification_settings() {

	if ( bp_action_variables() ) {
		bp_do_404();
		return;
	}

	if ( ! $email_notifications = bp_get_user_meta( bp_displayed_user_id(), 'lckrm_email_notifications', true ) ) {
		$email_notifications = 'yes';
	}

	if ( ! $sms_notifications = bp_get_user_meta( bp_displayed_user_id(), 'lckrm_sms_notifications', true ) ) {
		$sms_notifications = 'yes';
	} ?>

	<table class="notification-settings" id="events-notification-settings">
		<thead>
			<tr>
				<th class="icon"></th>
				<th class="title"><?php _e( 'Events', 'buddypress' ) ?></th>
				<th class="yes"><?php _e( 'Yes', 'buddypress' ) ?></th>
				<th class="no"><?php _e( 'No', 'buddypress' )?></th>
			</tr>
		</thead>

		<tbody>
			<tr id="events-notification-settings-email">
				<td></td>
				<td><?php _e( 'Get an email when an event is created, modified or cancelled', 'buddypress' ) ?></td>
				<td class="yes"><input type="radio" name="notifications[lckrm_email_notifications]" id="events-email-notifications-yes" value="yes" <?php checked( $email_notifications, 'yes', true ) ?>/><label for="events-email-notifications-yes" class="bp-screen-reader-text"><?php _e( 'Yes, send email', 'buddypress' ); ?></label></td>
				<td class="no"><input type="radio" name="notifications[lckrm_email_notifications]" id="events-email-notifications-no" value="no" <?php checked( $email_notifications, 'no', true ) ?>/><label for="events-email-notifications-no" class="bp-screen-reader-text"><?php _e( 'No, do not send email', 'buddypress' ); ?></label></td>
			</tr>
			<tr id="events-notification-settings-sms">
				<td></td>
				<td><?php _e( 'Get a text when an event is created, modified or cancelled', 'buddypress' ) ?></td>
				<td class="yes"><input type="radio" name="notifications[lckrm_sms_notifications]" id="events-sms-notifications-yes" value="yes" <?php checked( $sms_notifications, 'yes', true ) ?>/><label for="events-sms-notifications-yes" class="bp-screen-reader-text"><?php _e( 'Yes, send me a text', 'buddypress' ); ?></label></td>
				<td class="no"><input type="radio" name="notifications[lckrm_sms_notifications]" id="events-sms-notifications-no" value="no" <?php checked( $sms_notifications, 'no', true ) ?>/><label for="events-sms-notifications-no" class="bp-screen-reader-text"><?php _e( 'No, do not send me a text', 'buddypress' ); ?></label></td>
			</tr>

			<?php

			/**
			 * Fires inside the closing </tbody> tag for messages screen notification settings.
			 *
			 * @since 1.0.0
			 */
			do_action( 'events_notification_settings' ); ?>
		</tbody>
	</table>

<?php
}
add_action( 'bp_notification_settings', 'events_notification_settings', 0 );

function lckrm_render_edit_phone_field() {
	if ( 1 != bp_get_current_profile_group_id() || 'edit' != bp_current_action() ) {
		return;
	}

	$phone = ltrim( bp_get_user_meta( bp_displayed_user_id(), 'lckrm_member_cellphone', true ), '+' ); ?>

	<div class="editfield">
		<label for="member_cellphone">Cellphone Number</label>
		+ <input type="number" min="1" step="1" name="member_cellphone" id="member_cellphone" value="<?php echo $phone ? esc_attr( $phone ) : '1'; ?>" />

		<div class="field-visibility-settings-notoggle">
			This number is used for sending you SMS notifications and is only visible to you and your coaches.<br />
			Please enter your full phone number(including area code)
		</div>
	</div>
	<?php
}
add_action( 'bp_after_profile_field_content', 'lckrm_render_edit_phone_field', 10 );

function lckrm_maybe_save_phone_field( $user_id ) {
	if ( isset( $_POST['member_cellphone'] ) ) {
		$phone = absint( $_POST['member_cellphone'] );
		if ( empty( $phone ) ) {
			bp_update_user_meta( $user_id, 'lckrm_member_cellphone', '' );
		} else {
			bp_update_user_meta( $user_id, 'lckrm_member_cellphone', '+' . preg_replace( '/\D/', '', $phone ) );
		}
	}
}
add_action( 'xprofile_updated_profile', 'lckrm_maybe_save_phone_field', 10 );

function lckrm_maybe_hide_profile_page_title( $should_display ) {
	if ( bp_is_user() && get_current_user_id() && bp_displayed_user_id() == get_current_user_id() ) {
		$should_display = false;
	}

	return $should_display;
}
add_filter( 'lckrm/buddypress/should_display_page_title', 'lckrm_maybe_hide_profile_page_title', 10 );

function lckrm_maybe_send_notifications_on_group_activity( $activity ) {
	// Make sure we're looking at a groop activity_update and that we want to send notifications
	if ( 'groups' == $activity->component && 'activity_update' == $activity->type && ! empty( $_POST['group-activity-send-notifications'] ) ) {
		// Make sure the user 
		if ( is_super_admin() || groups_is_user_admin( $activity->user_id, $activity->item_id ) ) {
			$group_id = $activity->item_id;
			$user_id = $activity->user_id;
			$group = groups_get_group( array( 'group_id' => $group_id ) );
			$group_args = array(
				'group_id' => $group_id,
				'per_page' => 99999, // -1 is not supported :(
				// 'exclude' => array( $user_id ),
				'exclude_admins_mods' => false,
			);

			if ( bp_group_has_members( $group_args ) ) {
				$sender = get_userdata( $user_id );
				$activity_thread_link = bp_activity_get_permalink( $activity->id, $activity );
				$email_subject = $group->name . ' - new update';

				$activity_content = 100 < mb_strlen( $activity->content ) ? mb_substr( $activity->content, 0, 97 ) . '...' : $activity->content;

				while ( bp_group_members() ) {
					bp_group_the_member();
					$user = get_userdata( bp_get_member_user_id() );

					if ( 'no' != bp_get_user_meta( $user->ID, 'lckrm_email_notifications', true ) ) {
						$email_message = lckrm_get_message_contents( 'New Team Message', array( 'group' => $group_id, 'team_message' => $activity_content, 'user' => $user, 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'redirect_to' => $activity_thread_link ) ) ) );
						$to = $user->user_email;
						
						add_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
						wp_mail( $to, $email_subject, $email_message );
						remove_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
					}

					$user_number = bp_get_user_meta( $user->ID, 'lckrm_member_cellphone', true );

					if ( LCKRM_Twilio::is_available() && 'no' != bp_get_user_meta( $user->ID, 'lckrm_sms_notifications', true ) && $user_number ) {
						LCKRM_Twilio::send_sms( $user_number, lckrm_get_message_contents( 'New Team Message', array( 'group' => $group_id, 'team_message' => $activity_content, 'user' => $user, 'type' => 'SMS', 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'redirect_to' => $activity_thread_link ) ) ) ) );
					}
				}
			}
		}
	}
}
add_action( 'bp_activity_after_save', 'lckrm_maybe_send_notifications_on_group_activity', 10 );


function lckrm_rest_class_to_cpt( $args ) {
	$args['rest_controller_class'] = 'LCKRM_REST_Events_v1';
	$args['show_in_rest'] = true;
	$args['rest_base'] = 'sp_event';

	return $args;
}
add_filter( 'sportspress_register_post_type_event', 'lckrm_rest_class_to_cpt', 500000 );

register_activation_hook( __FILE__, 'lckrm_init_upcoming_events_cron_hook' );
register_deactivation_hook( __FILE__, 'lckrm_init_upcoming_events_cron_hook' );

function lckrm_init_upcoming_events_cron_hook() {
	wp_schedule_event( time(), 'five_minutes', 'lckrm_check_for_upcoming_events' );
}

function lckrm_remove_upcoming_events_cron_hook() {
	wp_clear_scheduled_hook( 'lckrm_check_for_upcoming_events' );
}

function lckrm_add_five_minute_cron_interval($schedules) {
	$schedules['five_minutes'] = array(
		'interval' => 5 * MINUTE_IN_SECONDS,
		'display' => __( 'Every 5 minutes' ),
	);

	return $schedules;
}
add_filter( 'cron_schedules', 'lckrm_add_five_minute_cron_interval'); 

add_action( 'lckrm_check_for_upcoming_events', 'check_upcoming_events' );
function check_upcoming_events() {

	$args = array(
		'post_type' => 'sp_event',
		'post_status' => array( 'publish', 'future' ),
		'meta_query'  => array(
			array(
				'key' => 'sent_2h_notification',
				'value' => 'sent',
				'compare' => 'NOT EXISTS',
			),
		),
		'date_query' => array(
			'after' => date( 'Y-m-d H:i:s' ),
			'before' => date( 'Y-m-d H:i:s', strtotime( 'now +2 hours' ) ),
		),
		'posts_per_page' => -1,
	);

	$query = new WP_Query( $args );
	$events = $query->posts;

	foreach ( $events as $event ) {
		$group_id = get_post_meta( $event->ID, 'lckrm_event_created_group', true );

		update_post_meta( $event->ID, 'sent_2h_notification', 'sent' );
		lckrm_send_2h_notify_before_event( $event, $group_id );
	}

	$args = array(
		'post_type' => 'sp_event',
		'post_status' => array( 'publish', 'future' ),
		'meta_query'  => array(
			array(
				'key' => 'check_24h_rsvp_before_event',
				'value' => 'checked',
				'compare' => 'NOT EXISTS',
			),
		),
		'date_query' => array(
			'after' => date( 'Y-m-d H:i:s' ),
			'before' => date( 'Y-m-d H:i:s', strtotime( 'now +24 hours' ) ),
		),
		'posts_per_page' => -1,
	);

	$query = new WP_Query( $args );
	$events = $query->posts;

	
	foreach ( $events as $event ) {
		$group_id = get_post_meta( $event->ID, 'lckrm_event_created_group', true );
		$signed_players = get_post_meta( $event->ID, '_event_signed_players', true );


		$group_args = array(
			'group_id' => $group_id,
			'per_page' => 99999,
		);

		if ( bp_group_has_members( $group_args ) ) {
			while ( bp_group_members() ) {
				bp_group_the_member();
				$user = get_userdata( bp_get_member_user_id() );
				if ( ! array_key_exists( $user->ID , $signed_players ) ) {
					
					lckrm_send_24h_rsvp_before_event( $event, $user, $group_id );
					update_post_meta( $event->ID, 'check_24h_rsvp_before_event', 'checked' );
				}
			}
		}
	}
}

function lckrm_maybe_update_user_pass() {
	$user_id = bp_displayed_user_id();

	if ( 'yes' == bp_get_user_meta( $user_id, 'lckrm_generated_pass', true ) ) {
		if ( ! ( empty( $_POST['lckrm_pass1'] ) && empty( $_POST['lckrm_pass2'] ) ) ) {
			$update_user = get_userdata( $user_id );
			$pass_changed = false;
			$pass_error = false;
			$feedback_type = 'error';

			if ( !empty( $_POST['lckrm_pass1'] ) && !empty( $_POST['lckrm_pass2'] ) ) {

				if ( ( $_POST['lckrm_pass1'] == $_POST['lckrm_pass2'] ) && !strpos( " " . $_POST['lckrm_pass1'], "\\" ) ) {

					// Password change attempt is successful
					$update_user->user_pass = $_POST['lckrm_pass1'];
					$pass_changed = true;

				// Password change attempt was unsuccessful
				} else {
					$pass_error = 'mismatch';
				}

			// Both password fields were empty
			} elseif ( empty( $_POST['lckrm_pass1'] ) && empty( $_POST['lckrm_pass2'] ) ) {
				$pass_error = false;

			// One of the password boxes was left empty
			} elseif ( ( empty( $_POST['lckrm_pass1'] ) && !empty( $_POST['lckrm_pass2'] ) ) || ( !empty( $_POST['lckrm_pass1'] ) && empty( $_POST['lckrm_pass2'] ) ) ) {
				$pass_error = 'empty';
			}

			// The structure of the $update_user object changed in WP 3.3, but
			// wp_update_user() still expects the old format
			if ( isset( $update_user->data ) && is_object( $update_user->data ) ) {
				if ( $pass_changed && empty( $update_user->data->user_pass ) ) {
					$update_user->data->user_pass = $update_user->user_pass;
				}
				$update_user = $update_user->data;
				$update_user = get_object_vars( $update_user );

				// Unset the password field to prevent it from emptying out the
				// user's user_pass field in the database.
				// @see wp_update_user()
				if ( false === $pass_changed ) {
					unset( $update_user['user_pass'] );
				}
			}

			// Clear cached data, so that the changed settings take effect
			// on the current page load
			if ( ( false === $pass_error ) && ! is_wp_error( wp_update_user( $update_user ) ) ) {
				wp_cache_delete( 'bp_core_userdata_' . bp_displayed_user_id(), 'bp' );
				wp_cache_delete( $user_id, 'users' );
				$bp->displayed_user->userdata = bp_core_get_core_userdata( bp_displayed_user_id() );
				// Will hide the change password nag
				bp_delete_user_meta( $user_id, 'lckrm_generated_pass' );
			} else {
				$pass_changed = false;
			}

			// Password feedback
			switch ( $pass_error ) {
				case 'mismatch' :
					$feedback['pass_mismatch'] = __( 'The password fields did not match.', 'buddypress' );
					break;
				case 'empty' :
					$feedback['pass_empty']    = __( 'One of the password fields was empty.', 'buddypress' );
					break;
				case false :
					// No change
					break;
			}

			// No errors so show a simple success message
			if ( false == $pass_error && true === $pass_changed ) {
				$feedback[]    = __( 'Your password has been set.', 'buddypress' );
				$feedback_type = 'success';

			// Some kind of errors occurred
			} elseif ( false === $pass_error && false === $pass_changed ) {
				if ( bp_is_my_profile() ) {
					$feedback['nochange'] = __( 'No changes were made to your account.', 'buddypress' );
				} else {
					$feedback['nochange'] = __( 'No changes were made to this account.', 'buddypress' );
				}
			}

			// Set the feedback
			bp_core_add_message( implode( "\n", $feedback ), $feedback_type );
		} else {
			@setcookie( 'bp-message',      $message, time() - 60 * 60 * 24, COOKIEPATH );
			@setcookie( 'bp-message-type', $type,    time() - 60 * 60 * 24, COOKIEPATH );

			// Get BuddyPress
			$bp = buddypress();

			/***
			 * Send the values to the $bp global so we can still output messages
			 * without a page reload
			 */
			$bp->template_message      = false;
			$bp->template_message_type = false;
		}
	}
}
add_action( 'bp_core_general_settings_after_save', 'lckrm_maybe_update_user_pass', 10 );

function lckrm_maybe_change_username() {
	global $wpdb;

	// var_dump( bp_displayed_user_domain() );
	// exit;

	// var_dump( class_exists( 'BP_Username_Change_Helper' ) && ! empty( $_POST['new_user_name'] ) );
	// exit;
	if ( class_exists( 'BP_Username_Change_Helper' ) && ! empty( $_POST['new_user_name'] ) ) {
		$bp = buddypress();

		$error = new WP_Error();
		$is_super_admin = false;
		$user_id = bp_displayed_user_id();

		//check_admin_referer('bp_settings_change_username');
		$new_user_name = $_POST['new_user_name'];
		// username_exists() references the userlogins object cache, so we must clear
		// it before using the function
		wp_cache_delete( $new_user_name, 'userlogins' );
		wp_cache_delete( $_POST['current_user_name'], 'userlogins' );

		//if the username is empty or invalid
		if ( empty( $new_user_name ) || ! validate_username( $new_user_name ) ) {
			$error->add( 'invalid', __( 'Please enter a valid Username!', 'bp-username-changer' ) );
		} elseif ( $bp->displayed_user->userdata->user_login == $new_user_name ) {
			$error->add( 'nochange', __( 'Please enter a different Username!', 'bp-username-changer' ) );
		} elseif ( username_exists( $new_user_name ) ) {
			$error->add( 'exiting_username', sprintf( __( 'The Username %s already exists. Please use a different username!', 'bp-username-changer' ), $new_user_name ) );
		} elseif ( BP_Username_Change_Helper::get_instance()->is_reserved_name( $new_user_name ) ) {
			$error->add( 'reserved_usernam', sprintf( __( 'The Username %s is reserved. Please choose a differenet username!' ), $new_user_name ) );
		}

		$error = apply_filters( 'bp_username_changer_validation_errors', $error, $new_user_name );
		//if there was an error
		//show error &redirect
		if ( $error->get_error_code() ) {
			bp_core_add_message( $error->get_error_message(), 'error' );
			return;
		}

		//if it is multisite, before change the username, revoke the admin capability
		if ( is_multisite() && is_super_admin( $user_id ) ) {

			if ( ! function_exists( 'revoke_super_admin' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/ms.php' );
			}

			$is_super_admin = true;

			revoke_super_admin( $user_id );
		}
		/* now, update the user_login / user_nicename in the database */
		// this will update user_nicename
		// wp_update_user() doesn't update user_login when updating a user... sucks!
		wp_update_user( array(
			'ID'			=> $user_id,
			'user_login'	=> $new_user_name,
			'user_nicename' => sanitize_title( $new_user_name )
		) );

		// manually update user_login
		$wpdb->update( $wpdb->users, array( 'user_login' => $new_user_name ), array( 'ID' => $user_id ), array( '%s' ), array( '%d' ) );

		// delete object cache
		clean_user_cache( $user_id );
		wp_cache_delete( $user_id, 'users' );
		wp_cache_delete( 'bp_core_userdata_' . $user_id, 'bp' );
		wp_cache_delete( 'bp_user_username_' . $user_id, 'bp' );
		wp_cache_delete( 'bp_user_domain_' . $user_id, 'bp' );

		// reset auth cookie for new user_login
		// only do this if the current user is attempting to change their own username
		// copies auth cookie logic from wp_update_user()
		if ( bp_is_my_profile() ) {
			wp_clear_auth_cookie();
			// Here we calculate the expiration length of the current auth cookie and compare it to the default expiration.
			// If it's greater than this, then we know the user checked 'Remember Me' when they logged in.
			$logged_in_cookie = wp_parse_auth_cookie( '', 'logged_in' );

			/** This filter is documented in wp-includes/pluggable.php */
			$default_cookie_life = apply_filters( 'auth_cookie_expiration', ( 2 * DAY_IN_SECONDS ), $user_id, false );
			$remember = ( ( $logged_in_cookie['expiration'] - time() ) > $default_cookie_life );

			wp_set_auth_cookie( $user_id, $remember );
		}

		//if multisite and the user was super admin, mark him back as super admin
		if ( is_multisite() && $is_super_admin ) {
			grant_super_admin( $user_id );
		}

		// add message
		bp_core_add_message( __( 'Username Changed Successfully!', 'bp-username-changer' ) );

		// fetch the user object just in case plugins altered the user_login
		$user = new WP_User( $user_id );
		// hook for plugins
		do_action( 'bp_username_changed', $new_user_name, $bp->displayed_user->userdata, $user );

		$bp->displayed_user->domain = bp_core_get_user_domain( $user_id, $user->user_nicename, $user->user_login ) . '/';

		/*str_ireplace( '/' . $_POST['current_user_name'] . '/', '/' . $new_user_name . '/', $bp->displayed_user->domain );*/
	}
}
add_action( 'bp_core_general_settings_after_save', 'lckrm_maybe_change_username', 10 );

function lckrm_maybe_render_password_nag() {
	// bp_update_user_meta( get_current_user_id(), 'lckrm_generated_pass', true )
	// var_dump( bp_get_user_meta( get_current_user_id(), 'lckrm_generated_pass', true ) );
	if ( 'yes' != bp_get_user_meta( get_current_user_id(), 'lckrm_generated_pass', true ) ) {
		return;
	}

	$settings_slug = bp_get_settings_slug();
	$user_domain   = bp_loggedin_user_domain();
	$settings_link = trailingslashit( $user_domain . $settings_slug ); ?>
	<div class="ui negative message" style="margin-top: 15px;">
		<i class="close icon"></i>
		<div class="header">
			You are still using your invitation account and auto-generated password!
		</div>
		<p>Please change them <a href="<?php echo esc_url( $settings_link ); ?>"><strong>here</strong></a>.</p>
	</div>
	<?php
}
add_action( 'bp_after_groups_item_nav', 'lckrm_maybe_render_password_nag', 10 );
add_action( 'bp_before_member_body', 'lckrm_maybe_render_password_nag', 10 );


/* Remove change username tab */
if ( class_exists( 'BP_Username_Change_Helper' ) ) {
	remove_action( 'bp_setup_nav', array( BP_Username_Change_Helper::get_instance(), 'nav_setup' ), 11 );
}

function lckrm_members_admin_bar_my_account_menu() {
	global $wp_admin_bar;

	// Only show if viewing a user.
	if ( ! bp_is_user() && ! is_user_logged_in() ) {
		return false;
	}

	$bp = buddypress();
	$user_domain = bp_loggedin_user_domain();
	// Unique ID for the 'My Account' menu.

	// Add the top-level User Admin button.
	$wp_admin_bar->add_menu( array(
		'parent' => 'my-account-buddypress',
		'id'    => 'lckrm_admin_menu_groups',
		'title' => __( 'Groups', 'buddypress' ),
		'href'  => bp_core_get_user_domain( get_current_user_id() ) . 'groups/',
	) );

	$vgroups = BP_Groups_Group::get(array(
							'per_page'=>999,
							'user_id' => get_current_user_id(),
							'show_hidden' => true,
							));

	if ( ! empty( $vgroups ) ) {
		foreach ( $vgroups as $tgroups ) {
			if ( is_array( $tgroups ) ) {
				foreach ($tgroups as $group) {
					$group_link = lckrm_get_group_permalink_by_id( $group->id );
					$group_team = groups_get_groupmeta( $group->id, 'team_bp_group_id', true );
					$group_avatar = wp_get_attachment_image_src( get_post_thumbnail_id( $group_team ), 'medium' );	
					$wp_admin_bar->add_menu( array(
						'parent'    => 'lckrm_admin_menu_groups',
						'id'        => 'lckrm_admin_menu_group_' . $group->id,
						'title'     => '<img style="height: 35px; float: left; margin-right: 10px;" src="'. $group_avatar[0] .'" /> ' . $group->name,
						'href'      => $group_link,
					) );
				}
			}
		}
	}

	// Add the top-level Activity button.
	$wp_admin_bar->add_menu( array(
		'parent'    => 'my-account-buddypress',
		'id'        => 'lckrm_admin_menu_activity',
		'title'     => '<i class="fa fa-pencil-square-o"></i> Activity',
		'href'      => bp_core_get_user_domain( get_current_user_id() ) . 'activity/',
	) );

	$messages_slug  = bp_get_messages_slug();
	$messages_link  = trailingslashit( $user_domain . $messages_slug );
	$messages_count = bp_get_total_unread_messages_count();
	$messages_class = ( 0 === $messages_count ) ? 'no-count' : 'count';
	$messages_title = sprintf( __( '<i class="fa fa-envelope-o"></i> Messages <span class="ui grey circular label %s" style="float: right;">%s</span>', 'buddypress' ), esc_attr( $messages_class ), bp_core_number_format( $messages_count ) );

	$wp_admin_bar->add_menu( array(
		'parent'    => 'my-account-buddypress',
		'id'        => 'lckrm_admin_menu_messages',
		'title'     => $messages_title,
		'href'      => $messages_link,
	) );

	$notifications_slug  = bp_get_notifications_slug();
	$notifications_link  = trailingslashit( $user_domain . $notifications_slug );
	$notifications_count = bp_notifications_get_unread_notification_count( bp_displayed_user_id() );
	$notifications_class = ( 0 === $notifications_count ) ? 'no-count' : 'count';
	$notifications_title = sprintf( _x( '<i class="fa fa-bell-o"></i> Notifications <span class="ui grey circular label %s" style="float: right;">%s</span>', 'Profile screen nav', 'buddypress' ), esc_attr( $notifications_class ), bp_core_number_format( $notifications_count ) );

	// Add the top-level Notifications button.
	$wp_admin_bar->add_menu( array(
		'parent'    => 'my-account-buddypress',
		'id'        => 'lckrm_admin_menu_notifications',
		'title'     => $notifications_title,
		'href'      => $notifications_link,
	) );

	$wp_admin_bar->add_menu( array(
		'parent'    => 'my-account-buddypress',
		'id'        => 'lckrm_admin_menu_settings',
		'title'     => '<i class="fa fa-cog"></i> Settings',
		'href'      => $user_domain . 'settings/',
	) );

	$wp_admin_bar->add_menu( array(
		'parent'    => 'my-account-buddypress',
		'id'        => 'lckrm_admin_menu_logout',
		'title'     => 'Logout',
		'href'      => wp_logout_url(),
	) );
}
add_action( 'admin_bar_menu', 'lckrm_members_admin_bar_my_account_menu', 99 );

function lckrm_direct_member_msg_ajax() {
	if ( 'false' == $_POST['mail'] && 'false' == $_POST['text'] ) {
		wp_send_json_error( array( 'on_error' => 'No mail or text has been chosen.' ) );
	}

	$user = get_userdata( $_POST['user_id'] );
	$group = groups_get_group( array( 'group_id' => $_POST['group_id'] ) );
	$user_number = bp_get_user_meta( $user->ID, 'lckrm_member_cellphone', true );
	$to = $user->user_email;
	$subject = 'Message from ' . $group->name;
	$message = $_POST['msg'];

	if ( 'false' != $_POST['mail'] ) {
		add_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
		wp_mail( $to, $subject, $message );
		remove_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
	}

	if ( 'false' != $_POST['text'] && LCKRM_Twilio::is_available() && $user_number ) {
		$to = $user_number;
		LCKRM_Twilio::send_sms( $to, $message );	
	}

	wp_send_json_success();
}
add_action( 'wp_ajax_direct_memeber_messages', 'lckrm_direct_member_msg_ajax', 10 );

function lckrm_filter_rest_user_query( $args, $request ) {
	if ( ! function_exists( 'bp_is_active' ) || ! bp_is_active( 'groups' ) ) {
		return $args;
	}

	if ( ! empty( $request['lckrm_users_for_group'] ) ) {
		$group_id = absint( $request['lckrm_users_for_group'] );
		if ( ! is_super_admin() && ! groups_is_user_member( get_current_user_id(), $group_id ) ) {
			return $args;
		}

		$member_ids = lckrm_get_group_members( $group_id );
		unset( $args['has_published_posts'] );
		$args['include'] = ! empty( $request['filter']['include'] ) ? array_intersect( $request['filter']['include'], $member_ids ) : $member_ids;
	}

	return $args;
}
add_filter( 'rest_user_query', 'lckrm_filter_rest_user_query', 10, 2 );

function lckrm_group_admin_settings() {
	global $bp;
	$args = array(
		'post_type' => 'sp_calendar',
		'posts_per_page' => -1
	);
	$query = new WP_Query( $args );

	$export = groups_get_groupmeta( $bp->groups->current_group->id, 'group_calendar_export', true ); ?>
	<h4>Calendar Options</h4>
	<div class="ui form">
	<p>Selected calendar for export.</p>
		<select class="ui fluid dropdown" name="calendar_export" value="">
			<option value="" hidden disabled selected>Select Calendar</option>
			<option value="" <?php selected( $export, $calendar->ID ); ?>>Disable Calendar Export</option>
			<?php foreach ( $query->posts as $calendar ): ?>
				<option <?php selected( $export, $calendar->ID ); ?> value="<?php echo $calendar->ID; ?>"><?php echo get_the_title( $calendar->ID ); ?></option>
			<?php endforeach; ?>
		</select>
	</div>

	<?php
}
add_action( 'bp_before_group_settings_admin', 'lckrm_group_admin_settings', 10 );

function lckrm_on_save_group_admin_settings( $bp_groups_current_group_id ) {
	if ( ! isset( $_POST['calendar_export'] ) ) {
		return;
	}

	groups_update_groupmeta( $bp_groups_current_group_id, 'group_calendar_export', $_POST['calendar_export'] );
}
add_action( 'groups_group_settings_edited', 'lckrm_on_save_group_admin_settings', 10, 1 );

function lckrm_messages_notification_new_message( $raw_args ) {
	static $thread_ids = array();

	if ( ! empty( $GLOBALS['lckrm_no_message_notifications'] ) ) {
		return;
	}

	// Cast possible $message object as an array.
	if ( is_object( $raw_args ) ) {
		$args = (array) $raw_args;
	} else {
		$args = $raw_args;
	}

	if ( ! isset( $thread_ids[ $args['thread_id'] ] ) ) {
		$has_groups = groups_get_groups( array(
			'per_page'   => 1,
			'meta_query' => array(
				array(
					'key'     => '_lckrm_mass_message_id',
					'value'   => $args['thread_id'],
					'compare' => '=',
				),
			),
		) );

		if ( 0 != $has_groups['total'] ) {
			$thread_ids[ $args['thread_id'] ] = 'no_notifications';
		} else {
			$thread_ids[ $args['thread_id'] ] = 'send_notifications';
		}
	}

	if ( 'send_notifications' == $thread_ids[ $args['thread_id'] ] ) {
		messages_notification_new_message( $raw_args );
	}
}
if ( has_action( 'messages_message_sent', 'messages_notification_new_message' ) ) {
	remove_action( 'messages_message_sent', 'messages_notification_new_message', 10 );
	add_action( 'messages_message_sent', 'lckrm_messages_notification_new_message', 10 );
}

function lckrm_maybe_render_mass_message_form() {
	if ( is_super_admin() || bp_group_is_admin() ) : ?>
		<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post" id="mass-message-form" role="complementary" class="ui form">
			<div class="ui inverted dimmer" id="mass-message-loading-overlay">
				<div class="ui text loader">
					Sending Message...
				</div>
			</div>
			<p class="activity-greeting">Send a Mass Message</p>

			<div id="mass-message-content">
				<div id="mass-message-textarea">
					<label for="mass-message" class="bp-screen-reader-text">Post a mass message</label>
					<div class="ui fluid field">
						<textarea class="bp-suggestions" name="mass-message" id="mass-message" cols="50" rows="10" data-suggestions-group-id="21"></textarea>
					</div>

					<div id="mass-message-options">
						<div id="mass-message-submit">
							<div class="button small ui">
								<div class="ui toggle checkbox">
									<input type="checkbox" class="hidden" name="mass-message-send-notifications" value="yes" id="mass_message_send_notifications" checked="checked" />
									<label for="mass_message_send_notifications">Send Notification</label>
								</div>
							</div>
							
							<input class="button large medium blue ui" type="submit" name="aw-mass-message-submit" id="aw-mass-message-submit" value="Send Message" />
						</div>
					</div><!-- #mass-message-options -->
				</div>

			</div><!-- #mass-message-content -->
			<input type="hidden" name="group_id" value="<?php echo bp_get_current_group_id() ?>" />
			<input type="hidden" name="action" value="lckrm/group/mass_message" />
			<?php wp_nonce_field( 'lckrm/group/send_mass_message', '_wpnonce', false ); ?>
		</form>
	<?php
	endif;
}
// add_action( 'bp_before_group_activity_post_form', 'lckrm_maybe_render_mass_message_form', 10 );

function lckrm_grab_new_message_id( $message ) {
	$GLOBALS['lckrm_newest_message_id'] = $message->id;
}

function lckrm_maybe_send_group_mass_message() {
	$group_id = absint( $_POST['group_id'] );
	$message = trim( stripslashes( $_POST['mass-message'] ) );
	$group = groups_get_group( array( 'group_id' => $group_id ) );

	if ( ! $group_id || ! $group || ! wp_verify_nonce( $_POST['_wpnonce'], 'lckrm/group/send_mass_message' ) ) {
		wp_send_json_error( array( 'message' => 'Malformed request' ) );
	}

	if ( ! is_super_admin() && ! groups_is_user_admin( get_current_user_id(), $group_id ) ) {
		wp_send_json_error( array( 'message' => 'You are not allowed to do this.' ) );
	}

	if ( ! $message ) {
		wp_send_json_error( array( 'message' => 'Please enter a message.' ) );
	}

	$message_thread_id = groups_get_groupmeta( $group_id, '_lckrm_mass_message_id', true );
	$data = array(
		'sender_id'  => 'grp_' . $group_id, // This will be converted to 0
		'recipients' => lckrm_get_group_members( $group_id ),
		'content'    => $message,
		'subject'    => $group->name,
		'error_type' => 'wp_error',
	);
	if ( $message_thread_id ) {
		$data['thread_id'] = $message_thread_id;
	}

	$GLOBALS['lckrm_no_message_notifications'] = true;
	$GLOBALS['lckrm_newest_message_id'] = false;

	add_action( 'messages_message_sent', 'lckrm_grab_new_message_id', 10 );
	$thread_id = messages_new_message( $data );
	remove_action( 'messages_message_sent', 'lckrm_grab_new_message_id', 10 );

	if ( ! is_wp_error( $thread_id ) ) {
		if ( ! $message_thread_id ) {
			groups_update_groupmeta( $group_id, '_lckrm_mass_message_id', $thread_id );
		}

		bp_messages_update_meta( $GLOBALS['lckrm_newest_message_id'], '_lckrm_from_group', $group_id );

		if ( ! empty( $_POST['mass-message-send-notifications'] ) ) {
			$message_content = 100 < mb_strlen( $message ) ? mb_substr( $message, 0, 97 ) . '...' : $message;
			$email_subject = $group->name . ' - new update';

			foreach ( $data['recipients'] as $member_id ) {
				// if ( $member_id == get_current_user_id() ) {
				// 	continue;
				// }
				$message_link = apply_filters( 'bp_get_message_thread_view_link', trailingslashit( bp_core_get_user_domain( $member_id ) . bp_get_messages_slug() . '/view/' . $thread_id ) ) . '#msg-' . intval( $GLOBALS['lckrm_newest_message_id'] );

				$user = get_userdata( $member_id );

				if ( 'no' != bp_get_user_meta( $user->ID, 'lckrm_email_notifications', true ) ) {
					$email_message = lckrm_get_message_contents( 'New Team Message', array( 'group' => $group_id, 'team_message' => $message, 'user' => $user, 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'redirect_to' => $message_link ) ) ) );
					$to = $user->user_email;
					
					add_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
					wp_mail( $to, $email_subject, $email_message );
					remove_filter( 'wp_mail_content_type', 'lckrm_set_mail_content_type');
				}

				$user_number = bp_get_user_meta( $user->ID, 'lckrm_member_cellphone', true );

				if ( $user_number && LCKRM_Twilio::is_available() && 'no' != bp_get_user_meta( $user->ID, 'lckrm_sms_notifications', true ) ) {
					LCKRM_Twilio::send_sms( $user_number, lckrm_get_message_contents( 'New Team Message', array( 'group' => $group_id, 'team_message' => $message_content, 'user' => $user, 'type' => 'SMS', 'url' => lckrm_generate_user_auth_url( $user->ID, array( 'redirect_to' => $message_link ) ) ) ) );
				}
			}

			bp_activity_add( array(
				'action'            => 'A new message was posted by <a href="' . esc_url( bp_get_group_permalink( $group ) ) . '">' . $group->name . '</a>',
				'component'         => 'groups',
				'type'              => 'lckrm_mass_message',
				'user_id'           => 0,
				'item_id'           => $group_id,
				'secondary_item_id' => $GLOBALS['lckrm_newest_message_id'],
				'content'           => $message,
			) );
		}

		wp_send_json_success( array( 'message' => 'Mass message sent successfully.' ) );
	} else {
		wp_send_json_error( array( 'message' => 'The following error occurred while trying to send a mass message: <br />' . $thread_id->get_error_message() ) );
	}
}
add_action( 'wp_ajax_lckrm/group/mass_message', 'lckrm_maybe_send_group_mass_message', 10 );

function lckrm_filter_message_thread_from( $from ) {
	global $messages_template, $thread_template;

	$message_id = false;
	$with_link = false;
	if ( 'bp_get_the_thread_message_sender_name' == current_filter() ) {
		$message_id = $thread_template->message->id;
	} else {
		$with_link = true;
		$message_id = $messages_template->thread->last_message_id;
	}
	$from_group = bp_messages_get_meta( $message_id, '_lckrm_from_group', true );

	if ( $from_group ) {
		$group = groups_get_group( array( 'group_id' => $from_group ) );
		if ( $group ) {
			$from = '';
			if ( $with_link ) {
				$from .= '<a href="' . esc_url( bp_get_group_permalink( $group ) ) . '" title="' . esc_attr( bp_get_group_name( $group ) ) . '">';
			}
			$from .= bp_get_group_name( $group );
			if ( $with_link ) {
				$from .= '</a>';
			}
		}
	}

	return $from;
}
add_filter( 'bp_get_message_thread_from', 'lckrm_filter_message_thread_from', 10 );
add_filter( 'bp_get_the_thread_message_sender_name', 'lckrm_filter_message_thread_from', 10 );

function lckrm_filter_thread_message_sender_link( $link ) {
	global $thread_template;

	$message_id = $thread_template->message->id;
	$from_group = bp_messages_get_meta( $message_id, '_lckrm_from_group', true );

	if ( $from_group ) {
		$group = groups_get_group( array( 'group_id' => $from_group ) );
		if ( $group ) {
			return bp_get_group_permalink( $group );
		}
	}

	return $from;
}
add_filter( 'bp_get_the_thread_message_sender_link', 'lckrm_filter_thread_message_sender_link', 10 );

function lckrm_filter_message_thread_avatar( $avatar ) {
	global $messages_template, $thread_template;

	$message_id = false;
	if ( 'bp_get_the_thread_message_sender_avatar_thumb' == current_filter() ) {
		$message_id = $thread_template->message->id;
	} else {
		$message_id = $messages_template->thread->last_message_id;
	}

	$from_group = bp_messages_get_meta( $message_id, '_lckrm_from_group', true );

	if ( $from_group ) {
		$group = groups_get_group( array( 'group_id' => $from_group ) );
		if ( $group ) {
			return bp_core_fetch_avatar( array(
				'item_id' => $from_group,
				'title' => $group->name,
				'avatar_dir' => 'group-avatars',
				'object'     => 'group',
				'type' => 'thumb',
				'width' => 25,
				'height' => 25,
			) );
		}
	}

	return $avatar;
}
add_filter( 'bp_get_message_thread_avatar', 'lckrm_filter_message_thread_avatar', 10 );
add_filter( 'bp_get_the_thread_message_sender_avatar_thumb', 'lckrm_filter_message_thread_avatar', 10 );

function lckrm_filter_group_acitvity_avatar_object( $type ) {
	global $activities_template;

	$current_activity_item = isset( $activities_template->activity->current_comment ) ? $activities_template->activity->current_comment : $activities_template->activity;

	if ( 'lckrm_mass_message' == $current_activity_item->type ) {
		$type = 'group';
	}

	return $type;
}
add_filter( 'bp_get_activity_avatar_object_groups', 'lckrm_filter_group_acitvity_avatar_object', 10 );

function lckrm_filter_activity_user_link( $link ) {
	global $activities_template;

	$current_activity_item = isset( $activities_template->activity->current_comment ) ? $activities_template->activity->current_comment : $activities_template->activity;

	if ( 'lckrm_mass_message' == $current_activity_item->type ) {
		$group = groups_get_group( array( 'group_id' => $current_activity_item->item_id ) );
		$link = bp_get_group_permalink( $group );
	}

	return $link;
}
add_filter( 'bp_get_activity_user_link', 'lckrm_filter_activity_user_link', 10 );

function lckrm_filter_activity_can_comment( $can_comment, $activity_action ) {
	if ( 'lckrm_mass_message' == $activity_action ) {
		$can_comment = false;
	}

	return $can_comment;
}
add_filter( 'bp_activity_can_comment', 'lckrm_filter_activity_can_comment', 10, 2 );

function lckrm_maybe_render_mass_message_activity_reply_link() {
	global $activities_template;

	$current_activity_item = isset( $activities_template->activity->current_comment ) ? $activities_template->activity->current_comment : $activities_template->activity;

	if ( 'lckrm_mass_message' == $current_activity_item->type ) {
		$thread_id = groups_get_groupmeta( $current_activity_item->item_id, '_lckrm_mass_message_id', true );
		$message_link = bp_get_message_thread_view_link( $thread_id ) . '#msg-' . intval( $current_activity_item->secondary_item_id ); ?>
		<a href="<?php echo esc_url( $message_link ); ?>" class="button reply-to-message bp-secondary-action" title="<?php esc_attr( 'Reply' ); ?>">Reply</a>
		<?php
	}
}
add_action( 'bp_activity_entry_meta', 'lckrm_maybe_render_mass_message_activity_reply_link', 10 );
