<?php

function carpathiafc_init_custom_widgets() {
	$path = dirname( __FILE__ ) . '/widgets/';
	require_once( $path . 'custom-user-nav.php' );
	require_once( $path . 'upcoming_events.php' );
}
add_action( 'widgets_init', 'carpathiafc_init_custom_widgets', 12 );
