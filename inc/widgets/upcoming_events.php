<?php

class Custom_Buddy_Press_Upcoming_Events_Widget extends WP_Widget {

	public function __construct() {
		$widget_ops = array( 'classname' => 'custom-bp-upcoming-events-widget', 'description' => __( 'Displays # upcoming group events.' ) );
		$control_ops = array( 'width' => 400, 'height' => 40 );
		parent::__construct( 'custom-bp-upcoming-events-widget', __( 'Custom BuddyPress Upcoming Events' ), $widget_ops, $control_ops );
	}

	public function widget( $args, $instance ) {
		if ( ! is_user_logged_in() && ! $instance['events_number'] ) {
			return;
		}

		extract( $args );

		$events_number = isset( $instance['events_number'] ) ? $instance['events_number'] : 3;

		echo $args['before_widget'];

		$groups = array();
		$vgroups = BP_Groups_Group::get(array(
							'per_page'=>999,
							'user_id' => get_current_user_id(),
							'show_hidden' => true,
							));
		foreach ( $vgroups as $tgroup ) {
			if ( is_array( $tgroup ) ) {
				foreach ($tgroup as $group) {
					$groups[] = $group->id;
				}
			}
		}
 
		$query_args = array(
			'post_type'			=> 'sp_event',
			'post_status'		=> array( 'future' ),
			'posts_per_page'	=> $events_number,
			'orderby'			=> 'date',
			'order'				=> 'ASC',
			'meta_query'  => array(
				array(
					'key' => 'lckrm_event_created_group',
					'value' => $groups,
					'compare' => 'IN',
				),
			),
		);


		$query = new WP_Query( $query_args );
		$events = $query->posts;

		?>
		<div class="lckrm-upcoming-events widget">
		<?php foreach ( $events as $key => $event ) :
			$group_id = get_post_meta( $event->ID, 'lckrm_event_created_group', true );
			$group_permalink = lckrm_get_group_permalink_by_id( $group_id );
			$format = get_post_meta( $event->ID, 'sp_format', true );
			$color = 'gray';
			$letter = 'X';
			$post_time = strtotime( $event->post_date );

			if ( 'league' == $format || 'friendly' == $format ) {
				$color = 'blue';
				$letter = 'G';
			} elseif ( 'practice' == $format ) {
				$color = 'green';
				$letter = 'P';
			} elseif ( 'meeting' == $format ) {
				$color = 'red';
				$letter = 'M';
			} elseif ( 'other' == $format ) {
				$color = 'yellow';
				$letter = 'O';
			}

			$terms = get_the_terms( $event->ID, 'sp_venue', null );

		    if ( $terms ) {
				$term = array_shift( $terms );
				$term->term_link = get_term_link( $term->term_id, 'sp_venue' );
				$term->term_link = is_wp_error( $term->term_link ) ? '' : $term->term_link;
		    }
		?>

		<!--  event colors -->
		
			<div class="event-react-block widget">
				<div class="ui grid mobile">
					<div class="four wide column event-type">
						<a href="<?php echo esc_url( $group_permalink ); ?>event/?event_id=<?php echo $event->ID; ?>"  class="ui large circular label <?php echo $color; ?>"><?php echo $letter; ?></a>
					</div>
					<div class="twelve wide column event-info">
						<h3><a href="<?php echo esc_url( $group_permalink ); ?>event/?event_id=<?php echo $event->ID; ?>"><?php echo get_the_title( $event->ID ); ?></a></h3>
						<p><?php echo date( 'D M d, Y @ g:iA', $post_time ); ?></p>
						<a href="<?php echo $term ? $term->term_link : '#'; ?>"><i class="fa fa-map-marker"></i> <?php echo $term ? $term->name : ''; ?></a>
					</div>
				</div>
			</div>

		<?php endforeach; ?>
		</div>

		<?php

		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['events_number'] = $new_instance['events_number'];
		// $instance['title'] = $new_instance['title'];

		return $instance;
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'events_number' => null ) ); ?>
		<p>Number of recent events to display.</p>
		<input type="number" name="<?php echo $this->get_field_name('events_number'); ?>" min="0" max="8" value="<?php echo $instance['events_number']; ?>">
<?php
	}
}
register_widget( 'Custom_Buddy_Press_Upcoming_Events_Widget' );