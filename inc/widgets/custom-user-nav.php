<?php

class Custom_Buddy_Press_User_Nav_Widget extends WP_Widget {

	public function __construct() {
		$widget_ops = array( 'classname' => 'custom-bp-user-nav-widget', 'description' => __( 'Displays a navigation for the currently logged-in user, independant of the current context.' ) );
		$control_ops = array( 'width' => 400, 'height' => 40 );
		parent::__construct( 'custom-bp-user-nav-widget', __( 'Custom BuddyPress User Nav' ), $widget_ops, $control_ops );
	}

	public function widget( $args, $instance ) {
		if ( ! is_user_logged_in() ) {
			return;
		}

		echo $args['before_widget'];
		
		$avatar_args = array(
			'type'   => 'thumb',
			'width'  => 90,
			'height' => 90,
		);

		$viewing_curr_user = bp_displayed_user_id() == bp_loggedin_user_id();
		$user_domain   = bp_loggedin_user_domain();
		$userdata      = get_userdata( bp_loggedin_user_id() );

		$settings_slug = bp_get_settings_slug();
		$settings_link = trailingslashit( $user_domain . $settings_slug );

		$messages_slug  = bp_get_messages_slug();
		$messages_link  = trailingslashit( $user_domain . $messages_slug );
		$messages_count = bp_get_total_unread_messages_count();
		$messages_class = ( 0 === $messages_count ) ? 'no-count' : 'count';
		$messages_title = sprintf( __( 'Messages <span class="ui blue circular label right floated %s">%s</span>', 'buddypress' ), esc_attr( $messages_class ), bp_core_number_format( $messages_count ) );

		$notifications_slug  = bp_get_notifications_slug();
		$notifications_link  = trailingslashit( $user_domain . $notifications_slug );
		$notifications_count = bp_notifications_get_unread_notification_count( bp_displayed_user_id() );
		$notifications_class = ( 0 === $notifications_count ) ? 'no-count' : 'count';
		$notifications_title = sprintf( _x( 'Notifications <span class="ui blue circular label right floated %s">%s</span>', 'Profile screen nav', 'buddypress' ), esc_attr( $notifications_class ), bp_core_number_format( $notifications_count ) );
		
		$groups_slug   = bp_get_groups_slug();
		$groups_link   = trailingslashit( $user_domain . $groups_slug );
		$groups_count  = bp_get_total_group_count_for_user();
		$groups_class  = ( 0 === $groups_count ) ? 'no-count' : 'count';
		$groups_title  = sprintf( _x( 'Groups <span class="ui blue circular label right floated %s">%s</span>', 'Group screen nav with counter', 'buddypress' ), esc_attr( $groups_class ), bp_core_number_format( $groups_count ) );

		 ?>

		<div class="ui relaxed divided items">
			<div class="item">
				<a class="ui tiny image" href="<?php echo esc_url( $user_domain ); ?>">
					<?php bp_loggedin_user_avatar( $avatar_args ); ?>
				</a>
				<div class="content">
					<a class="header ui huge" href="<?php echo esc_url( $user_domain ); ?>">
						<?php echo $userdata->first_name ? $userdata->first_name : $userdata->display_name; ?>
					</a>
					<div class="meta">
						<a href="<?php echo esc_url( $settings_link ); ?>"<?php echo $viewing_curr_user && bp_is_current_component( $settings_slug ) ? ' class="active"' : ''; ?>><i class="setting icon"></i>Settings</a>
					</div>
				</div>
			</div>
		</div>

		<div class="ui big link list">
			<a class="item item-messages<?php echo $viewing_curr_user && bp_is_current_component( $messages_slug ) ? ' active' : ''; ?>" href="<?php echo esc_url( $messages_link ); ?>"><i class="left floated mail outline icon"></i><?php echo $messages_title; ?></a>
			<a class="item item-notifications<?php echo $viewing_curr_user && bp_is_current_component( $notifications_slug ) ? ' active' : ''; ?>" href="<?php echo esc_url( $notifications_link ); ?>"><i class="left floated alarm outline icon"></i><?php echo $notifications_title; ?></a>
			<a class="item item-groups<?php echo $viewing_curr_user && bp_is_current_component( $groups_slug ) ? ' active' : ''; ?>" href="<?php echo esc_url( $groups_link ); ?>"><i class="left floated users icon"></i><?php echo $groups_title; ?></a>
		</div>
		<?php

		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		// $instance['title'] = $new_instance['title'];

		return $instance;
	}

	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array() ); ?>
		<p>There are no settings for this widget.</p>
<?php
	}
}
register_widget( 'Custom_Buddy_Press_User_Nav_Widget' );