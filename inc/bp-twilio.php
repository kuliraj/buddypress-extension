<?php

if ( ! defined( 'LCKRM_TWILIO_SID' ) ) {
	define( 'LCKRM_TWILIO_SID', false );
}

if ( ! defined( 'LCKRM_TWILIO_AUTH_TOKEN' ) ) {
	define( 'LCKRM_TWILIO_AUTH_TOKEN', false );
}

if ( ! defined( 'LCKRM_TWILIO_NUMBER' ) ) {
	define( 'LCKRM_TWILIO_NUMBER', false );
}

class LCKRM_Twilio {
	static $credentials;

	public static function start() {
		static $started = false;
 
		if ( ! $started ) {
			self::add_filters();
 
			self::add_actions();
 
			$started = true;

			self::$credentials = array(
				'account_sid' => LCKRM_TWILIO_SID,
				'auth_token'  => LCKRM_TWILIO_AUTH_TOKEN,
				'number'      => LCKRM_TWILIO_NUMBER,
			);
		}
	}
 
	protected static function add_filters() {
		// Add all filters here
	}
 
	protected static function add_actions() {
		// Add all actions here
	}

	public static function is_available() {
		return ! empty( self::$credentials['account_sid'] ) && ! empty( self::$credentials['auth_token'] );
	}

	public static function client() {
		static $client = false;

		if ( ! $client ) {
			require_once( dirname( dirname( __FILE__ ) ) . '/vendor/twilio/Services/Twilio.php' );

			$client = new Services_Twilio( self::$credentials['account_sid'], self::$credentials['auth_token'] );
		}

		return $client;
	}

	public static function prepare_number_for_api( $number ) {

		return $number;
	}

	public static function send_sms( $to, $message, $debug = false ) {
		if ( ! self::is_available() ) {
			return false;
		}

		$result = true;

		try {
			self::client()->account->messages->sendMessage(
				self::$credentials['number'],
				$to,
				$message
			);
		} catch (Exception $e) {
			$result = false;
			if ( $debug ) {
				var_dump( $e->getMessage() );
			}
		}

		return $result;
	}
}
 
LCKRM_Twilio::start();
